CREATE SYNONYM [dbo].[PrimaryClass] FOR [dbo].[Classes]
GO
GRANT SELECT ON [dbo].[PrimaryClass] TO [pEnrolmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[PrimaryClass] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PrimaryClass] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PrimaryClass] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PrimaryClass] TO [public] AS [dbo]
GO

