SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpSalaryLevel](
	[salLevel] [nvarchar](5) NOT NULL,
	[salLevelDesc] [nvarchar](50) NULL,
	[salDefaultCostPoint] [nvarchar](10) NULL,
	[salExternal] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpSalaryLevel1_PK] PRIMARY KEY NONCLUSTERED 
(
	[salLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpSalaryLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSalaryLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSalaryLevel] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [dbo].[lkpSalaryLevel] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpSalaryLevel] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpSalaryLevel] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpSalaryLevel] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpSalaryLevel] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salary level codes in the Civil Service. Salary points further divide these, and Pay rates can be linked to salary points.
A range of salary levels may be associated to any RoleGrade, hence to any establishm,ent position (which must define a RoleGrade).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpSalaryLevel'
GO

