SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[roomCounts](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[rmType] [nvarchar](10) NOT NULL,
	[numRooms] [int] NULL,
	[avgSize] [float] NULL,
	[NewestRoom] [int] NULL,
	[rmMaterials] [nvarchar](1) NULL,
	[Estimate] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Total rooms by type in school.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'roomCounts'
GO

