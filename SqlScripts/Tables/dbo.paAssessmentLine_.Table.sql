SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paAssessmentLine_](
	[palID] [int] IDENTITY(1,1) NOT NULL,
	[paID] [int] NOT NULL,
	[paindID] [int] NOT NULL,
	[paplCode] [nvarchar](10) NOT NULL,
	[palComment] [nvarchar](200) NULL,
 CONSTRAINT [PK_paAssessmentLine_] PRIMARY KEY CLUSTERED 
(
	[palID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paAssessmentLine_]  WITH CHECK ADD  CONSTRAINT [FK_paAssessmentLine__paAssessmentL] FOREIGN KEY([paID])
REFERENCES [dbo].[paAssessment_] ([paID])
GO
ALTER TABLE [dbo].[paAssessmentLine_] CHECK CONSTRAINT [FK_paAssessmentLine__paAssessmentL]
GO

