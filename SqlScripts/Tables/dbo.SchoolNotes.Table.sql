SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolNotes](
	[ntID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[ntDate] [datetime] NULL,
	[ntAuthor] [nvarchar](50) NULL,
	[ntContact] [nvarchar](50) NULL,
	[ntSubject] [nvarchar](250) NULL,
	[ntNote] [ntext] NULL,
	[svyYear] [smallint] NULL,
 CONSTRAINT [aaaaaSchoolNotes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ntID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolNotes] TO [pSchoolReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolNotes] TO [pSchoolWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolNotes] TO [pSchoolWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolNotes] TO [pSchoolWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolNotes] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SchoolNotes_SchNo] ON [dbo].[SchoolNotes]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolNotes]  WITH CHECK ADD  CONSTRAINT [SchoolNotes_FK00] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolNotes] CHECK CONSTRAINT [SchoolNotes_FK00]
GO
ALTER TABLE [dbo].[SchoolNotes]  WITH CHECK ADD  CONSTRAINT [SchoolNotes_FK01] FOREIGN KEY([svyYear])
REFERENCES [dbo].[Survey] ([svyYear])
GO
ALTER TABLE [dbo].[SchoolNotes] CHECK CONSTRAINT [SchoolNotes_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text notes about a school. Signed and dated. schNo is the foreign key from Schools.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolNotes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolNotes'
GO

