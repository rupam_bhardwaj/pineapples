SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpBuildingSubType](
	[bsubCode] [nvarchar](10) NOT NULL,
	[bdlgCode] [nvarchar](10) NOT NULL,
	[bsubDescription] [nvarchar](50) NOT NULL,
	[bsubDescriptionL1] [nvarchar](50) NULL,
	[bsubDescriptionL2] [nvarchar](50) NULL,
	[bsubRoomsClass] [int] NULL,
	[bsubRoomsOHT] [int] NULL,
	[bsubRoomsStaff] [int] NULL,
	[bsubRoomsAdmin] [int] NULL,
	[bsubRoomsStorage] [int] NULL,
	[bsubRoomsDorm] [int] NULL,
	[bsubRoomsKitchen] [int] NULL,
	[bsubRoomsDining] [int] NULL,
	[bsubRoomsLibrary] [int] NULL,
	[bsubRoomsSpecialTuition] [int] NULL,
	[bsubRoomsOther] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpBuildingSubType] PRIMARY KEY CLUSTERED 
(
	[bsubCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpBuildingSubType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpBuildingSubType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpBuildingSubType] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBuildingSubType] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpBuildingSubType] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpBuildingSubType]  WITH CHECK ADD  CONSTRAINT [FK_lkpBuildingSubType_lkpBuildingTypes] FOREIGN KEY([bdlgCode])
REFERENCES [dbo].[lkpBuildingTypes] ([bdlgCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[lkpBuildingSubType] CHECK CONSTRAINT [FK_lkpBuildingSubType_lkpBuildingTypes]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Subtypes of buildings within Building Type. Particularly used for teacher houses, where the subtype corresponds to the earlier resource type.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingSubType'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingSubType'
GO

