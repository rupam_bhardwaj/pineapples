SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[schemaVersion](
	[schemaID] [nvarchar](50) NOT NULL,
	[schemaDate] [datetime] NULL,
	[schemaApplied] [datetime] NULL,
 CONSTRAINT [PK_schemaVersion] PRIMARY KEY CLUSTERED 
(
	[schemaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[schemaVersion] TO [pineapples] AS [dbo]
GO
GRANT INSERT ON [dbo].[schemaVersion] TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[schemaVersion] TO [pineapples] AS [dbo]
GO
GRANT SELECT ON [dbo].[schemaVersion] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[schemaVersion] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Currently unused' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'schemaVersion'
GO

