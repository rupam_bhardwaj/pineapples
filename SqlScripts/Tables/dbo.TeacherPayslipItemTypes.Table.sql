SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherPayslipItemTypes](
	[payitemType] [nvarchar](20) NOT NULL,
	[payitemDescription] [nvarchar](100) NULL,
	[payItemPaySlipText] [nvarchar](30) NULL,
 CONSTRAINT [PK_TeacherPayslipItemTypes] PRIMARY KEY CLUSTERED 
(
	[payitemType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT DELETE ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherOps] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherOps] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherPayslipItemTypes] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherPayslipItemTypes] TO [public] AS [dbo]
GO

