SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpLevels](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[lvlYear] [smallint] NULL,
	[ilsCode] [nvarchar](10) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[lvlGV] [nvarchar](1) NULL,
	[secCode] [nvarchar](3) NULL,
	[lvlDefaultPath] [nvarchar](10) NULL,
 CONSTRAINT [aaaaalkpLevels1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpLevels] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpLevels] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpLevels] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpLevels] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpLevels] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpLevels] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpLevels] ADD  CONSTRAINT [DF__lkpLevels__codeS__78D3EB5B]  DEFAULT ((0)) FOR [codeSeq]
GO
ALTER TABLE [dbo].[lkpLevels]  WITH CHECK ADD  CONSTRAINT [FK_lkpLevels_EducationSectors] FOREIGN KEY([secCode])
REFERENCES [dbo].[EducationSectors] ([secCode])
GO
ALTER TABLE [dbo].[lkpLevels] CHECK CONSTRAINT [FK_lkpLevels_EducationSectors]
GO
ALTER TABLE [dbo].[lkpLevels]  WITH CHECK ADD  CONSTRAINT [lkpLevels_FK00] FOREIGN KEY([ilsCode])
REFERENCES [dbo].[ISCEDLevelSub] ([ilsCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpLevels] CHECK CONSTRAINT [lkpLevels_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Class Levels are the fundamental unit of recording enrolment. The Class Level defines the Year Of Education (hence Education Level, Level Alt, Level Alt2) Sector, and ISCED Code, all critical for reporting. 
School types map to class level taught in that school type. This metadata is used to set up survey form gtrids with the appropriate set of level codes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLevels'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpLevels'
GO

