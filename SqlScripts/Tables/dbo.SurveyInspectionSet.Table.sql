SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyInspectionSet](
	[inspsetID] [int] NOT NULL,
	[svyYear] [int] NOT NULL,
	[intyCode] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_SurveyInspectionSet] PRIMARY KEY CLUSTERED 
(
	[inspsetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_SurveyInspection] UNIQUE NONCLUSTERED 
(
	[svyYear] ASC,
	[intyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyInspectionSet] TO [pInspectionRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyInspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyInspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyInspectionSet] TO [pInspectionWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[SurveyInspectionSet] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyInspectionSet] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyInspectionSet] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyInspectionSet] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyInspectionSet] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Provides a link between a survey year, and an inspection set ID for a given survey type. Applies particularly to BLDG - Building Reviews. A Building Review as part of the school survey will use the linked inspection set ID as the Inspection Set ID for the Inspection (Building Review). This ID will be aloocated at the time a Building Review is created gfrom the Survey, if it does not already exist.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyInspectionSet'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyInspectionSet'
GO

