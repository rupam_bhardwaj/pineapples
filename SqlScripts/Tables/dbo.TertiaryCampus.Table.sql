SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TertiaryCampus](
	[tcampID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[tcampName] [nvarchar](150) NULL,
	[tcampAddress1] [nvarchar](150) NULL,
	[tcampAddress2] [nvarchar](150) NULL,
	[tcampAddress3] [nvarchar](150) NULL,
	[tcampAddress4] [nvarchar](150) NULL,
	[tcampPh] [nvarchar](50) NULL,
	[tcampClassrooms] [int] NULL,
	[tcampLecTheatres] [int] NULL,
	[tcampStaffRooms] [int] NULL,
	[tcampStoreRooms] [int] NULL,
	[tcampWorkshops] [int] NULL,
	[tcampComputerLabs] [int] NULL,
	[tcampScienceLabs] [int] NULL,
	[tcampBoatLabs] [int] NULL,
	[tcampLibraries] [int] NULL,
	[tcampAmenities] [int] NULL,
	[tcampDormitoriesM] [int] NULL,
	[tcampDormitoriesF] [int] NULL,
 CONSTRAINT [TertiaryCampus_PK] PRIMARY KEY NONCLUSTERED 
(
	[tcampID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TertiaryCampus] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[TertiaryCampus] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TertiaryCampus] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TertiaryCampus] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TertiaryCampus] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [ssID] ON [dbo].[TertiaryCampus]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TertiaryCampus]  WITH CHECK ADD  CONSTRAINT [FK_TertiaryCampus_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TertiaryCampus] CHECK CONSTRAINT [FK_TertiaryCampus_SchoolSurvey]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campuses of a tertiary institution. Solomon Islands Tertiary survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TertiaryCampus'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Tertiary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TertiaryCampus'
GO

