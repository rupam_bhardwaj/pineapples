SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeCourseMap](
	[sjID] [int] IDENTITY(1,1) NOT NULL,
	[sjSurvey] [nvarchar](50) NULL,
	[stCode] [nvarchar](10) NULL,
	[sjSort] [int] NULL,
	[sjSubject] [nvarchar](50) NULL,
 CONSTRAINT [aaaaametaSchoolTypeCourseMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[sjID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeCourseMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeCourseMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeCourseMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeCourseMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeCourseMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeCourseMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeCourseMap] ADD  CONSTRAINT [DF__metaSchoo__sjSor__09FE775D]  DEFAULT ((0)) FOR [sjSort]
GO
ALTER TABLE [dbo].[metaSchoolTypeCourseMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeCourseMap_FK00] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeCourseMap] CHECK CONSTRAINT [metaSchoolTypeCourseMap_FK00]
GO

