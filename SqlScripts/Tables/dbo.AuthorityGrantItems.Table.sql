SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorityGrantItems](
	[agiID] [int] IDENTITY(1,1) NOT NULL,
	[agID] [int] NOT NULL,
	[grCode] [nvarchar](20) NULL,
	[grValue] [money] NOT NULL,
	[agiQty] [int] NOT NULL,
	[agiValue] [money] NULL,
	[agiPaymentNo] [int] NULL,
	[agiReverse] [nvarchar](1) NULL,
 CONSTRAINT [AuthorityGrantItems_PK] PRIMARY KEY NONCLUSTERED 
(
	[agiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[AuthorityGrantItems] TO [pFinanceReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[AuthorityGrantItems] TO [pFinanceWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[AuthorityGrantItems] TO [pFinanceWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AuthorityGrantItems] TO [pFinanceWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[AuthorityGrantItems] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [agID] ON [dbo].[AuthorityGrantItems]
(
	[agID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [GrantRulesAuthorityGrantItems] ON [dbo].[AuthorityGrantItems]
(
	[grCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuthorityGrantItems] ADD  CONSTRAINT [DF__Authority__agiVa__3BD82C18]  DEFAULT ((0)) FOR [agiValue]
GO
ALTER TABLE [dbo].[AuthorityGrantItems] ADD  CONSTRAINT [DF__Authority__agiPa__3CCC5051]  DEFAULT ((0)) FOR [agiPaymentNo]
GO
ALTER TABLE [dbo].[AuthorityGrantItems]  WITH CHECK ADD  CONSTRAINT [AuthorityGrantAuthorityGrantItems] FOREIGN KEY([agID])
REFERENCES [dbo].[AuthorityGrant] ([agID])
GO
ALTER TABLE [dbo].[AuthorityGrantItems] CHECK CONSTRAINT [AuthorityGrantAuthorityGrantItems]
GO
ALTER TABLE [dbo].[AuthorityGrantItems]  WITH CHECK ADD  CONSTRAINT [GrantRulesAuthorityGrantItems] FOREIGN KEY([grCode])
REFERENCES [dbo].[GrantRules] ([grCode])
GO
ALTER TABLE [dbo].[AuthorityGrantItems] CHECK CONSTRAINT [GrantRulesAuthorityGrantItems]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AuthorityGrantItems is the "line item" table for authority Grants. The Header table, AuthorityGrant, provides the foreign key. The type of Grant Items that may be listed are determined by the Grant Rules.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrantItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrantItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Grants form' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrantItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrantItems'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'School Grants' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuthorityGrantItems'
GO

