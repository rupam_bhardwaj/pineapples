SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpTeacherTraining](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpTeacherTraining1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpTeacherTraining] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpTeacherTraining] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherTraining] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpTeacherTraining] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherTraining] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpTeacherTraining] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpTeacherTraining] ADD  CONSTRAINT [DF__lkpTeache__codeS__3552E9B6]  DEFAULT ((0)) FOR [codeSeq]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'user configurable list of options for Teacher Training status.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherTraining'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherTraining'
GO

