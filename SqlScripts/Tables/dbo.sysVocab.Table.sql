SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysVocab](
	[vocabName] [nvarchar](50) NOT NULL,
	[vocabTerm] [nvarchar](50) NULL,
	[vocabTermL1] [nvarchar](50) NULL,
	[vocabTermL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaasysVocab1_PK] PRIMARY KEY NONCLUSTERED 
(
	[vocabName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[sysVocab] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[sysVocab] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[sysVocab] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[sysVocab] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[sysVocab] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vocab terms are locally used terms for common concepts such as "School No", "Payroll No", "Registration Number", "district" etc. Vocab terms are translated on forms, and translated in pivot tables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysVocab'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Localisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysVocab'
GO

