SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Survey](
	[svyYear] [smallint] NOT NULL,
	[svyPSAge] [int] NULL,
	[svyMonth] [int] NULL,
	[svyCensusDate] [datetime] NULL,
	[svyCollectionDate] [datetime] NULL,
	[svyClosed] [bit] NULL,
	[svyDefaultPath] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaSurvey1_PK] PRIMARY KEY NONCLUSTERED 
(
	[svyYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Survey] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Survey] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Survey] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Survey] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Survey] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Survey] ADD  CONSTRAINT [DF__Survey__svyPSAge__7EF6D905]  DEFAULT ((0)) FOR [svyPSAge]
GO
ALTER TABLE [dbo].[Survey] ADD  CONSTRAINT [DF__Survey__svyMonth__7FEAFD3E]  DEFAULT ((0)) FOR [svyMonth]
GO
ALTER TABLE [dbo].[Survey] ADD  CONSTRAINT [DF__Survey__svyClose__00DF2177]  DEFAULT ((0)) FOR [svyClosed]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Survey definition record. One record for each survey year. Defines some key charactersitics of the survey, such as the Official Start Age for primary school, census date, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Survey'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Survey'
GO

