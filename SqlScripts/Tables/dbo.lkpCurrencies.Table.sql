SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpCurrencies](
	[currCode] [nvarchar](3) NOT NULL,
	[currName] [nvarchar](50) NULL,
	[currNameL1] [nvarchar](50) NULL,
	[currNameL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpCurrencies_PK] PRIMARY KEY NONCLUSTERED 
(
	[currCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpCurrencies] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpCurrencies] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpCurrencies] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpCurrencies] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpCurrencies] TO [public] AS [dbo]
GO

