SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeRoomMap](
	[rmapID] [int] IDENTITY(1,1) NOT NULL,
	[rmapSurveyName] [nvarchar](50) NULL,
	[stCode] [nvarchar](10) NULL,
	[rmapGroup] [nvarchar](5) NULL,
	[rmapCode] [nvarchar](10) NULL,
	[rmapSort] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeRoomMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[rmapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeRoomMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeRoomMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeRoomMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeRoomMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeRoomMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeRoomMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeRoomMap] ADD  CONSTRAINT [DF__metaSchoo__rmapS__3FD07829]  DEFAULT ((0)) FOR [rmapSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Controls the configuraiton of "admin" room list. . Sets may be created by survey form, school type , and mapping group.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeRoomMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeRoomMap'
GO

