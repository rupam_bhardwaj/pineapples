SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Establishment](
	[estpNo] [nvarchar](20) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[estpScope] [nvarchar](1) NULL,
	[estpAuth] [nvarchar](10) NULL,
	[estpRoleGrade] [nvarchar](20) NULL,
	[estpActiveDate] [datetime] NOT NULL,
	[estpClosedDate] [datetime] NULL,
	[estpClosedReason] [nvarchar](100) NULL,
	[estpTitle] [nvarchar](50) NULL,
	[estpDescription] [nvarchar](65) NULL,
	[estpSubject] [nvarchar](50) NULL,
	[estpFlag] [nvarchar](10) NULL,
	[estpCreateDate] [datetime] NULL,
	[estpCreateUser] [nvarchar](100) NULL,
	[estpEditDate] [datetime] NULL,
	[estpEditUser] [nvarchar](100) NULL,
	[estpConfirmed] [datetime] NULL,
	[estpSendDate] [datetime] NULL,
	[estpSendUser] [nvarchar](100) NULL,
	[estpSendBatch] [int] NULL,
	[estpNote] [nvarchar](400) NULL,
	[estpID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Establishment] PRIMARY KEY CLUSTERED 
(
	[estpNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Establishment_ID] UNIQUE NONCLUSTERED 
(
	[estpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Establishment] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Establishment] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[Establishment] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Establishment] TO [pEstablishmentWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Establishment] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Establishment] ON [dbo].[Establishment]
(
	[estpActiveDate] ASC,
	[estpClosedDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Establishment_Flag] ON [dbo].[Establishment]
(
	[estpFlag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Establishment_SchNo] ON [dbo].[Establishment]
(
	[schNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Establishment]  WITH CHECK ADD  CONSTRAINT [FK_Establishment_Authorities] FOREIGN KEY([estpAuth])
REFERENCES [dbo].[Authorities] ([authCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Establishment] CHECK CONSTRAINT [FK_Establishment_Authorities]
GO
ALTER TABLE [dbo].[Establishment]  WITH CHECK ADD  CONSTRAINT [FK_Establishment_RoleGrades] FOREIGN KEY([estpRoleGrade])
REFERENCES [dbo].[RoleGrades] ([rgCode])
GO
ALTER TABLE [dbo].[Establishment] CHECK CONSTRAINT [FK_Establishment_RoleGrades]
GO
ALTER TABLE [dbo].[Establishment]  WITH CHECK ADD  CONSTRAINT [FK_Establishment_Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[Establishment] CHECK CONSTRAINT [FK_Establishment_Schools]
GO
ALTER TABLE [dbo].[Establishment]  WITH CHECK ADD  CONSTRAINT [CK_Establishment] CHECK  (([estpScope] IS NULL AND [schNo] IS NOT NULL AND [estpRoleGrade] IS NOT NULL OR [estpScope]='A' AND [estpAuth] IS NOT NULL))
GO
ALTER TABLE [dbo].[Establishment] CHECK CONSTRAINT [CK_Establishment]
GO
ALTER TABLE [dbo].[Establishment]  WITH CHECK ADD  CONSTRAINT [CK_Establishment_1] CHECK  (([estpScope] IS NULL OR [estpScope]='A'))
GO
ALTER TABLE [dbo].[Establishment] CHECK CONSTRAINT [CK_Establishment_1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 27 10 2009
-- Description:	Check trigger
-- =============================================
CREATE TRIGGER [dbo].[EstablishmentChangeLog] 
   ON  [dbo].[Establishment] 
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Establishment 

-- sample simple change logging

	SET Establishment.estpEditDate = getDate()
	, Establishment.estpEditUser = suser_sname()

	FROM Establishment
	INNER JOIN INSERTED 
		ON Establishment.estpNo = INSERTED.estpNo




	
END
GO
ALTER TABLE [dbo].[Establishment] ENABLE TRIGGER [EstablishmentChangeLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 27 10 2009
-- Description:	Insert new establishment position
-- =============================================
CREATE TRIGGER [dbo].[EstablishmentInsert] 
   ON  [dbo].[Establishment] 
   INSTEAD OF  INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Nums TABLE
	(
		posNo nvarchar(20)
		, num int
	)
	

	

	declare @NumInserted int
	declare @NumSchools int
	declare @SchoolNo nvarchar(50)
	declare @NumEstpNo int
	declare @numNumsNeeded int
	declare @Authority nvarchar(10)
	declare @RoleGrade nvarchar(10)

begin try
	
	SELECT @NumSchools = count(DISTINCT schNo)
		, @SchoolNo = min(schNo)
		, @Authority = min(estpAuth)
		, @RoleGRade = min(estpRoleGRade)
		, @NumInserted = count(*)
		, @NumEstpNo = count(estpNo)
	FROM INSERTED
	
	
	if (@NumSchools > 1) 
		RAISERROR('Cannot insert positions for more than one school in a batch',16,0);
		
		
	Select @numNumsNeeded = (@NumInserted - @NumEstpNo)
	
	if @NumInserted = 1 begin
		-- in the simple case, we can put some friendly validations
		If @SchoolNo is null and @Authority is null 
				RAISERROR('<ValidationError>
							Position must have a School or Authority - both are NULL
							</ValidationError>',16,0);
		
		if @Authority is null and @RoleGRade is null
				RAISERROR('<ValidationError>
							School Position must have a Role/Grade
							</ValidationError>',16,0);
	end
		
	-- we only want to allocate numbers that we need
	if (@numNumsNeeded > 0)
		INSERT INTO @Nums
			EXEC pEstablishmentWriteX.makeEstablishmentNumbers @SchoolNo,@numNumsNeeded
	
	INSERT INTO Establishment
	(
		estpNo
		, estpScope
		, schNo
		, estpAuth
		, estpRoleGrade
		, estpActiveDate
		, estpClosedDate
		, estpClosedReason
		, estpTitle
		, estpDescription
		, estpSubject
		, estpFlag
		, estpNote
		, estpCreateDate
		, estpCreateUSer
		, estpEditDate
		, estpEditUSer
	)
	SELECT
		isnull(estpNo,N.posNo)
		, isnull(estpScope, case when schNo is null and estpAuth is not null then 'A' else null end)
		, schNo
		, estpAuth
		, estpRoleGrade
		, estpActiveDate
		, estpClosedDate
		, estpClosedReason
		, estpTitle
		, estpDescription
		, estpSubject
		, estpFlag
		, estpNote
		, getdate()
		, suser_sname()
		, getdate()
		, suser_sname()
	FROM
		(Select INSERTED.*
			, row_number() OVER (ORDER BY estpNo, estpActiveDate) ROW
		 FROM	INSERTED
		 ) ORDEREDROWS
		 -- this join will only have a hit for those rows in INSERTED, where the estpNo is null
		 LEFT JOIN @Nums N
			on ORDEREDROWS.ROW = N.num 

end try

begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	begin 
		rollback transaction
		select @errorMessage = @errorMessage + ' The transaction was rolled back.'
	end 

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
end catch
END
GO
ALTER TABLE [dbo].[Establishment] ENABLE TRIGGER [EstablishmentInsert]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Establishment contains the active positions for teachers. Each record in Establishment represents a single position, at a given school, for a given role/grade.
Positions have a start data, and optional end date.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Accessed through various SPs in the schema pEstablishmentRead and pEstablismentWrite.
Note table Establishment has an INSTEAD OF trigger, to allocate the position number. ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Workforce Review (frmManpowerReview) is the main screen to show Establishment positions and their related appointments.

' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Planning' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'authority code for authority scope positions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment', @level2type=N'CONSTRAINT',@level2name=N'FK_Establishment_Authorities'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'controls positions of Authority scope - ie supernumeraries' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment', @level2type=N'CONSTRAINT',@level2name=N'CK_Establishment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Types of scope' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Establishment', @level2type=N'CONSTRAINT',@level2name=N'CK_Establishment_1'
GO

