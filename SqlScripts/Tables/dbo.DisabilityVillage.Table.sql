SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DisabilityVillage](
	[disID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[disVillage] [nvarchar](50) NULL,
	[disAge] [int] NULL,
	[disCode] [nvarchar](10) NULL,
	[disGender] [nvarchar](1) NULL,
 CONSTRAINT [aaaaaDisabilityVillage1_PK] PRIMARY KEY NONCLUSTERED 
(
	[disID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[DisabilityVillage] TO [pEnrolmentReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[DisabilityVillage] TO [pEnrolmentWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[DisabilityVillage] TO [pEnrolmentWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DisabilityVillage] TO [pEnrolmentWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[DisabilityVillage] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_DisabilityVillage_SSID] ON [dbo].[DisabilityVillage]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DisabilityVillage] ADD  CONSTRAINT [DF__Disability__ssID__5441852A]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[DisabilityVillage] ADD  CONSTRAINT [DF__Disabilit__disAg__5535A963]  DEFAULT ((0)) FOR [disAge]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Children who do not attend school because of disability. This appears on most survey forms.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisabilityVillage'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Survey 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisabilityVillage'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DisabilityVillage'
GO

