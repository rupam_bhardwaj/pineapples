SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherIdentityAudit](
	[tauditID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NOT NULL,
	[tauditDate] [datetime] NOT NULL,
	[tauditUser] [nvarchar](50) NULL,
	[tauditContext] [nvarchar](100) NULL,
	[tauditChanges] [xml] NULL,
 CONSTRAINT [PK_TeacherIdentityAudit] PRIMARY KEY CLUSTERED 
(
	[tauditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT INSERT ON [dbo].[TeacherIdentityAudit] TO [pineapples] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherIdentityAudit] TO [pTeacherRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherIdentityAudit] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chnages to key fields on TeacherIdentity are written by a trigger into this table. The changes field, before, after, are written in an XML field on the single record representing the edit.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentityAudit'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Audit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentityAudit'
GO

