SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PopulationModel](
	[popmodCode] [nvarchar](10) NOT NULL,
	[popmodName] [nvarchar](50) NULL,
	[popmodNameL1] [nvarchar](50) NULL,
	[popmodNameL2] [nvarchar](50) NULL,
	[popmodDesc] [ntext] NULL,
	[popmodSource] [nvarchar](200) NULL,
	[popmodDefault] [bit] NOT NULL,
	[popmodEFA] [bit] NOT NULL,
 CONSTRAINT [aaaaaPopulationModel1_PK] PRIMARY KEY NONCLUSTERED 
(
	[popmodCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[PopulationModel] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PopulationModel] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PopulationModel] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[PopulationModel] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PopulationModel] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[PopulationModel] ADD  CONSTRAINT [DF_PopulationModel_popmodDefault]  DEFAULT ((0)) FOR [popmodDefault]
GO
ALTER TABLE [dbo].[PopulationModel] ADD  CONSTRAINT [DF_PopulationModel_popmodEFA]  DEFAULT ((1)) FOR [popmodEFA]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pineapples can store more than one set of population data. Different sets are indicated by the populationmodel code. 
Only one population model can be used for EFA reporting. One model must also be designated as the default model. These don’t necessarily have to be the same.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PopulationModel'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Population' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PopulationModel'
GO

