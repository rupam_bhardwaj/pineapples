SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysCounters](
	[counterName] [nvarchar](20) NOT NULL,
	[counterValue] [int] NULL,
	[counterFormat] [nvarchar](20) NULL,
 CONSTRAINT [PK_sysCounters] PRIMARY KEY CLUSTERED 
(
	[counterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[sysCounters] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[sysCounters] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[sysCounters] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[sysCounters] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores next available value for various counters. Currently supports allocation of TPF number.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysCounters'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Utility' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysCounters'
GO

