SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpExternalDataSources](
	[edfCode] [nvarchar](20) NOT NULL,
	[edfType] [nvarchar](10) NULL,
	[edfFormat] [nvarchar](20) NULL,
	[edfOwnedID] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpExternalDataSources1_PK] PRIMARY KEY NONCLUSTERED 
(
	[edfCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpExternalDataSources] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpExternalDataSources] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpExternalDataSources] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpExternalDataSources] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpExternalDataSources] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parameter definition for External Data Sources. Used for import to TeacherIdentityExternal.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpExternalDataSources'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'External Data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpExternalDataSources'
GO

