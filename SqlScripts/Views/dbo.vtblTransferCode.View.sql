SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTransferCode]
AS
SELECT     TOP (100) TrfrCode, TrfrName
FROM         (SELECT     iCode AS TrfrCode, iName AS TrfrName, iCode AS SortKey
                       FROM          dbo.Islands
                       WHERE      ('Islands' IN
                                                  (SELECT     paramText
                                                    FROM          dbo.SysParams
                                                    WHERE      (paramName = 'TRANSFER_SPLIT')))
                       UNION
                       SELECT     dID AS TrfrCode, dName AS TrfrName, dName AS SortKey
                       FROM         dbo.Districts
                       WHERE     ('Districts' IN
                                                 (SELECT     paramText
                                                   FROM          dbo.SysParams AS SysParams_1
                                                   WHERE      (paramName = 'TRANSFER_SPLIT')))
                       UNION
                       SELECT     '<OS>' AS TrfrCode, 'Overseas' AS TrfrrName, 'ZZ' AS SortKey
                       FROM         dbo.metaNumbers
                       WHERE     (num = 1)
                       UNION
                       SELECT     TOP 100 PERCENT '<??>' AS TrfrCode, 'Unknown' AS TrfrrName, 'ZZZ' AS SortKey
                       FROM         dbo.metaNumbers AS metaNumbers_1
                       WHERE     (num = 1)) AS S
ORDER BY SortKey
GO
GRANT SELECT ON [dbo].[vtblTransferCode] TO [pSchoolRead] AS [dbo]
GO

