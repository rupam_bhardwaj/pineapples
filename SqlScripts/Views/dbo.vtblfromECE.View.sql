SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblfromECE]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptRow AS KinderName,
PupilTables.ptAge AS Age,
PupilTables.ptM AS M,
PupilTables.ptF AS F,
PupilTables.ptCode,
PupilTables.ptPage AS Seq
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='KINDER'))
GO

