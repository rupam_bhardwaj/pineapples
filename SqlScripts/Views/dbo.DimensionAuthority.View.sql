SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionAuthority]
AS
SELECT
authCode AS AuthorityCode,
authName AS Authority,
dbo.TRAuthorityType.codeCode AS AuthorityTypeCode,
dbo.TRAuthorityType.codeDescription AS AuthorityType,
dbo.TRAuthorityGovt.codeCode AS AuthorityGroupCode,
dbo.TRAuthorityGovt.codeDescription AS AuthorityGroup

FROM
	dbo.TRAuthorities
	INNER JOIN 	dbo.TRAuthorityType
		ON dbo.TRAuthorities.authType = dbo.TRAuthorityType.codeCode
	INNER JOIN dbo.TRAuthorityGovt
		ON dbo.TRAuthorityType.codeGroup = dbo.TRAuthorityGovt.codeCode
GO
GRANT DELETE ON [dbo].[DimensionAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[DimensionAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DimensionAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[DimensionAuthority] TO [public] AS [dbo]
GO

