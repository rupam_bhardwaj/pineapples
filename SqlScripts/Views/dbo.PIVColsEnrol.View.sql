SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BRian Lewis
-- Create date: 1 12 2013
-- Description:	Somali (Puntland) fee totals - note that we went from collecting the total to the charge per head in 2013
-- =============================================
CREATE VIEW [dbo].[PIVColsEnrol]
AS
Select ssID
, G.codeCode GenderCode
, G.codeDescription Gender
, L.codeCode LevelCode
, L.codeDescription  Level
                     ,
					  EducationLevels.codeCode AS edLevelCode, EducationLevels.codeDescription AS [Education Level],
					  EducationLevelsAlt.codeCode AS edLevelAltCode,	-- name changed to match jet
                      EducationLevelsAlt.codeDescription AS [Education Level Alt],
					  EducationLevelsAlt2.codeCode AS edLevelAlt2Code,  -- name changed to match jet
                      EducationLevelsAlt2.codeDescription AS [Education Level Alt2]

, case when G.codeCode = 'M' then M when G.CodeCode = 'F' then F end Enrol
FROM
(
	Select ssID
	, enLevel
	, sum(enM) M
	, sum(enF) F
	FROM Enrollments
	GROUP BY ssID, enM, enF, enLevel
) E
INNER JOIN TRLevels L
 ON L.codeCode = E.enLevel
INNER JOIN TREducationLevels EducationLevels
	ON L.lvlYear BETWEEN EducationLevels.edlMinYear AND EducationLevels.edlMaxYear
INNER JOIN TREducationLevelsAlt EducationLevelsAlt
	ON L.lvlYear BETWEEN EducationLevelsAlt.edlMinYear AND EducationLevelsAlt.edlMaxYear
INNER JOIN TREducationLevelsAlt2 EducationLevelsAlt2
	ON L.lvlYear BETWEEN EducationLevelsAlt2.edlMinYear AND EducationLevelsAlt2.edlMaxYear
CROSS JOIN TRGEnder G
GO

