SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EstablishmentFormulaSetup]
AS
select efGRoup as [Table Group]
, TB.efTable as [Table Name]
, TB.efbMin as [Min]
, TB.efbMax as [Max]
, RG.rgCode as [Role Grade Code]
, RG.rgDescription as [Grade Description]
, RG.roleCode as [RoleCode]
, RG.rgSalaryLevelMin as [Min Salary Level]
, RG.rgSalaryLevelMax as [Max Salary Level]
, A.efaAlloc [Quota]
from EFTAbles T
INNER JOIN EFTableBands TB
ON T.efTable = TB.efTable
 INNER JOIN efAllocations A
 ON TB.efbID = A.efbID
INNER JOIN TRRoleGRades RG
ON Rg.rgCode = A.rgCode
GO
GRANT SELECT ON [dbo].[EstablishmentFormulaSetup] TO [pEstablishmentRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EstablishmentFormulaSetup] TO [public] AS [dbo]
GO

