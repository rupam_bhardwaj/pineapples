SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qauditsubPrimaryClassTotals]
AS
SELECT PrimaryClass.ssID, PrimaryClassLevel.pclLevel,
Sum(PrimaryClassLevel.pclNum) AS PCTotal
FROM PrimaryClass INNER JOIN PrimaryClassLevel
ON PrimaryClass.pcID = PrimaryClassLevel.pcID
GROUP BY PrimaryClass.ssID, PrimaryClassLevel.pclLevel
GO

