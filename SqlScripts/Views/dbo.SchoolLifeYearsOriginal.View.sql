SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolLifeYearsOriginal]
AS
SELECT     LY.schNo, LY.schEst, LY.svyYear, LY.schClosed, SS.ssID AS ActualssID
FROM         (SELECT     dbo.Schools.schNo, dbo.Schools.schEst, dbo.Survey.svyYear, dbo.Schools.schClosed
                       FROM          dbo.Schools INNER JOIN
                                              dbo.Survey ON isnull(dbo.Schools.schEst,0) <= dbo.Survey.svyYear
												and schclosedlimit > Survey.svyYear

										LEFT OUTER JOIN
											SurveyControl SC on schools.schno = sc.schno and Survey.svyYear = sc.saYear

                       WHERE      (isnull(sc.saDormant,0) =0) ) AS LY LEFT OUTER JOIN
                      dbo.SchoolSurvey AS SS ON LY.schNo = SS.schNo AND LY.svyYear = SS.svyYear
UNION
-- if there is a survey in the year, pick it up, even if it is before the recorded est or after the close
Select Schools.schNo, Schools.schEst, SS.svyYear, Schools.schClosed, SS.ssID AS ActualssID
FROM          dbo.Schools
				INNER JOIN SchoolSurvey SS
					ON Schools.schNo = SS.schNo
				LEFT OUTER JOIN
					SurveyControl SC on schools.schno = sc.schno and SS.svyYear = sc.saYear

WHERE      (isnull(sc.saDormant,0) =0)
GO

