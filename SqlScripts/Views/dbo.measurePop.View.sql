SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--      Author: Brian Lewis
-- Create date: 2017
-- Description:
-- Fundamental list of population, by district if available
-- for the default population model
-- Used in the creation of data warehouse
-- =============================================
CREATE VIEW [dbo].[measurePop]
AS
Select popYear
, dID
, popAge
, popM
, popF
from Population P
	INNER JOIN PopulationModel PM
		ON P.popmodCode= PM.popModCode
WHERE PM.popmodDefault=1
GO

