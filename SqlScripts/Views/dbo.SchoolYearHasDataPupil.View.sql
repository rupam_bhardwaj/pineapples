SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SchoolYearHasDataPupil]
AS
SELECT DISTINCT
	 SS.ssID,
	 SS.svyYear,
	 SS.schNo,
	 PT.ptCode AS tdefCode,
	 dbo.SchoolSurveyQualityPupilTables.ssqLevel
FROM dbo.SchoolSurvey AS SS INNER JOIN
     dbo.PupilTables AS PT ON SS.ssID = PT.ssID
	 LEFT OUTER JOIN
     dbo.SchoolSurveyQualityPupilTables ON SS.ssID =
	 dbo.SchoolSurveyQualityPupilTables.ssID
    AND PT.ptCode =
    dbo.SchoolSurveyQualityPupilTables.ssqSubItem

WHERE (dbo.SchoolSurveyQualityPupilTables.ssqLevel IS NULL) OR
      (dbo.SchoolSurveyQualityPupilTables.ssqLevel = 0) OR
      (dbo.SchoolSurveyQualityPupilTables.ssqLevel = 1)
GO

