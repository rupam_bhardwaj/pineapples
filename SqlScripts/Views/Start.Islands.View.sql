SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Start].[Islands]
AS
Select
iCode		IslandID
, iName		Island
, dName		District
from lkpIslands
GO

