SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblPSA]
AS
SELECT
PupilTables.ssID,
PupilTables.ptPage AS presSeq,
PupilTables.ptRow AS presName,
PupilTables.ptAge AS Age,
PupilTables.ptF AS PSAF,
PupilTables.ptM AS PSAM
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='kinder'))
GO

