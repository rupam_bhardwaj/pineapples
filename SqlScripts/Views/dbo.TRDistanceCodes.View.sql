SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRDistanceCodes]
AS
Select convert(nvarchar(5), codeNum) codeCode,
codeDescription
FROM lkpDistanceCodes
GO

