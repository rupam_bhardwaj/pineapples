SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveyCore]
AS
SELECT [Survey ID]
      ,[Survey Data Year]
      ,[School No]
      ,[School Name]
      ,[SchoolID_Name]
      ,[SchoolID_Name_Type]
      ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      ,[School Class]
      , 1 NumSchools
  FROM [DimensionSchoolSurveyNoYear]
GO

