SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblSchoolLedger]
AS
SELECT
dbo.GeneralLedger.glCode,
dbo.GeneralLedger.glDesc,
dbo.GeneralLedger.glDbCr,
dbo.SchoolLedger.schNo,
dbo.SchoolLedger.svyYear,
dbo.SchoolLedger.[svyYear] + dbo.SchoolLedger.[schlgYearOffset] AS FinYear,
dbo.SchoolLedger.schlgView AS AccountView,
dbo.GeneralLedger.glGroup1,
dbo.GeneralLedger.glGroup2,
dbo.GeneralLedger.glGroup3,
dbo.GeneralLedgerDBCR.DBCRSign,

dbo.SchoolLedger.[schlgBudget] * dbo.GeneralLedgerDBCR.DBCRSign AS Budget,
dbo.SchoolLedger.[schlgActual] * dbo.GeneralLedgerDBCR.DBCRSign AS Actual,
(dbo.SchoolLedger.[schlgActual] * dbo.GeneralLedgerDBCR.DBCRSign) - (dbo.SchoolLedger.[schlgBudget] * dbo.GeneralLedgerDBCR.DBCRSign ) AS Variance,

dbo.SchoolLedger.schlgActual AS ActualUnsigned,
dbo.SchoolLedger.schlgBudget AS BudgetUnsigned

FROM dbo.GeneralLedger INNER JOIN dbo.SchoolLedger
ON GeneralLedger.glCode = SchoolLedger.schlgAccount
INNER JOIN dbo.GeneralLedgerDBCR ON
GeneralLedger.glCode = dbo.GeneralLedgerDBCR.glCode
GO

