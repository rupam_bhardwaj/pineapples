SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsSanitationSO]
AS
SELECT
	ssID,
	toiNum Number,


	toiYN as AreUsedYN,
	case when toiYN = 'Y' then 1 end AreUsed,

	lkpToiletTypes.ttypName AS [Type],
	Toilets.toiUse AS UsedBy
	-- yn is not included
FROM Toilets
INNER JOIN lkpToiletTypes
	ON lkpToiletTypes.ttypName = Toilets.toiType
GO

