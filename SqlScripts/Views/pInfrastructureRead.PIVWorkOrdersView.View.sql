SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInfrastructureRead].[PIVWorkOrdersView]
AS
SELECT WO.woID as ID
      ,woRef as [WO Ref]
      ,woStatus as [WO Status]
      , WOS.wosStage as [WO Stage]
      ,woDesc as [Description]
      ,woPlanned [Planned To Commence]
      , year(woPlanned) as [Planned Year]

      ,woBudget as [Approved Budget]
      ,woWorkApproved as [Approval Date]
      ,woFinanceApproved as [Finance Approval Date]
      ,woSourceOfFunds as [Source of Funds]
      ,woDonorManaged as [DonorManaged]
      ,woCostCentre as [CostCentre]
      ,woTenderClose as [TenderCloseDate]
      ,year(woTenderClose) as [TenderCloseYear]
      ,woContracted as [ContractedDate]
       ,year(woContracted) as [ContractedYear]
      ,supCode as [SupplierCode]
      ,woContractValue as [ContractValue]

      ,woCommenced as [CommencedDate]
       ,year(woCommenced) as [CommencedYear]
      ,woPlannedCompletion as [PlannedCompletionDate]
      ,year(woPlannedCompletion) as [PlannedCompletionYear]
     ,woCompleted as [CompletedDate]
      ,year(woCompleted) as [CompletedYear]
     ,woInvoiceValue as [InvoiceValue]

-- item totals com frm the subquery

	, ITOT.EstCost
	, ITOT.NumITems
	, ITOT.NumSchools

	, EstCostNewBldg
	, EstCostMaintenance
	, EstCostReplacement
	, EstCostWaterSanitation
	, EstCostFurniture
	, EstCostManagement
	, EstCostOther

	, NumSchoolsNewBldg
	, NumSchoolsMaintenance
	, NumSchoolsReplacement
	, NumSchoolsWaterSanitation
	, NumSchoolsFurniture
	, NumSchoolsManagement
	, NumSchoolsOther


	, ITOT.NotCommenced
	, ITOT.NotCommencedEstCost
	, ITOT.NotCommencedContractValue

	, ITOT.InProgress
	, ITOT.InProgressEstCost
	, ITOT.InProgressContractValue

	, ITOT.Completed
	, ITOT.CompletedEstCost
	, ITOT.CompletedContractValue

  FROM WorkOrders WO
	LEFT JOIN
		(
		Select woID
		, sum(witmEstCost) as EstCost
		, count(witmID) as NumItems
		, count(DISTINCT schNo) as NumSchools

		, sum(case when witGroup = 'N' then witmEstCost else null end)  EstCostNewBldg
		, sum(case when witGroup = 'M' then witmEstCost else null end)  EstCostMaintenance
		, sum(case when witGroup = 'R' then witmEstCost else null end)  EstCostReplacement
		, sum(case when witGroup = 'W' then witmEstCost else null end)  EstCostWaterSanitation
		, sum(case when witGroup = 'F' then witmEstCost else null end)  EstCostFurniture
		, sum(case when witGroup = 'O' then witmEstCost else null end)  EstCostManagement
		, sum(case when witGroup not in ('N','M','R','W','F','O')  then witmEstCost else null end)  EstCostOther


		, count(DISTINCT case when witGroup = 'N' then schNo else null end)  NumSchoolsNewBldg
		, count(DISTINCT case when witGroup = 'M' then schNo else null end)  NumSchoolsMaintenance
		, count(DISTINCT case when witGroup = 'R' then schNo else null end)  NumSchoolsReplacement
		, count(DISTINCT case when witGroup = 'W' then schNo else null end)  NumSchoolsWaterSanitation
		, count(DISTINCT case when witGroup = 'F' then schNo else null end)  NumSchoolsFurniture
		, count(DISTINCT case when witGroup = 'O' then schNo else null end)  NumSchoolsManagement
		, count(DISTINCT case when witGroup not in ('N','M','R','W','F','O')  then witmEstCost else null end)  NumSchoolsOther


		, sum(case  when witmProgress is null then 1 else null end) NotCommenced
		, sum(case  when witmProgress is null then witmEstCost else null end) NotCommencedEstCost
		, sum(case  when witmProgress is null then witmContractValue else null end) NotCommencedContractValue

		, sum(case witmProgress when 'IP' then 1 else 0 end) InProgress
		, sum(case witmProgress when 'IP' then witmEstCost else 0 end) InProgressEstCost
		, sum(case witmProgress when 'IP' then witmContractValue else 0 end) InProgressContractValue

		, sum(case when witmProgress in ('COM','DP', 'INSP') then 1  else 0 end) Completed
		, sum(case when witmProgress in ('COM','DP', 'INSP') then witmEstCost  else 0 end) CompletedEstCost
		, sum(case when witmProgress in ('COM','DP', 'INSP') then witmContractValue else 0 end) CompletedContractValue

		FROM WorkITems
			LEFT JOIN lkpWorkItemType WIT
				ON WorkITems.witmType = WIT.codeCode
		GROUP BY woID
		) ITOT
		ON WO.woID = ITOT.woID
	LEFT JOIN lkpWorkOrderStatus WOS
		ON WO.woStatus = WOS.codeCode
GO

