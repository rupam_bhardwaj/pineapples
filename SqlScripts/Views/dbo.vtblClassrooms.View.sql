SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblClassrooms]
AS

SELECT
Rooms.rmID,
Rooms.ssID,
Rooms.rmType,
Rooms.rmNo,
Rooms.rmSchLevel,
Rooms.rmSize,
Rooms.rmYear,
Rooms.rmMaterials,
Rooms.rmCondition,
Rooms.rmLevel
FROM dbo.Rooms
WHERE (((Rooms.rmType)='CLASS'))
GO

