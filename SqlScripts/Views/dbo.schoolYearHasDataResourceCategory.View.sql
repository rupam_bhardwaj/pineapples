SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataResourceCategory]
AS
SELECT     DISTINCT S.schNo, S.svyYear,S.ssID,R.resName,   Q.ssqLevel

FROM         dbo.SchoolSurvey AS S
			INNER JOIN Resources R
			on S.SSID = R.ssID
			LEFT OUTER JOIN
             dbo.tfnQualityIssues('Facilities', null) AS Q
				ON R.ssID = Q.ssID and R.resName = Q.ssqSubitem
WHERE     (q.ssqLevel is null or q.ssqLevel <2)
GO

