SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Teacher Count Audit
--
-- Analyses the quality and prevenance of data in the TeacherLocation table.
-- this is based on the field Source in warehouse.TeacherLocation:

-- Source

-- when both survey and appt present for a given year:
--	C:    confirmed - survey confirms appointment
--  CE:   survey confirms appt, but survey value is estimate
--  XS:   conflict - use survey - survey differs from appt and survey is not estimate;
--		  ie the teacher is reported at a different school to their appointment
--  XA:   conflict but survey is estimate so use appt
--  XA?:  conflict but survey is estimate so use appt
--		  ? iindicates the appt school returned a survey and the teacher was not on it
--  XE:   conflict , using survey ( e.g. survey, even though estimate, is more recent than the appointment)

-- when only only appointment:
--  A:    appointment
--  A?:	  using appointment, but appt school returned survey and teacher not on it

--  when only survey
--  S:    survey
--  E:    survey - estimate
--
-- =============================================
CREATE VIEW [warehouse].[TeacherCountAudit]
AS
Select SurveyYear
, count(*) NumTeachers
, sum (case when Source = 'S' then 1 end ) S
, sum (case when Source = 'XS' then 1 end ) XS
, sum (case when Source = 'C' then 1 end ) C
, sum (case when Source in ('S','XS', 'C') then 1 end ) Actual
, sum (case when Source = 'E' then 1 end ) E
, sum (case when Source = 'XE' then 1 end ) XE
, sum (case when Source = 'CE' then 1 end ) CE
, sum (case when Source in ('E','XE', 'CE', 'XA', 'XA?') then 1 end ) Estimate
, sum (case when Source = 'A' then 1 end ) A
, sum (case when Source = 'XA' then 1 end ) XA
, sum (case when Source = 'A?' then 1 end ) [A?]
, sum (case when Source = 'XA?' then 1 end ) [XA?]
, sum (case when Source in ('A', 'A?') then 1 end ) Appt
, sum(XtraSurvey) Duplicates
from warehouse.TeacherLocation
GROUP by SurveyYear
GO

