SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsRubbish]
AS
Select ssID
, ssRubbishFreq FreqCode
, RF.codeDescription Frequency
, ssRubbishMethod MethodCode
, RM.codeDescription Method
FROM SchoolSurvey
	LEFT JOIN TRRubbishDisposal RM
		ON ssRubbishMethod = RM.codeCode
	LEFT JOIN TRCodesMisc RF
		ON ssRubbishFreq= RF.codeCode
		AND RF.codeType = 'TIMEPERIOD'
GO

