SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 04 2010
-- Description:	create a new teachersurvey, using either a previous survey, and/or an apointment
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[CreateTeacherSurveyEx]
	-- Add the parameters for the stored procedure here
	@TeacherID int ,
	@SchoolNo nvarchar(50) ,
	@SurveyYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @SurveyID int
declare @TSSource int
declare @AppointmentID int

declare @YearEnd datetime
declare @YearStart datetime
declare @SchoolType nvarchar(10)

Select @SurveyID = ssID
, @SchoolType = ssSchType
FROM SchoolSurvey
WHERE schNo = @SchoolNo
	AND svyYEar = @SurveyYear

SELECT TOP 1 TS.*
INTO #ts
FROM TeacherSurvey TS
INNER JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
WHERE tID = @TeacherID
ORDER BY svyYear DESC

-- also check if there is an appointment
-- at the school
Select @YearStart = common.dateserial(@SurveyYear,1,1)
Select @YearEnd = common.dateserial(@SurveyYear,12,31)

SELECT TOP 1 A.*
, roleCode
INTO #Appt
FROM TeacherAppointment A
	INNER JOIN Establishment E
		ON A.estpNo = E.estpNo
	INNER JOIN RoleGRades RG
		ON E.estpRoleGRade = RG.rgCode
WHERE tID = @TeacherID
	AND E.schNo = @SchoolNo
	AND taDate <= @YearEnd
		AND (TaEndDate >= @YearStart or taEndDate is null)
ORDER BY TaDate DESC

-- can we derive the default sector?
--
    INSERT INTO TeacherSurvey
           (ssID
           ,tchSort
           ,tchEmplNo  ,tchRegister ,tchProvident
           ,tchUnion
           ,tchNamePrefix ,tchFirstName ,tchMiddleNames,tchFamilyName,tchNameSuffix
           ,tchSalaryScale, tchSalary
           ,tchDOB ,tchGender
           ,tchCitizenship,tchIsland ,tchDistrict
           ,tchSponsor
           ,tchFTE,tchFullPart
           ,tchTrained,tchEdQual ,tchQual
           ,tchSubjectTrained
           ,tchCivilStatus,tchNumDep
           ,tchHouse
           ,tchSubjectMajor,tchSubjectMinor,tchSubjectMinor2
           ,tchClass,tchClassMax
           ,tchJoint,tchComposite
           ,tchStatus
           ,tchRole
           ,tchTAM
           ,tID
           ,tchECE
           ,tchInserviceYear
           ,tchInservice
           ,tchSector
           ,tchYearStarted
           ,tchSpouseFirstName
           ,tchSpouseFamilyName
           ,tchSpouseOccupation
           ,tchSpouseEmployer
           ,tchSpousetID
           ,tchLangMajor
           ,tchLangMinor
			)
    Select
           @surveyID
           ,tchSort
           ,tPayroll ,tRegister, tProvident
           ,tchUnion
           ,tNamePrefix ,tGiven ,tMiddleNames,tSurname ,tNameSuffix
           , tchSalaryScale , tchSalary
           ,tDOB ,tSEx
           ,tchCitizenShip,tchIsland,tchDistrict
           ,tchSponsor
           ,tchFTE,tchFullPart
           ,tchTrained,tchEdQual,tchQual
           ,tchSubjectTrained
           ,tchCivilStatus,tchNumDep
           ,tchHouse
           ,tchSubjectMajor,tchSubjectMinor ,tchSubjectMinor2
           ,tchClass,tchClassMax
           ,0, CASE WHEN tchClass <> tchClassMax then 1 else 0 end
           ,tchStatus
           ,isnull(A.roleCode,tchRole)
           ,tchTAM
           ,@TeacherID
           ,tchECE
           ,tchInserviceYear
           ,tchInservice
           ,tchSector
           ,tchYearStarted
           ,tchSpouseFirstName
           ,tchSpouseFamilyName
           ,tchSpouseOccupation
           ,tchSpouseEmployer
           ,tchSpousetID
           ,tchLangMajor
           ,tchLangMinor
     FROM
		TeacherIdentity TI

		LEFT JOIN #ts TS
			ON TS.tID = TI.tID
		LEFT JOIN #Appt A
			ON TI.tID = A.tID
		WHERE TI.tID = @TeacherID

DECLARE @TEacherSurveyID int
SELECT @TEacherSurveyID =  scope_identity()

UPDATE TEacherSurvey
SET tchSector = CalcSector
FROM TeacherSurvey TS
CROSS APPLY pTEacherRead.TEacherSurveySectorAudit(tchsID) AU
WHERE CalcSector is not null
AND TS.tchsID = @TEacherSurveyID

SELECT *
FROM TEacherSurvey
WHERE tchsID =@TEacherSurveyID

END
GO

