SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[xmlTeachers]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50),
	@SurveyYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- can use a simpler approach, becuase these records are much closer to a recordet; ie 1 node per teacher

declare @TeachersXML xml
select @TeachersXML =
(
Select
	1 as Tag,
	null as Parent,
		tchsID as [Teacher!1!SurveyID],
		tchsmisID as [Teacher!1!SMISID],
		TI.tID as [Teacher!1!PineapplesID],
		tchFamilyName as [Teacher!1!FamilyName],
		tchFirstName as [Teacher!1!GivenName],
		tchDOB as [Teacher!1!DOB],
		tchGender as [Teacher!1!Sex],
		tchCivilStatus as [Teacher!1!MaritalStatus],
		tchNumDep as [Teacher!1!NumDependentChild],
		tchCitizenship as [Teacher!1!Nationality],
		tchLangMajor as [Teacher!1!LanguageMajor],
		tchLangMinor as [Teacher!1!LanguageMinor],
		tchIsland as [Teacher!1!HomeIsland],
		tchDistrict as [Teacher!1!HomeDistrict],
-- spouse


		tchSpousetID as [Teacher!1!spousetID],
		tchSpouseFamilyName as [Teacher!1!spouseFamilyName],
		tchSpouseFirstName as [Teacher!1!spouseGivenName],
		tchSpouseOccupation as [Teacher!1!spouseOccupation],
		tchSpouseEmployer as [Teacher!1!spouseEmployer],

--employment

		tchFullPart as [Teacher!1!FullPart],
		tchFTE as [Teacher!1!FTE],
		tchSponsor as [Teacher!1!SalaryPaidBy],
		tchEmplNo as [Teacher!1!EmploymentNo],
		tchProvident as [Teacher!1!ProvidentFundNo],
		tchSalaryScale as [Teacher!1!SalaryScale],
		tchSalary as [Teacher!1!Salary],
		tchHouse as [Teacher!1!HouseProvided],
-- duties
		tchRole as [Teacher!1!Role],
		tchTAM as [Teacher!1!TeachingDuty],
		tchSubjectMajor as [Teacher!1!SubjectMajor],
		tchSubjectMinor as [Teacher!1!SubjectMinor],
		tchSubjectMinor2 as [Teacher!1!SubjectMinor2],
		tchClass as [Teacher!1!LevelTaughtMin],
		tchClassMax as [Teacher!1!LevelTaughtMax],
--qualifications
		tchEdQual as [Teacher!1!EdQual] ,
		tchSubjectTrained as [Teacher!1!SubjectTrained],
		tchQual as [Teacher!1!Qual],
		tchRegister as [Teacher!1!RegistrationNo],
		tchYearStarted as [Teacher!1!YearFirstTeaching],
		tchYears as [Teacher!1!YearsTeaching],
		tchYearsSchool as [Teacher!1!YearsAtSchool],
		tchInserviceYear as [Teacher!1!YearLastInservice],
		tchInService as [Teacher!1!LastInserviceSubject],
		tchTrained as [Teacher!1!Accreditation]

from TeacherSurvey Teacher
	inner join SchoolSurvey SS
	on Teacher.ssID = SS.ssID
	inner join TeacherIdentity TI
	on Teacher.tID = TI.tID
	WHERE SchNo = @schNo
		and svyYear = @SurveyYear
	for xml explicit
)

-- use xml path to p[ut the parent node around
Select @TeachersXML =
(
Select @TeachersXML
for XML PAth('Teachers')
)
-- select the XML result to return as a resultset

select @TeachersXML
END
GO
GRANT EXECUTE ON [dbo].[xmlTeachers] TO [pTeacherRead] AS [dbo]
GO

