SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 06 2009
-- Description:	Log key system activities
-- =============================================
CREATE PROCEDURE [dbo].[LogActivity]
	-- Add the parameters for the stored procedure here
	@Task nvarchar(100) ,
	@Reference int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try
	INSERT INTO ActivityLog
	(	ActivityTime
		, ActivityLogin
		, ActivityTask
		, ActivityReference
	)
	Select getdate(), system_user, @task, @Reference
end try

begin catch
end catch


END
GO
GRANT EXECUTE ON [dbo].[LogActivity] TO [public] AS [dbo]
GO

