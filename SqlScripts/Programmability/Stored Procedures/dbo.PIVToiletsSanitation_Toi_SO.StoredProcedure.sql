SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 10 2013
-- Description:	sets up temp table
-- called from PIVToiletsSanitation
--		calls PIVToiletsSanitaton_EXEC
-- =============================================
CREATE PROCEDURE [dbo].[PIVToiletsSanitation_Toi_SO]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'Toi SO'
   Select *
   INTO #tmpPIVColsToilets
   FROM PIVColsToiletsSO
		WHERE [Type] in
			(Select ttypName from lkpToiletTypes WHERE ttypGroup = @group)

	exec dbo.PIVToiletsSanitation_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

