SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[_test1]
	-- Add the parameters for the stored procedure here
	@ReportYEar int = null
	, @UseCache int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @xml xml
declare @rdID int

If (@UseCache = 1) begin
	Select @rdID = 	rdID
		FROM ReportData
		WHERE rdName = 'ER'
		AND (isnull(rdYear,0) = isnull(@ReportYear,0))
	if @rdID is not null begin
		SELECT *
		FROM ReportData
		WHERE rdID = @rdID

		return
	end
end

--- logic to build the cache

	declare @efa table
(
	schNo nvarchar(50),
	svyYear int,
	surveyDimensionssID int,

	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	RepM int,
	RepF int,
	EnrolNYM int,
	EnrolNYF int,
	RepNYM int,
	RepNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,
	[Age of Data NY] int
)
INSERT INTO @efa
EXEC sp_EFACORE 1, @ReportYear,0


Select @xml =
(
	Select
	Tag
	, Parent
	, stCode [SchoolType!1!c]
	, dID [District!2!c]
	, schNo [S!3!n]
	, surveyYear [Y!4!y]
	, LevelCode [L!5!c]
	, EnrolM [L!5!eM]
FROM
(
Select
1 Tag
, null Parent
, stCode
, stDescription
, null dID
, null dName
, null schNo
, null schName
, null surveyYear
, null levelCode
, null levelName
, null EnrolM
, null EnrolF
, null RepM
, null RepF
, null EnrolNYM
, null EnrolNYF
, null RepNYM
, null RepNYF
, null EnrolNYNextLevelM
, null EnrolNYNextLevelF
, null RepNYNextLevelM
, null RepNYNextLevelF
, null Estimate

FROM SchoolTypes
UNION
Select
2 Tag
, 1 Parent
, stCode
, stDescription
, dID
, dNAme
, null schNo
, null schName
, null surveyYear
, null levelCode
, null levelName
, null EnrolM
, null EnrolF
, null RepM
, null RepF
, null EnrolNYM
, null EnrolNYF
, null RepNYM
, null RepNYF
, null EnrolNYNextLevelM
, null EnrolNYNextLevelF
, null RepNYNextLevelM
, null RepNYNextLevelF
, null Estimate

FROM SchoolTypes
CROSS JOIN Districts
UNION
SELECT
3 Tag
, 2 Parent
, SchoolTypeCode
, SchoolType
, [District Code] dID
, District dName
, E.schNo
, null schName
, null surveyYear
, null levelCode
, null levelName
, null EnrolM
, null EnrolF
, null RepM
, null RepF
, null EnrolNYM
, null EnrolNYF
, null RepNYM
, null RepNYF
, null EnrolNYNextLevelM
, null EnrolNYNextLevelF
, null RepNYNextLevelM
, null RepNYNextLevelF
, null Estimate

FROM @efa E
INNER JOIN DimensionSchoolSurveyNoYEar DSS
ON E.surveyDimensionssID = DSS.[Survey Id]
UNION
SELECT
4 Tag
, 3 Parent
, SchoolTypeCode
, SchoolType
, [District Code] dID
, District dName
, E.schNo
, null schName
, E.svyYear surveyYear
, null levelCode
, null levelName
, null EnrolM
, null EnrolF
, null RepM
, null RepF
, null EnrolNYM
, null EnrolNYF
, null RepNYM
, null RepNYF
, null EnrolNYNextLevelM
, null EnrolNYNextLevelF
, null RepNYNextLevelM
, null RepNYNextLevelF
, null Estimate


FROM @efa E
INNER JOIN DimensionSchoolSurveyNoYEar DSS
ON E.surveyDimensionssID = DSS.[Survey Id]

UNION
SELECT
5 Tag
, 4 Parent
, SchoolTypeCode
, SchoolType
, [District Code] dID
, District dName
, E.schNo
, null schName
, E.svyYear surveyYear
, levelCode
, null levelName
, EnrolM
, EnrolF
, RepM
, RepF
, EnrolNYM
, EnrolNYF
, RepNYM
, RepNYF
, EnrolNYNextLevelM
, EnrolNYNextLevelF
, RepNYNextLevelM
, RepNYNextLevelF
, Estimate
FROM @efa E
INNER JOIN DimensionSchoolSurveyNoYEar DSS
ON E.surveyDimensionssID = DSS.[Survey Id]
) U
ORDER BY stCode,dID, schNo, surveyYear

FOR XML EXPLICIT
)


declare @date datetime = getdate()
Select @xml
=
(Select
@date [@date]
, @xml
FOR XML PATH('Data')
)

DELETE FROM ReportData
WHERE
	rdName = 'ER'
	AND rdYear = @reportYear

INSERT INTO ReportData
(
 rdName
 , rdYear
 , rdCalculated
 , rdData
 )
 VALUES ('ER', isnull(@ReportYear,0), @date, @xml)

	Select *
		FROM ReportData
		WHERE rdName = 'ER'
		AND (isnull(rdYear,0) = isnull(@ReportYear,0))


END
GO

