SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TeacherIdentityAuditReorg]
AS
BEGIN

begin transaction

INSERT INTO audit.auditLog
(
  auditTable
, auditUser
, auditDateTime
, auditContext
, auditAction
, auditAffected
)
Select DISTINCT

 'TeacherIdentity'
, taudituser
, tauditDate
, tauditContext
, 'U'
, count(tID)
from TeacherIdentityAudit
group by
 taudituser
, tauditDate
, tauditContext

Select * from audit.AuditLog
WHERE auditAffected > 1
AND auditTable = 'TeacherIdentity'

INSERT INTO audit.auditRow
(
	auditID
	, arowKey
)
Select
auditID
, tID
from audit.auditLog L
INNER JOIN TeacherIdentityAudit TIA
ON L.auditUser = TIA.tauditUser
AND L.auditDateTime = TIA.tauditDate
AND isnull(auditContext,'') = isnull(tauditContext,'')


INSERT INTO audit.auditColumn
(
	arowID
	, acolName
	, acolBefore
	, acolAfter
)
SELECT
	arowID
	, colName
	, beforeValue
	, afterValue
FROM
	audit.AuditLog L
	INNER JOIN Audit.auditRow R
	ON L.auditID = R.auditID
	INNER JOIN TeacherIdentityAudit TIA
		ON TIA.tID = R.arowKey
		AND L.auditUser = TIA.tauditUser
		AND L.auditDateTime = TIA.tauditDate
		AND isnull(auditContext,'') = isnull(tauditContext,'')
	INNER JOIN
	(
		Select
		tauditID
		, tID
		, tauditChanges
		, T.c.value('./@Field','nvarchar(50)') ColName
		, T.c.value('./@before','nvarchar(50)') beforeValue
		, T.c.value('./@after','nvarchar(50)') afterValue

		from TeacherIdentityAudit
		CROSS APPLY tAuditCHanges.nodes('Edits/Edit') As T(c)
	) TCols
	ON TIA.tauditID = TCols.tauditID
EXEC common.auditLogFilter 0,'TeacherIdentity'

commit transaction

end
GO

