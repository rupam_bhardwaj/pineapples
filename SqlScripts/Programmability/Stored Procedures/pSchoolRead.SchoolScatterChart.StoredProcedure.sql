SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 05 2010
-- Description:	School scatter chart
-- june 2014: support kml export
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolScatterChart]
	-- Add the parameters for the stored procedure here
	@BaseYear int = 0,
	@XSeries nvarchar(50),
	@XSeriesOffset int,
	@YSeries nvarchar(50),
	@YSeriesOffset int,
	@xArg1 nvarchar(50) = null,
	@xArg2 nvarchar(50) = null,
	@yArg1 nvarchar(50) = null,
	@yArg2 nvarchar(50) = null,
	@SchoolType nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@District nvarchar(10) = null,
	@SchoolNo nvarchar(10) = null,
	@kml nvarchar(max) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @XData TABLE
(
  schNo nvarchar(50)
, XValue float
, Estimate int
, XQuality int
)


DECLARE @YData TABLE
(
  schNo nvarchar(50)
, YValue float
, Estimate int
, YQuality int
)

DECLARE @scatter TABLE
(
  schNo nvarchar(50)
, schName nvarchar(50)
, XValue float
, YValue float
, XEstimate int
, YEstimate int
, XQuality int
, YQuality int

, authCode nvarchar(10)
, authName nvarchar(100)
, AuthorityGroupCode nvarchar(10)
, AuthorityGroup nvarchar(100)

, schType nvarchar(10)
, stDescription nvarchar(100)
, dID nvarchar(10)
, District nvarchar(50)
, langCode nvarchar(10)
, langName nvarchar(50)
-- 15 10 2014 added lat and long
, schLat decimal(12,8)
, schLong decimal(12,8)

-- calculate3d values
, YonX float
, XQ float
, YQ float
, YonXQ float
, YonXQuality int
, PtSelected int
)

-- decode the series identifier to get the stored proc

Select @Xseries = case @XSeries
		when 'Enrolment' then 'pSchoolRead.SchoolDataSetEnrolment'
		when 'Repeaters' then 'pSchoolRead.SchoolDataSetRepeaterCount'
		when 'Teachers' then 'pSchoolRead.SchoolDataSetTeacherCount'
		when 'TeachersQualCert' then 'pSchoolRead.SchoolDataSetQualCertCount'
		when 'TeachersQualCertPerc' then 'pSchoolRead.SchoolDataSetQualCertPerc'
		else @XSeries
		end


Select @YSeries = case @YSeries
		when 'Enrolment' then 'pSchoolRead.SchoolDataSetEnrolment'
		when 'Repeaters' then 'pSchoolRead.SchoolDataSetRepeaterCount'
		when 'Teachers' then 'pSchoolRead.SchoolDataSetTeacherCount'
		when 'TeachersQualCert' then 'pSchoolRead.SchoolDataSetTeacherQualCertCount'
		when 'TeachersQualCertPerc' then 'pSchoolRead.SchoolDataSetTeacherQualCertPerc'
		else @YSeries
		end

declare @YearArg int

declare @SQL nvarchar(400)
------ set up the XData

Select @YearArg = @BaseYear + @XSeriesOffset
SELECT @SQL = 'exec ' + @XSeries + ' ' + cast(@YearArg as nvarchar(4))
		+ ', ' + case when @SchoolNo is null then 'NULL' else quotename(@SchoolNo,'''') end
		+ case when @xArg1 is null then
			case when @xArg2 is null then '' else ', NULL' end
		  else ', ' + quotename(@xArg1,'''')  end
		+ case when @xArg2 is null then '' else ', ' + quotename(@xArg2,'''') end

INSERT INTO @XData
EXEC sp_executesql @SQL

--- the y data
Select @YearArg = @BaseYear + @YSeriesOffset
SELECT @SQL = 'exec ' + @YSeries + ' ' + cast(@YearArg as nvarchar(4))
		+ ', ' + case when @SchoolNo is null then 'NULL' else quotename(@SchoolNo,'''') end
		+ case when @yArg1 is null then
			case when @yArg2 is null then '' else ', NULL' end
		  else ', ' + quotename(@yArg1,'''')  end
		+ case when @yArg2 is null then '' else ', ' + quotename(@yArg2,'''') end


INSERT INTO @YData
exec sp_executesql @SQL

--combine these into the main stagiung table
INSERT INTO @scatter
(
schNo
, schName

, XValue
, YValue
, XEstimate
,  YEstimate
, XQuality
, YQuality
,  authCode
, authName
, AuthorityGroupCode
, AuthorityGroup
,  schType
, stDescription
, dID
, District
, langCode
, langName
, schLat
, schLong
, YonX
, PtSelected
)
Select
S.schNo
, s.schName

, X.XValue
, Y.YValue
, X.Estimate XEstimate
, Y.Estimate YEstimate
, X.XQuality
, Y.YQuality
, isnull(SYH.syAuth, S.schAuth) authCode
, AUTH.Authority
, AuthorityGroupCode
, AuthorityGroup
, isnull(SYH.systCode, S.schType) schType
, ST.stDescription
, dID
, dName District
, schLang
, LangName
-- lat and long 15 10 2014
, S.schLat
, S.schLong

, case when isnull(XValue,0) = 0 then null else YValue / XValue end YonX
, 0
from Schools S
LEFT JOIN @XData X
	ON S.schNo = X.schNo
LEFT JOIN @YData Y
	ON S.schNo = Y.schNo
LEFT JOIN SchoolYearHistory SYH
	ON SYH.schNo = S.schNo
	AND SYH.syYear = @BaseYear
LEFT JOIN DimensionAuthority AUTH
	ON isnull(SYH.syAuth,S.schAuth) = AUTH.authorityCode

LEFT JOIN TRSchoolTypes ST
	ON isnull(SYH.systCode, S.schType) = ST.stCode
LEFT JOIN Islands I
	ON I.iCode = S.iCode
LEFT JOIN Districts D
	ON I.iGRoup = D.dID
LEFT JOIN TRLanguage LNG
	ON S.schLang= LNG.langCode
WHERE (XValue is not null or YValue is not null)
AND (dID = @District or @District is null)
AND (isnull(SYH.syAuth,S.schAuth) = @Authority or @Authority is null)
AND (isnull(SYH.systCode,S.schType) = @SchoolType or @SchoolType is null)

ORDER BY schNo, XValue, YValue


declare @avgX float
declare @stdevX float
declare @minX float
declare @maxX float
declare @medianX float
declare @Q1X float
declare @Q3X float
declare @avgY float
declare @stdevY float
declare @minY float
declare @maxY float
declare @medianY float
declare @Q1Y float
declare @Q3Y float

declare @avgYonX float
declare @stdevYonX float
declare @minYonX float
declare @maxYonX float
declare @medianYonX float
declare @Q1YonX float
declare @Q3YonX float


;
with X as
(
	Select *
	, row_number() OVER (ORDER BY XValue, schNo) UPX
	, row_number() OVER (ORDER BY XValue DESC, schNo DESC ) DOWNX
	, row_number() OVER (ORDER BY YValue, schNo) UPY
	, row_number() OVER (ORDER BY YValue DESC, schNo DESC ) DOWNY
	from @Scatter
)
Select @avgX = avg(XValue)
, @stdevX = stdev(XValue)
, @minX = min(XValue)
, @maxX = max(XValue)
, @medianX = avg(case when (UPX - DOWNX) between -1 and 1 then XValue else null end)
, @Q1X = avg(case when (3*UPX - DOWNX) between -3 and 3 then XValue else null end)
, @Q3X = avg(case when (3*DOWNX - UPX) between -3 and 3 then XValue else null end)

, @avgY = avg(YValue)
, @stdevY = stdev(YValue)
, @minY = min(YValue)
, @maxY= max(YValue)
, @medianY = avg(case when (UPY - DOWNY) between -1 and 1 then YValue else null end)
, @Q1Y = avg(case when (3*UPY - DOWNY) between -3 and 3 then YValue else null end)
, @Q3Y= avg(case when (3*DOWNY - UPY) between -3 and 3 then YValue else null end)
from X


;
WITH X AS
(
	Select YonX
	, row_number() OVER (ORDER BY YonX, schNo) UP
	, row_number() OVER (ORDER BY YonX DESC, schNo DESC ) DOWN
	from @Scatter
		WHERE isnull(XValue,0) <> 0

)
Select @avgYonX = avg(YonX)
, @stdevYonX = stdev(YonX)
, @minYonX = min(YonX)
, @maxYonX = max(YonX)
, @medianYonX = avg(case when (UP - DOWN) between -1 and 1 then YonX else null end)
, @Q1YonX = avg(case when (3*UP - DOWN) between -3 and 3 then YonX else null end)
, @Q3YonX = avg(case when (3*DOWN - UP) between -3 and 3 then YonX else null end)
from X

declare @IQX float		-- interquartile range
declare @IQY float		-- interquartile range
declare @IQYonX float


Select @IQX = @Q3X - isnull(@Q1X,0)
Select @IQY = @Q3Y - isnull(@Q1Y,0)
Select @IQYonX = @Q3YonX - isnull(@Q1YonX,0)


UPDATE @scatter
SET XQ =
 case when @IQX = 0 then 0
		when XValue > @Q3X then (XValue - @Q3X) / @IQX
		when XValue < @Q1X then (@Q1X - XValue) / @IQX
		else null
	end
, YQ = case when @IQY = 0 then 0
		 when YValue > @Q3Y then (YValue - @Q3Y) / @IQY
		when YValue < @Q1Y then (@Q1Y - YValue) / @IQY
		else null
	end
, YonXQ = case when @IQYonX = 0 then 0
		when YonX > @Q3YonX then (YonX - @Q3YonX) / @IQYonX
		when YonX < @Q1YonX then (@Q1YonX - YonX) / @IQYonX
		else null
	end
, YonXQuality =
	case when YQuality is null and XQuality is null then null
		when YQuality = 2 or XQuality = 2 then 	2
		when YQuality = 1 or XQuality = 1 then 1
		else  0
  end


--------------------------------------------------------------------------------
-- 18 10 2014 (caulfield cup!)
-- support fo kml with the @kml flag
if (@kml is null) begin
	-- first recordset is the data
	SELECT *
	, row_Number() OVER (ORDER BY schNo) - 1 PtIndex
	from @scatter
	ORDER By schNo

	-- second is the statistical values
	SELECT
	case when sum((XValue - @AvgX)*(XValue - @AvgX)) *sum((YValue - @avgY)*(YValue - @avgY)) =0 then null
	else
	sum((XValue - @AvgX)*(YValue - @avgY))
	/ power
		(sum((XValue - @AvgX)*(XValue - @AvgX)) *sum((YValue - @avgY)*(YValue - @avgY))
		, .5)
	end correlation
	, @avgX XAvg
	, @avgY YAvg
	, @avgYonX YonXAvg

	, @stdevX Xstdev
	, @stdevY Ystdev
	, @stdevYonX YonXstdev

	, @minX Xmin
	, @minY Ymin
	, @minYonX YonXmin

	, @maxX Xmax
	, @maxY Ymax
	, @maxYonX YonXmax

	, @medianX Xmedian
	, @medianY Ymedian
	, @medianYonX YonXmedian

	, @Q1X XQ1
	, @Q1Y YQ1
	, @Q1YonX YonXQ1

	, @Q3X XQ3
	, @Q3Y YQ3
	, @Q3YonX YonXQ3

	FROM @scatter
	WHERE xValue is not null and yValue is not null


	-- third is the quality analysis
	SELECT count(schNo) schoolCount
	, sum(case when XQ > 3 then 1 else null end) XIQ3
	, sum(case when YQ > 3 then 1 else null end) YIQ3
	, sum(case when YonXQ > 3 then 1 else null end) YonXIQ3
	, sum(case when XQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) XIQ3QualityAlerts

	, sum(case when YQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) YIQ3QualityAlerts

	, sum(case when YonXQ > 3 then
				case when (YonXQuality is not null) then 1 else 0 end
			 else null end) YonXIQ3QualityAlerts

	-- these are points with all alerts confirmed
	, sum(case when XQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) XIQ3QualityOK

	, sum(case when YQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) YIQ3QualityOK

	, sum(case when YonXQ > 3 then
				case when (YonXQuality = 0) then 1 else 0 end
			 else null end) YonXIQ3QualityOK
	, sum(case when YonXQ > 3 then XValue else null end) XSumIQ3
	, sum(case when YonXQ > 3 then YValue else null end) YSumIQ3
	, sum(XValue) XSum
	, sum(YValue) YSum

	from @Scatter
	end


--------------------------------------------------------------------------------
-- 18 10 2014
-- support fo kml with the @kml flag
-- we export xml in this form to schools kml
-- <data><d key="schNo"><ExtendedData><Data name="xvalue"><displayName>....</displayName>


if (@kml is not null)begin

declare @dt TABLE
(
Tag int
, parent int
, schNo nvarchar(50)
, seq int
, name nvarchar(100)
, displayname nvarchar(100)
, value nvarchar(100)
, yr int
, est nvarchar(10)
)

-- root
INSERT INTO @dt
(Tag, Parent, SchNo, seq)
SELECT 1, null, null, 0


-- first the schools
INSERT INTO @dt
(Tag,
Parent,
Schno,
Seq
)
Select 10
, 1
, schNo
, 0
FROM @scatter

-- node for extndeddata
INSERT INTO @dt
(Tag,
Parent,
Schno,
Seq
)
Select 11
, 10			-- child of d
, schNo
, 0
FROM @scatter


-- xvalue

	-- xvalue - twice
	INSERT INTO @dt
	(Tag,
	Parent,
	Schno,
	Seq,
	name,
	displayname,
	value,
	yr,
	est
	)
	Select num
	, 11
	, schNo
	, 0
	, 'xvalue'
	, replace(@XSeries,'pSchoolRead.schoolDataSet','')
	, xvalue
	,@BaseYear + @XSeriesOffset
	, case when xEstimate = 1 then 'est' else '' end
	FROM @scatter
	-- xvalue
	CROSS JOIN metaNumbers
	WHERE num in (21,22, 23)

	-- xvalue - twice
	INSERT INTO @dt
	(Tag,
	Parent,
	Schno,
	Seq,
	name,
	displayname,
	value,
	yr,
	est
	)
	Select num
	, 11
	, schNo
	, 0
	, 'yvalue'
	, replace(@YSeries,'pSchoolRead.schoolDataSet','')
	, Yvalue
	,@BaseYear + @YSeriesOffset
	, case when yEstimate = 1 then 'est' else '' end
	FROM @scatter
	-- xvalue
	CROSS JOIN metaNumbers
	WHERE num in (31,32,33)

declare @extdata xml

Select @extdata =
(
Select Tag, Parent
	, 'schoolScatterChart' [data!1!src]
	, schNo				 [d!10!key]
	, schNo				 [ExtendedData!11!!hide]

	, name				 [Data!21!name]
	, displayname		 [Data!21!displayName!element]
	, value				 [Data!21!value!element]
	, 'xyear'			 [Data!22!name]
	, yr				 [Data!22!value!element]
	, 'xest'			 [Data!23!name]
	, est				 [Data!23!value!element]


	, name				 [Data!31!name]
	, displayname		 [Data!31!displayName!element]
	, value				 [Data!31!value!element]

	, 'yyear'			 [Data!32!name]
	, yr				 [Data!32!value!element]
	, 'yest'			 [Data!33!name]
	, est				 [Data!33!value!element]


FROM @dt
ORDER BY schNo, Tag, seq
FOR XML EXPLICIT
)

-- Now invoke SchoolsKML with the extended data, and the balloon template passed by the caller

exec dbo.schoolsKml @kml, @extdata
end
END
GO

