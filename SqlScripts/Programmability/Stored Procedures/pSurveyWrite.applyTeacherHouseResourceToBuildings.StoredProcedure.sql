SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 03 2010
-- Description:	This procuedre brings into line the old and new ways of keeping teacher house data.
-- On the survey, we can collect teacher house data in gross numbers by house type
-- Through mappings to building type and subtype in metaResourceCAtegories
-- this information can be used to maintain specpfic building records for each teacher house
-- the linked School Inspection holds BuildingReviews for each house.
-- These reviews specify the house type and condition
-- and the create date , if we have created a new building since last year.

-- =============================================
CREATE PROCEDURE [pSurveyWrite].[applyTeacherHouseResourceToBuildings]
	-- Add the parameters for the stored procedure here
	@surveyID as int,
	@Category nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-----

/*

	T1		T2			T3
rs		3		4			5
bi		2		6			4

assume the minimum of each type don't change, so we have

	T1		T2			T3
	2		4			4

rs	1					1

bi			-2

-- process any decreases in BI - assume achange of type up, then change of tpe down

-- remaing decreases, any remaining are closed

-- remaining increases are added


EXAMPLE: opposite of above
	T1		T2			T3
rs		2		6			4
bi		3		4			5

	T1		T2			T3
	2		4			4

rs			2

bi	-1					-1


*/


declare @LinkedSI int
declare @LinkedInspSETID int
declare @SchoolNo nvarchar(50)
declare @surveyYear int
declare @CensusDate datetime

declare @ResBuildingType nvarchar(10)		-- the bulding type associated to the resource, this will likely be HOUSE

-- it is said that you should create your tables at the top

-- the number of resources of each type
CREATE TABLE #tmp
(
	houseType nvarchar(10)
	, condition nvarchar(1)
	, resNum int
	, bldCount int
	,
)

-- this one sequences the differnet house types. House types may change up or down (2 bed => 3 bed) -
-- this sequence specifies the most logical order that may occur
CREATE TABLE #houseTypeIdx
(
	houseType nvarchar(10),
	htIdx int
)


-- some background information
Select @CensusDate = svyCensusDate
		, @SchoolNo = schNo
		, @SurveyYear = SS.svyYEar
	FROM
		SchoolSurvey SS
			INNER JOIN Survey
				ON SS.svyYEar = Survey.svyYear
		WHERE SS.ssID = @SurveyID


Select @ResBuildingType = mresCatBuildingType
FROM metaResourceCategories
WHERE mresCatCode = @Category


EXEC @LinkedSI =  pSurveyRead.getLinkedInspectionID @surveyID,'BLDG'

if (@linkedSI = 0) begin
	EXEC @LinkedInspSETID = pSurveyWrite.createLinkedInspectionSetID @SurveyID,'BLDG'

	INSERT INTO SchoolInspection
	(
		schNo
		, inspStart
		, inspSetID
	)

	Select schNo
			, svyCensusDate
			, @LinkedInspSetID
	FROM
		SchoolSurvey SS
			INNER JOIN Survey
				ON SS.svyYEar = Survey.svyYear
		WHERE SS.ssID = @SurveyID

	SELECT @LinkedSI = SCOPE_IDENTITY()
end

-- now collect all the teacher houses as they are entered on the resources list
-- group these according to the building subtype
-- a gold taps version will not ahrdcode the subtype values
--


-- some hardcoding for now

INSERT INTO #houseTypeIdx(houseType, htIdx)
SELECT
	mResBuildingSubType
	, row_number() OVER (ORDER BY mresSeq) POS
FROM metaResourceDefs
WHERE mresCat = @Category


-- the list of buildings at the school of the type we are interested in
Select U.*
	, cast(null as nvarchar(10)) as NewType
	, cast(null as nvarchar(1)) as NewCondition
INTO #bldList
from pInfrastructureRead.BuildingSnapshot(@CensusDate) U
WHERE schNo = @SchoolNo
AND BuildingType = @ResBuildingType    ---'HOUSE'
AND SubType in (Select HouseType from #houseTypeIdx)
-- a closed school should only be in the list if it has a review on this linked survey
-- otherwise, it was closed before this, don;t attempt to reopen it, make a new one.
AND (Closed = 0 or ReviewInspID = @LinkedSI)

-- the number of resources we have to allocate to buildings
-- get the current totals by subtype
INSERT INTO #tmp
(
	houseType
, Condition
, resNum
, bldCount
)
select
	mresBuildingSubType
	, case min(resCondition) when 3 then 'A' when 2 then 'B' when 1 then 'C' end
	, sum(resNumber)
	, 0
FROM Resources
	INNER JOIN metaResourceDefs RD
		ON REsources.resName = RD.mresCAt
			AND Resources.resSplit = RD.mresName
WHERE mresBuildingType = @ResBuildingType ---'HOUSE'
	AND Resources.ssID = @SurveyID
	AND RD.mresCat = @Category
GROUP BY mresBuildingSubType


declare @housesToAllocate int
declare @buildingsUnAllocated int
declare @idxOffset int

select @idxOffset = 0

Select @HousesToAllocate = sum(isnull(resNum,0) - bldCount)
from #tmp

Select @BuildingsUnallocated = count(*) from #bldList WHERE NewType is null

-- the core is this loop which marks off the exisitng buildings against the resource list
-- beginning with those of the same type,
-- then progressively promoting or demoting buildings
-- until either all the resources are used up, or all the buildings are used up
while ( @HousesToAllocate > 0 and @BuildingsUnallocated > 0) begin
	-- update as many exisitng houses as possible of the same type
	print 'Pass' + cast(@idxOffset as nchar) + ' Houses to allocate:' + cast(@HousesToAllocate as nvarchar(3)) + 'Unallocated:' + cast(@BuildingsUnallocated as nvarchar(4))

	UPDATE #bldList
	SET NewType = #tmp.houseType
		, NewCondition = #tmp.Condition
	FROM
	#bldList
	INNER JOIN
		(Select ID, subType, row_Number() OVER (PARTITION BY subType ORDER BY ID, closed) POS
		 from #bldList
		 WHERE newType is null
		 ) U
		ON #bldList.ID = U.ID
	INNER JOIN #houseTypeIdx I1
		 ON I1.houseType = U.subType
	INNER JOIN #houseTypeIdx I2
		ON I1.htIdx = I2.htIDX + @idxOffset
	INNER JOIN #tmp
		ON I2.houseType = #tmp.houseType
	WHERE U.POS <= #tmp.resNum - #tmp.bldCount


	-- update the totals on #tmp
	UPDATE #tmp
	SET bldCount = NewCount
	FROM #tmp
	INNER JOIN
		(Select NewType, count(*) NewCount FROM #bldList WHERE NEwType is not null GROUP BY NewType ) U
		ON #tmp.houseType = U.NewType

	Select @HousesToAllocate = sum(isnull(resNum,0) - bldCount)
	from #tmp

	Select @BuildingsUnallocated = count(*) from #bldList WHERE NewType is null

	-- changet this index to look for not exact matches of house type; ie 1 or more catgeries up or down
	Select @idxOffset = case @idxOffset
							when 0 then 1
							when 1 then -1
							when -1 then 2
							when 2 then -2
							when -2 then 3
							when 3 then -3
							when -3 then 4
							when 4 then -4
						end

end
-- now we have identified all the surviving buildings and mapped types for them
-- we need to write those
BEGIN TRANSACTION
print 'End Matching Houses to allocate:' + cast(@HousesToAllocate as nvarchar(3)) + 'Unallocated:' + cast(@BuildingsUnallocated as nvarchar(4))

print 'insert the matches'
-- no exisitng review for this inspection
INSERT INTO BuildingReview
	(bldID
	, inspID
	, brevCode
	, brevSubType
	, brevTitle
	, brevClosed
	, brevEndMode
	, brevCondition
	)
	Select ID
	, @LinkedSI
	, @ResBuildingType   --'HOUSE'
	, newType
	, bldgTitle
	, 0
	, null
	, NewCondition
	FROM #bldList
	INNER JOIN Buildings
		ON #bldList.ID = Buildings.bldID
	WHERE #bldList.newType is not null
	AND (#bldList.reviewInspID <> @LinkedSI or #bldList.reviewInspID is null)

print cast(@@ROWCOUNT as nvarchar(4)) + ' new building reviews for matches	'

-- update exisitng review for this inspection
	UPDATE BuildingReview
	SET brevSubType = #bldList.newType
	, brevClosed = 0
	, brevEndMode = null
	, brevCondition = #bldList.NewCondition
	FROM BuildingReview
	INNER JOIN #bldList
		ON #bldList.reviewID = BuildingReview.brevID
	WHERE #bldList.newType is not null
	AND (#bldList.NewType <> #bldList.subType
			OR brevClosed=1
			OR isnull(brevCondition,'')<> isnull( #bldList.NewCondition,''))			-- don;t do what we don;t need to do
	AND (#bldList.reviewInspID = @LinkedSI)

print cast(@@ROWCOUNT as nvarchar(4)) + ' updated building reviews for matches	'

-- at this point either @HousesToAllocate > 0 - meaning there are new rooms
-- or @BuildingsUnalocated > 0 meaning there are less rooms
-- or both are 0 , meaning same rooms as before

-- we can treat the 2 cases separately

declare @TagDescription nvarchar(100)
Select @TagDescription = 'Building record generated from School Survey resource count ' + cast(@SurveyYear as nvarchar(4))
IF @HousesToAllocate > 0 begin
	INSERT INTO BuildingReview
	(bldID
	, inspID
	, brevCode
	, brevSubType
	, brevTitle
	, brevYear
	, brevDescription
	, brevCondition
	)
	Select null
	, @LinkedSI
	, @ResBuildingType -- 'HOUSE'
	, houseType
	, 'House'				-- on a rainy day I'll go look up the lkp Table
	, @SurveyYear
	, @TagDescription
	, #tmp.Condition
	FROM #Tmp
	CROSS JOIN metaNumbers
	WHERE num between 1 and (#tmp.resNum - #tmp.bldCount)
	AND (#tmp.resNum - #tmp.bldCount) > 0
end


IF @BuildingsUnallocated > 0 begin

-- we may be closing a building not reviewed yet

	INSERT INTO BuildingReview
	(bldID
	, inspID
	, brevCode
	, brevSubType
	, brevTitle
	, brevClosed
	, brevEndMode
	)
	Select ID
	, @LinkedSI
	, @ResBuildingType --'HOUSE'
	, subType
	, 'House'
	, 1
	, 'UNREPORTED'
	FROM #bldList
	INNER JOIN Buildings
		ON #bldList.ID = Buildings.bldID
	WHERE #bldList.newType is null
	AND (#bldList.reviewInspID <> @LinkedSI or #bldList.reviewInspID is null)


-- update exisitng review for this inspection

-- but IF:
--- the building was only created through this survey
--- there are no other reviews for it
--- there are no workitems that refer to it
--- then we may as well just delete
-- Otherwise we can accumulate some detritis of errors

print 'preparing to delete'


	DELETE from Buildings
	FROM Buildings
	INNER JOIN #bldList
		ON Buildings.bldID = #bldList.ID
	LEFT JOIN
		(Select DISTINCT bldID from WorkItems) WI
		ON Buildings.bldID = WI.bldID
	LEFT JOIN
		(Select bldID from BuildingReview GROUP BY bldID HAVING count(*) > 1) RVW
		ON Buildings.bldID = RVW.bldID
	WHERE #bldList.newType is  null
	AND Buildings.bldgDescription = @TagDescription
	AND (#bldList.reviewInspID = @LinkedSI)
	AND WI.bldID is null
	AND RVW.bldID is null


	-- the CASCADE delete on the above means that the inner join to
	-- BuildingReview will no longer return any of the IDs in #bldList
	-- that we just deleted
	UPDATE BuildingReview
	SET brevClosed = 1
	, brevEndMode = 'UNREPORTED'
	FROM BuildingReview
	INNER JOIN #bldList
		ON #bldList.reviewID = BuildingReview.brevID
	WHERE #bldList.newType is  null
	AND (brevClosed = 0)
	AND (#bldList.reviewInspID = @LinkedSI)

end
COMMIT TRANSACTION


-- is there any way to do this without a cursor?
---??
----?????

-----

END
GO

