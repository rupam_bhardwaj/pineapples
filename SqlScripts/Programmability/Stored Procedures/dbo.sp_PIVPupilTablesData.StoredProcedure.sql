SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 12 2007
-- Description:	Pupil tables pivot data
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVPupilTablesData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- this data denormalises the following pupil tables
-- DROP
-- ABSENT
-- PNA
-- PNAR
-- TRIN
-- TROUT
-- and adds enrol also, for comparison
-- repeaters and disability are treated in separate queries
-- repeaters needs to split by age, disability by disability type

-- jet version accomplishes this by creating 2 crosstabs,
-- one using ptM as data, the other ptF
-- these are then union-and-summed along with Enrollments

create table #data
(
ssID int,
levelCode nvarchar(10),
DropF int,
AbsentF int,
PNAF int,
PNARF int,
TRINF int,
TROUTF int,
EnrolF int,
DropM int,
AbsentM int,
PNAM int,
PNARM int,
TRINM int,
TROUTM int,
EnrolM int
)

-- cahce the estimate data
SELECT *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()


insert into #data
(ssID, levelCode,
DropF, AbsentF, PNAF, PNARF, TRINF, TROUTF)
SELECT
ssID, ptLevel,
[DROP], [ABSENT],[PNA],[PNAR],[TRIN],[TROUT]
from PupilTables
PIVOT (sum(ptF) for ptCode in
		([DROP], [ABSENT],[PNA],[PNAR],[TRIN],[TROUT])) p

-- male values
insert into #data
(ssID,  levelCode,
DropM, AbsentM, PNAM, PNARM, TRINM, TROUTM)
SELECT
ssID, ptLevel,
[DROP], [ABSENT],[PNA],[PNAR],[TRIN],[TROUT]
from PupilTables
PIVOT (sum(ptM) for ptCode in
		([DROP], [ABSENT],[PNA],[PNAR],[TRIN],[TROUT])) p

insert into #data
(ssID,  levelCode,

enrolM, EnrolF
)
SELECT
ssID, levelCode,
enrolM, enrolF
from pEnrolmentRead.ssIDEnrolmentLevel


Select
E.LifeYear [Survey Year],
E.Estimate,
E.bestYear [Year of Data],
E.Offset [Age of Data],
E.bestssqLevel as [Data Quality Level],
E.actualssqLevel as [Survey Year Quality Level],
E.surveyDimensionssID,
D.levelCode,
DropF,AbsentF,
PNAF, PNARF,
TRINF, TROUTF,
EnrolF,
DropM,AbsentM,
PNAM, PNARM,
TRINM, TROUTM,
EnrolM
FROM
(
Select
ssID, levelCode,
sum(DROPF) as DropF,
sum(AbsentF) as AbsentF,
sum(PNAF) as PNAF,
sum(PNARF) as PNARF,
sum(TRINF) as TRINF,
sum(TROUTF) as TROUTF,
sum(EnrolF) as EnrolF,
sum(DROPM) as DropM,
sum(AbsentM) as AbsentM,
sum(PNAM) as PNAM,
sum(PNARM) as PNARM,
sum(TRINM) as TRINM,
sum(TROUTM) as TROUTM,
sum(EnrolM) as EnrolM
from #data
GROUP BY ssID, levelCode
) D
	INNER JOIN #ebse E
	ON E.bestssID = D.ssID


    -- Insert statements for procedure here

END
GO
GRANT EXECUTE ON [dbo].[sp_PIVPupilTablesData] TO [pSchoolRead] AS [dbo]
GO

