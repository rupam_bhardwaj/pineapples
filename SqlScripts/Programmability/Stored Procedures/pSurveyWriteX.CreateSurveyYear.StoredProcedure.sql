SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 05 2010
-- Description:	Initiate a new survey year
-- =============================================
CREATE PROCEDURE [pSurveyWriteX].[CreateSurveyYear]
	-- Add the parameters for the stored procedure here
	@SurveyYear int
	, @CensusDate datetime
	, @CollectionDate datetime
	, @OfficialStartAge int
WITH EXECUTE AS 'pineapples'		-- becuase we need to be able to disable a trigger
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

-- validations
	if @CensusDate is null begin
				RAISERROR('<ValidationError field="syCensusDate">
					No census date specified
					</ValidationError>', 16,1)
	end

	if @CollectionDate is null begin
				RAISERROR('<ValidationError field="syCensusDate">
					No collection date specified
					</ValidationError>', 16,1)
	end

	if @OfficialStartAge is null begin
				RAISERROR('<ValidationError field="syCensusDate">
					No official start age specified
					</ValidationError>', 16,1)
	end
	begin transaction

	INSERT INTO Survey
	(	svyYear
		,svyCensusDate
		,svyCollectionDate
		, svyPSAge
	) VALUES

	(	@SurveyYear
		, @CensusDate
		, @CollectionDate
		, @OfficialStartAge
	)

	declare @MaxYear int
	Select @MaxYear = svyYear
	from Survey
	WHERE svyYear <> @SurveyYear

   -- copy all the school type definitions to the new year from the last
   INSERT INTO [SurveyYearSchoolTypes]
           ([svyYear]
           ,[stCode]
           ,[ytForm]
           ,[ytTeacherForm]
           ,[ytAgeMin]
           ,[ytAgeMax]
           ,[ytConfig])
    SELECT
			@SurveyYear
           ,[stCode]
           ,[ytForm]
           ,[ytTeacherForm]
           ,[ytAgeMin]
           ,[ytAgeMax]
           ,[ytConfig]
    FROM SurveyYearSchoolTypes
    WHERE svyYear = @MaxYear

 -- copy over teacher qualifations - always been a headache
    INSERT INTO [SurveyYearTeacherQual]
           ([svyYear]
           ,[ytqSector]
           ,[ytqQual]
           ,[ytqQualified]
           ,[ytqCertified])
     SELECT
			@SurveyYear
		  ,[ytqSector]
		  ,[ytqQual]
		  ,[ytqQualified]
		  ,[ytqCertified]
	  FROM [SurveyYearTeacherQual]
	WHERE svyYear = @MaxYear


	-- any settings that are in SchoolHistoryhistory already for the new year
	-- (ie future values for authority, school type, parent, dormant)
	-- become the current values on Schools
	;
	DISABLE TRIGGER dbo.Schools_RelatedData on Schools -- becuase this tries to update SchoolYearHistory
	UPDATE Schools
		-- beware any nulls error in SI 22 03 2011
		SET schType = isnull(systCode, schools.schType)
		, schAuth = isnull(syAuth, Schools.schAuth)
		, schParent = Schools.schParent		-- so if this is cleared to be a standalone, that comes into force
		, schDormant = syDormant
		, pEditContext = 'Create Survey Year' + cast(@SurveyYear as nvarchar(4))
	FROM Schools
		INNER JOIN SchoolYEarHistory SYH
		On Schools.schNo = SYH.schNo
		AND SYH.syYear = @SurveyYear

	;
	ENABLE TRIGGER dbo.Schools_RelatedData on Schools;

	-- now populate SchoolYearHistory
	-- (the table formerly known as SurveyControl)

	INSERT INTO SchoolYearHistory
	( syYear
		, schNo
		, systCode
		, syAuth
		, syParent
		, syDormant

	)
	SELECT @SurveyYear
	, Schools.schNo
	, schType
	, schAuth
	, schParent
	, schDormant
	FROM Schools
		LEFT JOIN SchoolYEarHistory SYH
		On Schools.schNo = SYH.schNo
		AND SYH.syYear = @SurveyYear
	WHERE SYH.syID is null		-- ie no exisitng record in the year
	AND (schClosed = 0 or schClosed > @SurveyYear)

	-- and should we delete anything that is closed?

	DELETE
	FROM SchoolYEarHistory
	WHERE SchoolYearHistory.schNo in
		(Select Schools.schno from Schools
			WHERE (schClosed <> 0 and schClosed <= @surveyYear)
		)
	AND SchoolYearHistory.syYEar = @SurveyYear

	commit transaction
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	-- it is of utmost importance the triggers are on
	;
	ENABLE TRIGGER dbo.Schools_RelatedData on Schools;

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

end
GO

