SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2009
-- Description:	Return the structured security grid for the selected user or role
-- =============================================
CREATE PROCEDURE [dbo].[getPineapplesSecurity]
	-- Add the parameters for the stored procedure here
	@Username sysname = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @DataAreas TABLE
(
	dataArea nvarchar(50)
	, hasB int
	, hasX int
	, hasXX int
	, hasA int
	, hasO int
)
declare @mems table
(
	rolename sysname
	, membername sysname null
	, depth int
)

-- the permission roles pXXXX
-- are flagged by the Extended property "PineapplesUser = coreRole"
-- split these role names aprt to isolate the data roles
INSERT INTO @DAtaAreas
EXEC common.getDataAreaAccessLevels


if (@UserName is null)
	INSERT INTO @mems
	SELECT DP.name, original_login(), 1
	FROM
		sys.database_principals DP
	WHERE DP.Type = 'R'
	AND is_member(DP.name) = 1

else
	INSERT INTO @mems
	SELECT rolename, memberName, depth
	FROM
		RecursiveRoleMembership


Select DA.dataArea
, isnull(MemberName,@UserName) MemberName
, isnull([Read],0) [Read]
, isnull( [Write],0) [Write]

, isnull([ReadX],0) [ReadX]
, isnull( [WriteX],0) [WriteX]

, isnull([ReadXX],0) [ReadXX]
, isnull( [WriteXX],0) [WriteXX]

, isnull([Maint],0) [Maint]
, isnull( [Admin],0) [Admin]
, isnull( [Ops],0) [Ops]
, DA.hasB
, DA.HasX
, hasXX
, hasA
, hasO
, ROW_NUMBER() OVER (Order by DA.DataArea) RowNo
From
	@dataAreas DA
	LEFT JOIN
		(
		-- group by, abd case logic 'pivots' the DataAccess column from sub
		Select MemberName
			, DataArea
			, case when sum(case when DataAccess = 'Read' then InRole end) > 0 then 1 else 0 end [Read]
			, case when sum(case when DataAccess = 'Write' then InRole end) > 0 then 1 else 0 end  [Write]

			, case when sum(case when DataAccess = 'ReadX' then InRole end) > 0 then 1 else 0 end  [ReadX]
			, case when sum(case when DataAccess = 'WriteX' then InRole end) > 0 then 1 else 0 end  [WriteX]

			, case when sum(case when DataAccess = 'ReadXX' then InRole end) > 0 then 1 else 0 end  [ReadXX]
			, case when sum(case when DataAccess = 'WriteXX' then InRole end) > 0 then 1 else 0 end  [WriteXX]

			, case when sum(case when DataAccess = 'Maint' then InRole end) > 0 then 1 else 0 end  [Maint]
			, case when sum(case when DataAccess = 'Admin' then InRole end) > 0 then 1 else 0 end   [Admin]
			, case when sum(case when DataAccess = 'Ops' then InRole end) > 0 then 1 else 0 end  [Ops]

			from
			(

			-- this inner query finds all coreRoles, and all membership of those
			-- the membership comes from recursive role membershup
			select U.name, MemberName, case when Depth > 0 then 1 else 0 end InRole,
			RN.*
			from sys.sysusers u
			inner join sys.extended_properties P
			on u.uid = P.Major_ID and P.class = 4
			LEFT JOIN
			@mems MEMS
			on u.name = MEMS.RoleNAme
			cross apply common.ParsePineapplesRoleName(U.name) RN
			WHERE P.name = 'PineapplesUser' and P.value = 'CoreRole'
			AND  (MEMS.MemberName = isnull(@userName, original_login()))
			and u.issqlrole = 1
			) Sub
		group BY MemberName
		, DataArea
		) PP
on DA.dataArea = PP.dataArea
ORDER BY DA.DataArea
END
GO
GRANT EXECUTE ON [dbo].[getPineapplesSecurity] TO [pAdminRead] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[getPineapplesSecurity] TO [public] AS [dbo]
GO

