SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pExamRead].[SchoolExamComparison]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50)
	, @examID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TOTS Table
(

	Gender nvarchar(1)
	, tot float
)

DECLARE @T TABLE
(
	bandMin int
	, Gender nvarchar(1)
	, C int
)


DECLARE @Summary TABLE
(
	code nvarchar(50)
	, name nvarchar(200)
	, candidates int
	, candidatesM int
	, candidatesF int

)
-- first get the scool's district and authority
declare @currentSchoolName nvarchar(200)
declare @currentDistrict nvarchar(10)
declare @currentDistrictName nvarchar(200)

declare @currentAuth nvarchar(10)
declare @currentAuthName nvarchar(200)

Select @currentDistrict = iGroup
, @currentDistrictName = dName
, @currentAuth = schAuth
, @currentAuthName = authName
, @currentSchoolName = schName
FROM Schools
	INNER JOIN Islands I
		ON Schools.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGroup =D.dID
	LEFT JOIN TRAuthorities A
		ON Schools.schAuth = A.authCode
WHERE Schools.schNo = @SchoolNo


-- some overall data
Select EX.*
, ET.exName
, @currentAuthName Authority
, @currentSchoolName SchoolName
FRom Exams EX
	INNER JOIN lkpExamTypes ET
		ON EX.exCode = ET.exCode
WHERe exID = @examID

-- first the school

INSERT INTO @TOTS
Select
exsGender
, sum(exsCandidates)
FROM ExamScores
WHERE exID = @examID
AND schNo = @SchoolNo
GROUP BY exsGender

INSERT INTO @T
SELECT exsMin
, exsGender
, sum(exsCandidates) C
FROM ExamScores S
WHERE exID = @examID
AND schNo = @SchoolNo
GROUP BY exsMin
, exsGender

INSERT INTO @Summary
SELECT
@schoolNo
, @currentSchoolName
, sum(C)
, sum(case Gender when 'M' then C end)
, sum(case Gender when 'F' then C end)
FROM @T

Select
	@SchoolNo schNo
	, X.*
	, ((L) + C) / Tot LP
	, (G + C) / Tot GP
	, case when (((L) + C) / Tot) >= .1 and (G + C) /Tot > .9 then 'P10'
		when (((L) + C) / Tot) >= .5 and (G + C) /Tot > .5 then 'P50'
		when (((L) + C) / Tot) >= .9 and (G + c) /Tot > .1 then 'P90'
	end PERCT
FROM
(
Select M.bandMin
, M.Gender
, T.Tot
, (M.C) C
, isnull(sum(L.C),0) L
, (T.Tot - M.C - isnull(sum(L.C),0)) G

FROM @T M
	INNER JOIN @TOTS T
		ON M.Gender = T.Gender
	LEFT JOIN
	(
		Select bandMin
		, Gender
		, C
		FROM @T
	)  L
		ON M.Gender = L.Gender
		AND M.BandMin > L.BandMin
GROUP BY
M.bandMin
, M.Gender
, M.C
, T.tot

) X
ORDER BY
Gender
, BandMin

------------------------------------------------------------------------------------
-- Authority
------------------------------------------------------------------------------------
DELETE FROM @TOTS
DELETE FROm @T

INSERT INTO @TOTS
Select
exsGender
, sum(exsCandidates)
FROM ExamScores
	INNER JOIN Schools S
		ON ExamScores.SchNo = S.schNo
WHERE exID = @examID
AND S.schAuth = @currentAuth
GROUP BY exsGender

INSERT INTO @T
SELECT exsMin
, exsGender
, sum(exsCandidates) C
FROM ExamScores
	INNER JOIN Schools S
		ON ExamScores.SchNo = S.schNo
WHERE exID = @examID
AND S.schAuth = @currentAuth
GROUP BY exsMin
, exsGender


INSERT INTO @Summary
SELECT
@currentAuth
, @currentAuthName
, sum(C)
, sum(case Gender when 'M' then C end)
, sum(case Gender when 'F' then C end)
FROM @T

Select
	@currentAuth authorityCode
	, X.*
	, ((L) + C) / Tot LP
	, (G + C) / Tot GP
	, case when (((L) + C) / Tot) >= .1 and (G + C) /Tot > .9 then 'P10'
		when (((L) + C) / Tot) >= .5 and (G + C) /Tot > .5 then 'P50'
		when (((L) + C) / Tot) >= .9 and (G + c) /Tot > .1 then 'P90'
	end PERCT
FROM
(
Select M.bandMin
, M.Gender
, T.Tot
, (M.C) C
, isnull(sum(L.C),0) L
, (T.Tot - M.C - isnull(sum(L.C),0)) G

FROM @T M
	INNER JOIN @TOTS T
		ON M.Gender = T.Gender
	LEFT JOIN
	(
		Select bandMin
		, Gender
		, C
		FROM @T
	)  L
		ON M.Gender = L.Gender
		AND M.BandMin > L.BandMin
GROUP BY
M.bandMin
, M.Gender
, M.C
, T.tot

) X
ORDER BY
Gender
, BandMin

------------------------------------------------------------------------------------
-- District
------------------------------------------------------------------------------------
DELETE FROM @TOTS
DELETE FROm @T

INSERT INTO @TOTS
Select
exsGender
, sum(exsCandidates)
FROM ExamScores
	INNER JOIN Schools S
		ON ExamScores.SchNo = S.schNo
	INNER JOIN Islands I
		ON S.iCode = I.iCode
WHERE exID = @examID
AND I.iGroup = @currentDistrict
GROUP BY exsGender

INSERT INTO @T
SELECT exsMin
, exsGender
, sum(exsCandidates) C
FROM ExamScores
	INNER JOIN Schools S
		ON ExamScores.SchNo = S.schNo
	INNER JOIN Islands I
		ON S.iCode = I.iCode
WHERE exID = @examID
AND I.iGroup = @currentDistrict
GROUP BY exsMin
, exsGender

--- Summary
INSERT INTO @Summary
SELECT
@currentDistrict
, @currentDistrictName
, sum(C)
, sum(case Gender when 'M' then C end)
, sum(case Gender when 'F' then C end)
FROM @T

--- Data
Select
	@currentDistrict District
	, X.*
	, ((L) + C) / Tot LP
	, (G + C) / Tot GP
	, case when (((L) + C) / Tot) >= .1 and (G + C) /Tot > .9 then 'P10'
		when (((L) + C) / Tot) >= .5 and (G + C) /Tot > .5 then 'P50'
		when (((L) + C) / Tot) >= .9 and (G + c) /Tot > .1 then 'P90'
	end PERCT
FROM
(
Select M.bandMin
, M.Gender
, T.Tot
, (M.C) C
, isnull(sum(L.C),0) L
, (T.Tot - M.C - isnull(sum(L.C),0)) G

FROM @T M
	INNER JOIN @TOTS T
		ON M.Gender = T.Gender
	LEFT JOIN
	(
		Select bandMin
		, Gender
		, C
		FROM @T
	)  L
		ON M.Gender = L.Gender
		AND M.BandMin > L.BandMin
GROUP BY
M.bandMin
, M.Gender
, M.C
, T.tot

) X
ORDER BY
Gender
, BandMin

--------------------------------------------------------------------------------------
-- now the grand total
--------------------------------------------------------------------------------------
DELETE FROM @TOTS
DELETE FROm @T

INSERT INTO @TOTS
Select
exsGender
, sum(exsCandidates)
FROM ExamScores
WHERE exID = @examID
GROUP BY exsGender

INSERT INTO @T
SELECT exsMin
, exsGender
, sum(exsCandidates) C
FROM ExamScores
WHERE exID = @examID
GROUP BY exsMin
, exsGender

-- summary
INSERT INTO @Summary
SELECT
null
, 'All Candidates'
, sum(C)
, sum(case Gender when 'M' then C end)
, sum(case Gender when 'F' then C end)
FROM @T


-- Data
Select
	'All' Total
	, X.*
	, ((L) + C) / Tot LP
	, (G + C) / Tot GP
	, case when (((L) + C) / Tot) >= .1 and (G + C) /Tot > .9 then 'P10'
		when (((L) + C) / Tot) >= .5 and (G + C) /Tot > .5 then 'P50'
		when (((L) + C) / Tot) >= .9 and (G + c) /Tot > .1 then 'P90'
	end PERCT
FROM
(
Select M.bandMin
, M.Gender
, T.Tot
, (M.C) C
, isnull(sum(L.C),0) L
, (T.Tot - M.C - isnull(sum(L.C),0)) G

FROM @T M
	INNER JOIN @TOTS T
		ON M.Gender = T.Gender
	LEFT JOIN
	(
		Select bandMin
		, Gender
		, C
		FROM @T
	)  L
		ON M.Gender = L.Gender
		AND M.BandMin > L.BandMin
GROUP BY
M.bandMin
, M.Gender
, M.C
, T.tot

) X
ORDER BY
Gender
, BandMin
END


SELECT * FROM @summary
GO

