SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 10 2013
-- Description:	data for rooms pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVRoomsTuition]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select *
INTO #ebsr
FROM dbo.tfnESTIMATE_BestSurveyRoomType(null) E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)

-- 14 11 2009 SRVU0017
declare @minYEar int

Select @MinYear = min(ptMin) -- shoul dne zero
from Partitions
WHERE
	ptSet='YearBuilt'


SELECT
E.LifeYear [Survey Year],
E.Estimate,
E.bestyear [Year of Data],
E.offset [Age of Data],
E.schNo,
E.SurveyDimensionssID,
R.rmType RoomTypeCode,
lkpRoomTypes.codeDescription AS RoomType,
R.rmID,
R.rmYear AS YearBuilt,
R.rmNo AS RoomNo,
R.rmSize AS RoomSize,
case when (rmSize > 0 ) then 1 else 0 end AS SizeSupplied,
R.rmLevel AS ClassLevel,
R.rmMaterials Material,
R.rmMaterialRoof MaterialRoof,
R.rmMaterialFloor MaterialFloor,
R.rmCondition AS Condition,
1 AS NumRooms,
case when rmYear is null then null else Partitions.ptName end AS YearBracket,
case when [rmShareType] Is Null then 0 else 1 end AS SharedRoom,
R.rmShareType AS ShareRoomTypeCode
INTO #tmpPivRooms
FROM #ebsr E
	INNER JOIN Rooms as R
		ON E.rmType = R.rmType and E.bestssID = R.ssID
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType,
	Partitions
WHERE
	--- 14 11 2009 SRVU0017 schools with rmYear is null were counted for every partition
	Partitions.ptSet='YearBuilt' AND
	((R.rmYear is null and ptMin = @MinYear	)	-- just to get us one row out of partitions
	or R.rmYear between ptMin and ptMax
	)
	AND R.rmType in ('CLASS','LIB','LAB')

if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurveyCore DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'CORESUMM') begin
	SELECT
	sum(NumShifts) NumShifts,
	sum(ClassesShift1) ClassesShift1,
	sum(ClassesShift2) ClassesShift2,
	sum(ClassesAllShifts) ClassesAllShifts,
	sum(NumSchools) NumSchools,
	[Survey Year]
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank


	SELECT
	sum(NumShifts) NumShifts,
	sum(ClassesShift1) ClassesShift1,
	sum(ClassesShift2) ClassesShift2,
	sum(ClassesAllShifts) ClassesAllShifts,
	sum(NumSchools) NumSchools,

	[Survey Year]
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
      -- from DimensionRank
		, sum(DRANK.[School Enrol]) [School Enrol]
		, DRANK.[District Decile],
		DRANK.[District Quartile],

		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPIVRooms T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

	GROUP BY
	[Survey Year]
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
      , ShiftsAtSchool
-- from DimensionRank

	, DRANK.[District Decile],
	DRANK.[District Quartile],
	--DRANK.[Rank],			don;t use rank when grouping
	DRANK.[Decile],
	DRANK.[Quartile]

end

drop table #tmpPivRooms

drop table #ebsr
END
GO

