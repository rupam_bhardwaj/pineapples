SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2009
-- Description:	National indicator summary
-- =============================================
CREATE PROCEDURE [dbo].[EFA]
	-- Add the parameters for the stored procedure here
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- some validations of population
declare @checkPop int		-- number of population models

Select @checkPop = count(*)
From PopulationModel


if (@checkPop=0) begin
	raiserror('There is no Population Model to use. ',
				16,10)
end

if (@checkPop > 1) begin
	declare @checkEFA int
	Select @checkEFA = count(*)
	From PopulationModel

	WHERE popModEFA = 1

	if (@checkEFA <> 1) begin
		raiserror('You must specify exactly one Population Model to use for EFA reporting. ',
					16,10)
	end
end

	create table #efatmp
	(
		[survey Year] int
		, recordType	nvarchar(4)
		, edlcode		nvarchar(10)
		,genderCode	nvarchar(1)
		,enrol		int
		, officialAgeEnrol int
		, Intake int
		, intakeOfficialAge int
		, pop int
		, popIntakeAge int
	)

	-- interfering with SELECT statements.
	select *
	into #ebse
	from dbo.tfnESTIMATE_BestSurveyEnrolments()
		-- Insert statements for procedure here

	SELECT     EE.schNo, EE.LifeYear AS [Survey Year], dbo.Enrollments.enAge, dbo.Enrollments.enLevel,
						enM, enF,
						EE.bestssID,
						EE.surveyDimensionssID,
						EE.Estimate,
						  EE.Offset AS [Age of Data], EE.bestYear AS [Year of Data],
						  EE.bestssqLevel AS DataQualityLevel, EE.ActualssqLevel AS SurveyYearQualityLevel,
						  dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset,
						dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth

	into #e
	FROM         #ebse EE INNER JOIN
						  dbo.Enrollments ON EE.bestssID = dbo.Enrollments.ssID INNER JOIN
						  dbo.lkpLevels ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode INNER JOIN
						  dbo.Survey ON EE.LifeYear = dbo.Survey.svyYear


	INSERT INTO #efatmp
	([survey Year], recordType, edlcode, genderCode, enrol, officialAgeEnrol
		, inTake, inTakeOfficialAge)
	SELECT [survey Year]
			, ' '
			, edLevelCode
			, genderCode
			, case when genderCode = 'M' then sum(enM) else sum(enF) end enrol
			, case when genderCode = 'M' then
					sum( case when (enAge - svyPSAge + 1) between EdLevelMin and edLevelMax
						then enM else null end)
				else
					sum( case when (enAge - svyPSAge + 1) between EdLevelMin and edLevelMax
						then enF else null end)
				end OfficialAge
			, case when genderCode = 'M' then
					sum(case when DL.[Year of Education] = DL.edLevelMin then enM else null end)
				else
					sum(case when DL.[Year of Education] = DL.edLevelMin then enF else null end)
			  end Intake
			, case when genderCode = 'M' then
					sum(case when DL.[Year of Education] = DL.edLevelMin
								and enAge - svyPSAge + 1 = edLevelMin
								then enM else null end)
				else
					sum(case when DL.[Year of Education] = DL.edLevelMin
						and enAge - svyPSAge + 1 = edLevelMin
						then enF else null end)
			  end IntakeOfficialAge

	from #e
	INNER JOIN Survey
		ON #e.[Survey Year] = Survey.svyYear
	LEFT JOIN DimensionLEvel DL
	on #e.enLevel = DL.levelCode
	CROSS JOIN DimensionGender

	GROUP By #e.[Survey Year], DL.edLevelCode, genderCode

-- now population

insert into #efaTmp
	([survey Year], recordType, edlcode, genderCode, pop, popIntakeAge)
Select
	popYear, ' '
	, EL.codeCode
	, genderCode
	, case when genderCode = 'M' then sum(popM) else sum(popF) end
	, case when genderCode = 'M' then
			sum(case when popAge - svyPSage + 1 = edlMinYEar then popM else null end)
		else
			sum(case when popAge - svyPSage + 1 = edlMinYEar then popF else null end)
		end popIntakeAge
from Population
inner join PopulationModel
	on Population.popModCode = PopulationModel.popmodCode
	INNER JOIN Survey
		ON Population.popYear = survey.svyYear
	INNER JOIN lkpEducationLevels EL	-- default path levels
		ON (Population.popAge - Survey.svyPSAge + 1) between EL.edlMinYear and edlMaxYEar
	CROSS JOIN DimensionGender
WHERE PopulationModel.popmodEFA = 1
		or (@checkPop=1) -- if there's only one, use it
GROUP BY popYear, EL.CodeCode, genderCode


-- repeaters in total


	SELECT
		[Survey Year]
		, recordType
		, edlCode
		, genderCode
		, sum(T.enrol) Enrol
		, sum(T.OfficialAgeEnrol) OfficialAgeEnrol
		, sum(T.Intake) Intake
		, sum(T.IntakeOfficialAge) IntakeOfficialAge
		, sum(T.Pop) Pop
		, sum(T.PopIntakeAge) PopIntakeAge
	FROM #efaTmp T
	GROUP BY
		[Survey Year]
		, recordType
		, edlCode
		, genderCode


	DROP Table #efaTmp
	DROP TABLE #e
END
GO

