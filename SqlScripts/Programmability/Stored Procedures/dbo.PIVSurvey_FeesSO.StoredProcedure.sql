SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2013
-- Description:	Fees
--
-- =============================================
CREATE PROCEDURE [dbo].[PIVSurvey_FeesSO]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'PupilTable Fees SO'
   Select *
   INTO #tmpPIVCols
   FROM PIVColsFeesSO

	exec dbo.PIVPupilTable_Default_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

