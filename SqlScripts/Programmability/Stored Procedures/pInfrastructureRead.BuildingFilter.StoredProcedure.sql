SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 11 2009
-- Description:	Return buldings matching criteria
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[BuildingFilter]
	-- Add the parameters for the stored procedure here
	@SummaryOnly int ,	-- 0 = return full data 1= summary only 2 = bldID schNo and summary
	@SchoolNo nvarchar(50) = null,

	@BuildingType 	nvarchar(10) = null,
	@IncludeClosed int = 0,

	-- selectors based on school
	@Schooltype nvarchar(10) = null ,
	@District nvarchar(2)  = null ,
	@Island   nvarchar(10) = null ,
	@ElectorateN nvarchar(10) = null,
	@ElectorateL nvarchar(10) = null,

	-- selectors based on building
	@ValuationClass nvarchar(10) = null,
	@SizeClass nvarchar(10) = null,
	@Condition nvarchar(1) = null ,

	@MaterialsClass nvarchar(10) = null,
	@MaterialW nvarchar(10) = null,
	@MaterialF nvarchar(10) = null,
	@MaterialR nvarchar(10) = null,

	@Title nvarchar(50) = null,

	@HasRoomClass bit = 0,
	@HasRoomOHT bit = 0,
	@HasRoomStaff bit = 0,
	@HasRoomAdmin bit = 0,
	@HasRoomStorage bit = 0,
	@HasRoomDorm bit = 0,
	@HasRoomDining bit = 0,
	@HasRoomKitchen bit = 0,
	@HasRoomLibrary bit = 0,
	@HasRoomSpecial bit = 0,
	@HasRoomHall bit = 0,
	@HasRoomOther bit = 0,

	-- selectors based on work items

	@WorkItemType nvarchar(15) = null,
	@WorkOrderStatus nvarchar(15) = null,
	@Supplier nvarchar(10) = null

AS
BEGIN

begin try


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put some enrolment numbers in the mix here
	declare @LastSurvey int
	select @LastSurvey = max(svyYear) from Survey

	Select *
	INTO #ee
	from dbo.tfnESTIMATE_BestSurveyEnrolments()
	WHERE LifeYear = @LastSurvey


	-- get hte best year for reporting the valuations
	declare @ValYear int
	select @ValYear = max(bvYear)
	from BuildingValuationRate
	WHERE bvYear <= year(getdate())

    -- Insert statements for procedure here
	SELECT DISTINCT Buildings.bldID, Buildings.schNo
	INTO #tmpBld
	FROM Buildings
		INNER JOIN Schools
			ON Buildings.schNo = Schools.schNo
		INNER JOIN lkpIslands
			ON Schools.iCode = lkpIslands.iCode
		LEFT JOIN WorkItems
			ON Buildings.bldID = WorkITems.bldID
		LEFT JOIN WorkOrders
			ON WorkItems.woID = WorkOrders.woID

	WHERE
		(Buildings.schNo = @SchoolNo or @SchoolNo is null)
		AND (bldgCode = @BuildingType or @BuildingType is null)

		AND (schType = @SchoolType or @SchoolType is null)
		AND (iGroup = @District or @District is null)
		AND (schools.iCode = @Island or @Island is null)
		AND (Schools.schElectN = @ElectorateN or @ElectorateN is null)
		AND (Schools.schElectL = @ElectorateL or @ElectorateL is null)

		AND (bvcCode = @ValuationClass or @ValuationClass is null)
		AND (bldgCondition = @Condition or @condition is null)
		AND (@SizeClass is null or
				exists (select bszCode
						from lkpBuildingSizeClass
						where
							bszCode = @SizeClass
						AND
							Buildings.bldgA > bszMin and
							Buildings.bldgA <=bszMax
						)
			)

		AND (bldgMaterials = @MaterialsClass or @MAterialsClass is null)
		AND (bldgMatW = @MaterialW or @MaterialW is null)
		AND (bldgMatF = @MaterialF or @MaterialF is null)
		AND (bldgMatR = @MaterialR or @MaterialR is null)

		AND (bldgTitle = @Title or @Title is null)

		AND (bldgRoomsClass > 0 or @HasRoomClass = 0)
		AND (bldgRoomsOHT > 0 or @HasRoomOHT = 0)
		AND (bldgRoomsStaff > 0 or @HasRoomStaff = 0)

		AND (bldgRoomsADmin > 0 or @HasRoomAdmin = 0)
		AND (bldgRoomsStorage > 0 or @HasRoomStorage = 0)


		AND (bldgRoomsDorm > 0 or @HasRoomDorm = 0)
		AND (bldgRoomsDining > 0 or @HasRoomDining = 0)
		AND (bldgRoomsKitchen > 0 or @HasRoomKitchen = 0)
		AND (bldgRoomsLibrary > 0 or @HasRoomLibrary = 0)
		AND (bldgRoomsSpecialTuition > 0 or @HasRoomSpecial = 0)
		AND (bldgRoomsHall > 0 or @HasRoomHall = 0)
		AND (bldgRoomsOther > 0 or @HasRoomOther = 0)

		AND (witmType = @WorkITemType or @WorkITemType is null)
		AND (woStatus = @WorkOrderStatus or @WorkOrderStatus is null)
		AND (supCode = @Supplier or @Supplier is null)

		AND (bldgClosed = 0 or @IncludeClosed = 1)


	if (@SummaryOnly = 0) begin
		Select Buildings.*
			, schName
			, schType
			, schElectN
			, schElectL
			, Schools.iCode
			, iName
			, imultFactor
			, Districts.dID
			, Districts.dName
			, @ValYEar ValYear
			, SZ.bszCode
			, V.bvVal
			, V.bvVal * bldgA * isnull(imultFactor,1) CalcValuation
		FROM Buildings
		INNER JOIN #tmpBld
			ON Buildings.bldID = #tmpBld.bldID
		INNER JOIN Schools
			ON Buildings.schNo = Schools.schNo
		INNER JOIN Islands
			ON Schools.iCode = Islands.iCode
		INNER JOIN Districts
			ON Islands.iGroup = Districts.dID

		LEFT JOIN lkpBuildingSizeClass SZ
			ON Buildings.bldgA > Sz.bszMin and Buildings.bldgA <= SZ.bszMax

		LEFT JOIN BuildingValuationRate V
			ON SZ.bszCode = V.bszCode
			AND Buildings.bvcCode = V.bvcCode
			AND bvYear = @ValYear

		LEFT JOIN IslandValuationRates IVR
			ON schools.iCode = IVR.iCode
			AND imultYear = @ValYear
	end

	if (@SummaryOnly = 2) begin
		Select Buildings.*
			, schName
		FROM Buildings
		INNER JOIN #tmpBld
			ON Buildings.bldID = #tmpBld.bldID
		INNER JOIN Schools
			ON Buildings.schNo = Schools.schNo
	end

	declare @totalEnrol int
	Select @TotalEnrol = sum(#ee.bestEnrol)
	from #ee
	WHERE #ee.schNo in (Select schNo from #tmpBld)

-- result set 2" the summary RS
	Select
			COUNT(#tmpBld.bldID) NumBuildings
			, COUNT(distinct #tmpBld.schNo) NumSchools
			, @totalEnrol TotalEnrol
			, SUM(bldgRoomsClass) NumRoomsClass
			, SUM(bldgRoomsOHT) NumRoomsOHT
			, SUM(bldgRoomsStaff) numRoomsStaff
			, SUM(bldgRoomsAdmin) NumRoomsAdmin
			, sum(bldgRoomsStorage) NumRoomsStorage
			, sum(bldgRoomsDorm) NumRoomsDorm
			, sum(bldgRoomsKitchen) NumRoomsKitchen
			, sum(bldgRoomsDining) NumRoomsDining
			, sum(bldgRoomsLibrary) NumRoomsLibrary
			, sum(bldgRoomsSpecialTuition) NumRoomsSpecialTuition
			, sum(bldgRoomsHall) NumRoomsHall
			, sum(bldgRoomsOther) NumRoomsOther
			, sum(bldgA) TotalArea
	from Buildings
			INNER JOIN #tmpBld
			ON Buildings.bldID = #tmpBld.bldID
		LEFT JOIN #ee
			ON Buildings.schNo = #ee.schNo

-- Resultset 3: grouped by building type
	Select
		bdlgCode
		,bdlgDescription
			, COUNT(#tmpBld.bldID) NumBuildings
			, COUNT(distinct #tmpBld.schNo) NumSchools

			, SUM(bldgRoomsClass) NumRoomsClass
			, SUM(bldgRoomsOHT) NumRoomsOHT
			, SUM(bldgRoomsStaff) numRoomsStaff
			, SUM(bldgRoomsAdmin) NumRoomsAdmin
			, sum(bldgRoomsStorage) NumRoomsStorage
			, sum(bldgRoomsDorm) NumRoomsDorm
			, sum(bldgRoomsKitchen) NumRoomsKitchen
			, sum(bldgRoomsDining) NumRoomsDining
			, sum(bldgRoomsLibrary) NumRoomsLibrary
			, sum(bldgRoomsSpecialTuition) NumRoomsSpecialTuition
			, sum(bldgRoomsHall) NumRoomsHall
			, sum(bldgRoomsOther) NumRoomsOther
			, sum(bldgA) TotalArea
		FROM commonInfrastructure.TRBuildingTypes BuildingTypes
		LEFT JOIN
			(
			Buildings
			INNER JOIN #tmpBld
			ON Buildings.bldID = #tmpBld.bldID
			)
			ON BuildingTypes.bdlgCode = Buildings.bldgCode
		GROUP BY bdlgCode
		,bdlgDescription
	drop table #ee
	drop table #tmpBld
end try
/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

