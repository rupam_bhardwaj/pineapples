SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2016
-- Description:
-- =============================================
CREATE PROCEDURE [warehouse].[buildWarehouse]
	@StartFromYear int = null			-- FOR FUTURE use support partial rebuild to preserve earlier data
	with EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get rid of 'Warning: Null value is eliminated by an aggregate or other SET operation.'
	set ansi_warnings off
    -- Insert statements for procedure here


	---- diemensions
	begin transaction
	IF OBJECT_ID('warehouse.dimensionSchoolSurvey', 'U') IS NOT NULL
		DROP TABLE warehouse.dimensionSchoolSurvey;

	SELECT *
	INTO warehouse.dimensionSchoolSurvey
	FROM DimensionSchoolSurveyNoYear
	commit

	begin transaction
	IF OBJECT_ID('warehouse.bestSurvey', 'U') IS NOT NULL
		DROP TABLE warehouse.bestSurvey;

	SELECT LifeYear SurveyYear
	, schNo
	, surveyDimensionssID surveyDimensionID
	INTO warehouse.bestSurvey
	FROM tfnESTIMATE_BestSurveyEnrolments()
	commit

	-- warehouse rebuild script
	begin transaction

	-- support @StartFromYear
	IF OBJECT_ID('warehouse.measureEnrolSchoolG', 'U') IS NULL begin
		SELECT *
			INTO warehouse.measureEnrolSchoolG
			FROM measureEnrolSchoolG

			print 'warehouse.measureEnrolSchoolG created - rows:' + convert(nvarchar(10), @@rowcount)

	end else begin
		DELETE
		FROM warehouse.measureEnrolSchoolG
		WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.measureEnrolSchoolG deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO warehouse.measureEnrolSchoolG
		SELECT *
		FROM measureEnrolSchoolG
		WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
		print 'warehouse.measureEnrolSchoolG inserts - rows:' + convert(nvarchar(10), @@rowcount)
	end
	commit


	-- enrolment-related pupil tables
	begin transaction
	IF OBJECT_ID('warehouse.PupilTablesG', 'U') IS NULL begin
		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, ptLevel ClassLevel
		, ptAge Age
		, G.genderCode
		, case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end pt
		, ptCode
		INTO warehouse.pupiltablesG
		from dbo.tfnESTIMATE_BestSurveyEnrolments() B
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('BRD', 'REP', 'TRIN','TROUT', 'DIS', 'DROP')
		AND isnull(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end ,0) <> 0
		print 'warehouse.pupilTablesG created - rows:' + convert(nvarchar(10), @@rowcount)

	end else begin
		DELETE
		FROM Warehouse.PupilTablesG
		WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.pupilTablesG deletes - rows:' + convert(nvarchar(10), @@rowcount)
		INSERT INTO warehouse.PupilTablesG
		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, ptLevel ClassLevel
		, ptAge Age
		, G.genderCode
		, case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end pt
		, ptCode
		from dbo.tfnESTIMATE_BestSurveyEnrolments() B
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('BRD', 'REP', 'TRIN','TROUT', 'DIS', 'DROP')
		AND isnull(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end ,0) <> 0
 		AND (LifeYear >=@StartFromYear or @StartFromYear is null)
		print 'warehouse.pupilTablesG inserts - rows:' + convert(nvarchar(10), @@rowcount)
	end

--- pre school attenders ( in the same transaction)


	declare @yr1 nvarchar(10)
	select @yr1 = levelCode
		FROM ListDefaultPathLevels
		WHERE yearOfEd = 1

	If @yr1 is null
		Select top 1 @yr1 =  codeCode
		from lkpLevels
		-- note that warehouse.PupilTablesG must exist by now
		INSERT INTO warehouse.PupilTablesG

		Select
		schNo
		, LifeYear SurveyYear
		, Estimate
		, SurveyDimensionssID SurveyDimensionID

		, @yr1 ClassLevel
		, ptAge Age
		, G.genderCode
		, sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) pt
		, 'PSA'
		from dbo.tfnESTIMATE_BestSurveyEnrolments() B
		-- in vermpaf, psa is only shown if there are Enrolments in yearofEd = 1
		-- (the psa are a subset of these)
		-- so this join will exclude any psa that do not have an enrolment record,
		-- even if that psa is in PupilTables
		-- see [dbo].[EnrolmentRatiosByYearOfEd]
--			INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelN N
--				ON B.bestssID = N.ssID
--				AND N.YearOfEd = 1
			INNER JOIN PupilTables PT
				ON B.bestssID = PT.ssID
			CROSS JOIN DimensionGender G
		WHERE ptCode in ('KINDER')
 		AND (LifeYear >=@StartFromYear or @StartFromYear is null)

		GROUP BY
		schNo
		, LifeYear
		, Estimate
		, SurveyDimensionssID
		, ptAge
		, G.genderCode
		HAVING isnull(sum(case when G.genderCode = 'M' then ptM
			when G.genderCode = 'F' then ptF
		 end) ,0) <> 0
		print 'warehouse.pupilTablesG inserts (KINDER) - rows:' + convert(nvarchar(10), @@rowcount)

		commit

		-- combine with enrolments and cross tab the measures
		-- OUTPUT: warehouse.enrol

		begin transaction
		IF OBJECT_ID('warehouse.enrol', 'U') IS NULL begin
			Select schNo
			, surveyYear
			, Estimate
			, SurveyDimensionID
			, ClassLevel
			, nullif(Age,0) Age
			, GenderCode
			, sum(case when ptCode = 'E' then Enrol end) Enrol
			, sum(case when ptCode = 'REP' then Enrol end) Rep
			, sum(case when ptCode = 'TRIN' then Enrol end) Trin
			, sum(case when ptCode = 'TROUT' then Enrol end) Trout
			, sum(case when ptCode = 'BRD' then Enrol end) Boarders
			, sum(case when ptCode = 'DIS' then Enrol end) Disab
			, sum(case when ptCode = 'DROP' then Enrol end) Dropout
			, sum(case when ptCode = 'PSA' then Enrol end) PSA
			INTO warehouse.enrol
			FROM
			(
			Select *
			, 'E' ptCode from measureEnrolG

			UNION ALL
			Select *
			from warehouse.pupilTablesG

			) U
			GROUP BY
			schNo
			, surveyYear
			, Estimate
			, SurveyDimensionID
			, ClassLevel
			, Age
			, GenderCode

			print 'warehouse.enrol created - rows:' + convert(nvarchar(10), @@rowcount)

		end else begin
			DELETE FROM warehouse.enrol
			WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)

			print 'warehouse.enrol deletes - rows:' + convert(nvarchar(10), @@rowcount)

			INSERT INTO warehouse.enrol
			Select schNo
				, surveyYear
				, Estimate
				, SurveyDimensionID
				, ClassLevel
				, nullif(Age,0) Age
				, GenderCode
				, sum(case when ptCode = 'E' then Enrol end) Enrol
				, sum(case when ptCode = 'REP' then Enrol end) Rep
				, sum(case when ptCode = 'TRIN' then Enrol end) Trin
				, sum(case when ptCode = 'TROUT' then Enrol end) Trout
				, sum(case when ptCode = 'BRD' then Enrol end) Boarders
				, sum(case when ptCode = 'DIS' then Enrol end) Disab
				, sum(case when ptCode = 'DROP' then Enrol end) Dropout
				, sum(case when ptCode = 'PSA' then Enrol end) PSA
				FROM
				(
				Select *
				, 'E' ptCode from measureEnrolG

				UNION ALL
				Select *
				from warehouse.pupilTablesG

				) U
				WHERE (SurveyYear >=@StartFromYear or @StartFromYear is null)
				GROUP BY
				schNo
				, surveyYear
				, Estimate
				, SurveyDimensionID
				, ClassLevel
				, Age
				, GenderCode
				print 'warehouse.enrol inserts: '+ convert(nvarchar(10), @@rowcount)

		end

		commit
		-- group by key dimension keys
		-- OUTPUT:  warehouse.tableEnrol
		begin transaction
		IF OBJECT_ID('warehouse.tableEnrol', 'U') IS NOT NULL
			DROP TABLE warehouse.tableEnrol

			Select E.SurveyYear
			, E.Estimate
			, ClassLevel
			, nullif(Age, 0) Age
			, GenderCode

			, DSS.[District Code] DistrictCode

			, [AuthorityCode]
			, [SchoolTypeCode]
			, sum(Enrol) Enrol
			, sum(Rep) Rep
			, sum(Trin) TrIn
			, sum(TROUT) TrOut
			, sum(Boarders) Boarders
			, sum(Disab) Disab
			, sum(Dropout) Dropout
			, sum(PSA) PSA
			INTO warehouse.tableEnrol
			from warehouse.enrol E
			INNER JOIN warehouse.dimensionSchoolSurvey DSS
			ON E.surveyDimensionID = DSS.[Survey ID]

			GROUP BY
			E.SurveyYear
			, E.Estimate
			, ClassLevel
			, Age
			, GenderCode

			, DSS.[District Code]
			, [AuthorityCode]
			, [SchoolTypeCode]

		commit transaction


	--- population
	exec warehouse.buildPopulation  @StartFromYear

	--- School counts
	exec warehouse.buildSchoolCounts @StartFromYear

	--- disability
	exec warehouse.buildDisability @StartFromYear

	--- Cohort -- aggregated
	exec warehouse.buildCohort @StartFromYear

	---- school level flow model
	exec warehouse.buildSchoolCohort @StartFromYear

	--- other data
	exec warehouse.buildTeacherLocation
	exec warehouse.buildRoomCounts
	exec warehouse.buildTextbooks


---- vermpaf support
	exec warehouse.buildClassLevelER
	exec warehouse.buildEdLevelER
	exec warehouse.BuildRank

--- some sanity checks

Select SurveyYear
, sum(Enrol) tableEnrol
from warehouse.tableEnrol
GROUP BY SurveyYear ORDER BY surveyYear

Select SurveyYear
, sum(Enrol) cohort
from warehouse.cohort
GROUP BY SurveyYear ORDER BY surveyYear

SELECT SurveyYear
, sum(Enrol) enrol
from warehouse.enrol
GROUP BY SurveyYear ORDER BY surveyYear

SELECT SurveyYear
, sum(Enrol) enrol
from warehouse.enrol
GROUP BY SurveyYear ORDER BY surveyYear

Select SurveyYear
, sum(Enrol) surveyYearRank
from warehouse.SurveyYearRank
GROUP BY SurveyYear ORDER BY surveyYear

END
GO

