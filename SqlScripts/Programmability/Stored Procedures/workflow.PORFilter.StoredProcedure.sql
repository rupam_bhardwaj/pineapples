SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 10 2009
-- Description:	Filter PORs
-- =============================================
CREATE PROCEDURE [workflow].[PORFilter]
	-- Add the parameters for the stored procedure here
	@PORID int = null
	,@Intray int = 0
	, @CanAction int = 0		--' 0 means don;t care
	,@WorkflowType int = null
	,@Authority nvarchar(10) = null
	,@SchoolNo nvarchar(50) = null
	,@TeacherID int = null

	,@Status nvarchar(20) = null
	,@OpenPOR int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	from workflow.ActionList

	WHERE
		@PORID = porID
		OR
		(@PORID is null and
			(
				(@Intray = 0 or urIntray = 1)
				AND (flowID = @workflowType or @WorkflowType is null)
				AND (porEA = @Authority or @Authority is null)
				AND (schNo = @SchoolNo or @SchoolNo is null)
				AND (tID = @TeacherID or @TeacherID is null)

				AND (porStatus = @Status
						OR (@Status is null and (@OpenPOR = 0 OR porStatus <>'FIN'))
					)
				AND (CanAction = 1 or @CanAction = 0)

			)
		)END
GO

