SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Return the estimated resources from the selected category
-- =============================================
CREATE PROCEDURE [dbo].[sp_vtblResourcesEst]
	-- Add the parameters for the stored procedure here
	@ResourceCategory nvarchar(50) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
create table #ebs
(
	schNo nvarchar(50),
	LifeYear int,
	resName nvarchar(50),
	ActualssID int,
	bestssID int,
	bestYear int,
	Offset int,
	bestssqLevel int,
	ActualssqLevel int,
	SurveyDimensionssID int,
	Estimate smallint
)
if @ResourceCategory = 'Staff Housing'
	begin

		insert into #ebs
		select *
		from ESTIMATE_BestSurveyResourceCategory EBSRC
		WHERE				(EBSRC.resName like 'Staff Housing%'
						or EBSRC.resName = 'Teacher Housing')

	end
if (@ResourceCategory is null)


		insert into #ebs
		select *
		from ESTIMATE_BestSurveyResourceCategory EBSRC

else
		insert into #ebs
		select *
		from dbo.tfnESTIMATE_BestSurveyResourceCategory(@ResourceCategory) EBSRC
		--from ESTIMATE_BestSurveyResourceCategory EBSRC
		--WHERE				(EBSRC.resName =@ResourceCategory)


if @ResourceCategory is null
	begin
		Select R.*,
			E.LifeYear AS [Survey Year],
			E.bestssID,
			E.ActualssID,
			E.SurveydimensionssID,
			E.Estimate,
			E.Offset AS [Age of Data],
			E.bestYear AS [Year of Data],
			E.bestssqLevel AS [Data Quality Level],
			E.ActualssqLevel AS [Survey Year Quality Level]
		from #ebs E inner join Resources R
			on E.bestssID = R.ssID
				and E.resName = R.resName
		return 0
	end

if @ResourceCategory = 'Water Supply'
	begin
		Select R.*,
			E.LifeYear AS [Survey Year],
			E.bestssID,
		E.ActualssID,
		E.SurveydimensionssID,
			E.Estimate,
			E.Offset AS [Age of Data],
			E.bestYear AS [Year of Data],
			E.bestssqLevel AS [Data Quality Level],
			E.ActualssqLevel AS [Survey Year Quality Level]
		from #ebs E left join vtblWaterSupply R
			on E.bestssID = R.ssID
				--and E.resName = R.resName
		return 0
	end

if @ResourceCategory = 'Power Supply'
	begin
		Select R.*,
			E.LifeYear AS [Survey Year],
			E.bestssID,
			E.ActualssID,
			E.SurveydimensionssID,
			E.Estimate,
			E.Offset AS [Age of Data],
			E.bestYear AS [Year of Data],
			E.bestssqLevel AS [Data Quality Level],
			E.ActualssqLevel AS [Survey Year Quality Level]
		from #ebs E left join vtblPowerSupply R
			on E.bestssID = R.ssID

		return 0
	end


if @ResourceCategory = 'Staff Housing'
	begin
		Select
			E.LifeYear AS [Survey Year],
			E.bestssID,
			E.ActualssID,
			E.SurveydimensionssID,
			E.Estimate,
			E.Offset AS [Age of Data],
			E.bestYear AS [Year of Data],
			E.bestssqLevel AS [Data Quality Level],
			E.ActualssqLevel AS [Survey Year Quality Level],
			R.*
			from #ebs E left join vtblTeacherHousing R
				on E.bestssID = R.ssID
				--and E.resName = R.resName
		return 0
	end

-- 22 03 2009 Text Books query PIVREsourcesBooksData
-- is expected to contain the names Available, Adequate etc, not resAvail,
if @ResourceCategory = 'Text Books'
	begin
		Select
			E.LifeYear AS [Survey Year],
			E.bestssID,
			E.ActualssID,
			E.SurveydimensionssID,
			E.Estimate,
			E.Offset AS [Age of Data],
			E.bestYear AS [Year of Data],
			E.bestssqLevel AS [Data Quality Level],
			E.ActualssqLevel AS [Survey Year Quality Level],
			R.*,
			R.resAvail Available,
			R.resAdequate as Adequate,
			R.resNumber as Number,
			R.resCondition as Condition,
			R.resName as ResourceType,
			case when resAvail = 1 then 'Y' else 'N' end AvailableYN,
			case when resAdequate = 1 then 'Y' else 'N' end AdequateYN

			from #ebs E left join vtblTextBooks R
				on E.bestssID = R.ssID
				--and E.resName = R.resName
		return 0
	end

-- default format for a specific resource category
-- returns only the resource table with the default headings
Select R.*,
	E.LifeYear AS [Survey Year],
	E.bestssID,
	E.ActualssID,
	E.SurveydimensionssID,
	E.Estimate,
	E.Offset AS [Age of Data],
	E.bestYear AS [Year of Data],
	E.bestssqLevel AS [Data Quality Level],
	E.ActualssqLevel AS [Survey Year Quality Level]
from #ebs E left join Resources R
	on E.bestssID = R.ssID
		WHERE R.resName = @ResourceCategory

return 0


END
GO
GRANT EXECUTE ON [dbo].[sp_vtblResourcesEst] TO [pSchoolRead] AS [dbo]
GO

