SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentWrite].[createTeacherInPayroll]
	-- Add the parameters for the stored procedure here
	@NamePrefix nvarchar(20) = null
	, @NameFirst nvarchar(50)
	, @NameMiddle nvarchar(50) = null
	, @NameLast	nvarchar(50)
	, @NameSuffix nvarchar(10)
	, @doB		datetime
	, @Sex		nchar(1)
	, @YearStarted int
	, @DatePSAppointed datetime = null
	, @DateRegistered datetime = null
	, @fNameCleaned int=1

AS
BEGIN
	SET NOCOUNT ON;
	declare @PayrollInt int

	declare  @Counter table
	(
		intCounter int
		, seq int
		, NumAllocations int
		, charCounter nvarchar(20)
	)
	begin try

	if @fNAmeCleaned = 0 and (@NamePrefix is null and @NameMiddle is null AND @NameSuffix is null)
		begin
			Select @NamePrefix = N.Honorific
					, @NameFirst = N.FirstName
					, @NameMiddle = N.MiddleName
					, @NAmeLast = N.LastName
					, @NameSuffix = N.Suffix
			from dbo.fnParseName(@NameFirst + ' ' + @NameLast) N
		end

	begin transaction
		INSERT INTO @Counter
		exec dbo.GetCounter
			'PAYROLL'

		INSERT INTO TEacherIdentity
			(tPayroll
			, tNamePrefix
			, tGiven
			, tMiddleNames
			, tSurname
			, tDOB
			, tSEx
			, tYearStarted
			, tDatePSAppointed
			, tDateRegister)

			Select charCounter
				, @NamePrefix
				, @NameFirst
				, @NameMiddle
				, @NameLast
				, @DOB
				, @Sex
				, @YearStarted
				, @DatePSAppointed
				, @DateRegistered
			from @Counter
		commit transaction
		select * from TeacherIdentity WHERE tID = scope_identity()		-- beware the add to the log in the trigger
	end try

--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


END
GO

