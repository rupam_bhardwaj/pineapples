SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2017
-- Description:	accepts a set of school number and returns those that are not valid
-- the Xml is a collection of all the schools represented on any page of the data survey workbook
-- called from Controllers_Api/NdoeController/upload
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeValidateSchools]
	-- Add the parameters for the stored procedure here
	@xml xml
AS
BEGIN
	SET NOCOUNT ON;

	-- this is the structure of the xml ageument:
	-- Note that Sheet_Name is taken from the @Sheet attribute on School_No, rather than the parent node @name
	-- this was fond to be far more efficinet in execution
/*
<?xml version="1.0"?>
<Schools>
	<Sheet name="Students">
		<School_No Sheet="Students" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Index="0">KSA201</School_No>
		<School_No Sheet="Students" Index="1">KSA201</School_No>
		<School_No Sheet="Students" Index="2">KSA201</School_No>
	</Sheet>
	<Sheet name="WASH">
		<School_No Sheet="WASH" Index="0">KSA201</School_No>
		<School_No Sheet="WASH" Index="1">KSA203</School_No>
	</Sheet>
</Schools>
*/

	-- collect all school nos that appear on any sheet
	declare @all table
	(
		School_No nvarchar(50)
		, Sheet_Name nvarchar(50)
		, rowID int
	)

	INSERT INTO @all
	Select
		nullif(v.value('.', 'nvarchar(50)'),'') School_No
		--, null
		--, null
		, v.value('@Sheet', 'nvarchar(50)') Sheet_Name
		, v.value('@Index', 'int') rowID
	FROM @xml.nodes('/Schools/Sheet/School_No') as V(v)

	DELETE FROM @all
	FROM @all
	INNER JOIN Schools S
		ON [@all].School_No = S.schNo

	-- bit kludgey - we are allowed to have the PNIDOE on the staff page
	-- FSM specific rule
	DELETE FROM @all
	WHERE Sheet_Name = 'SchoolStaff'
	AND School_No like '%%%DOE'

	Select Sheet_Name
	, rowID
	, isnull(School_No,'(missing)') School_No
	from @all
	ORDER BY Sheet_Name, rowID


END
GO

