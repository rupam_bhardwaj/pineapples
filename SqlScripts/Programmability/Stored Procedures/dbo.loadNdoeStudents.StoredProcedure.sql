SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of student data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeStudents]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @ndoe TABLE
(
RowIndex int
-- Start Generated Code
	          , SchoolYear                   nvarchar(100) NULL
              , [State]                      nvarchar(100) NULL
              , School_Name                nvarchar(100) NULL
              , School_No                  nvarchar(100) NULL
              , School_Type                nvarchar(100) NULL
              , National_Student_ID        nvarchar(100) NULL
              , First_Name                 nvarchar(100) NULL
              , Middle_Name                nvarchar(100) NULL
              , Last_Name                  nvarchar(100) NULL
              , Full_Name                  nvarchar(100) NULL
              , Gender                     nvarchar(100) NULL
              , inDate_of_Birth            nvarchar(100) NULL
              , DoB_Estimate               nvarchar(50) NULL
              , Age                        int NULL
              , Citizenship                nvarchar(100) NULL
              , Ethnicity                  nvarchar(100) NULL
              , Grade_Level                nvarchar(100) NULL
              , [From]                       nvarchar(100) NULL
              , Transferred_From_which_school   nvarchar(100) NULL
              , Transfer_In_Date                nvarchar(100) NULL
              , SpEd_Student               nvarchar(100) NULL
              , Full_Name_2                nvarchar(100) NULL
              , Days_Absent                int NULL
              , Completed                 nvarchar(100) NULL
              , Outcome                    nvarchar(100) NULL
              , Dropout_Reason             nvarchar(100) NULL
              , Expulsion_Reason           nvarchar(100) NULL
              , Post_secondary_study       nvarchar(100) NULL
-- END generated Code
, fileReference		uniqueidentifier
, studentID uniqueidentifier		-- search in the Student_ table

-- put come normalised values in here
, clsLevel nvarchar(10)			-- Grade Level is the grade name, translate back to code
, tfrSchNo nvarchar(50)			-- school number transferred from Transferred_From_which_school is the school name
, ssID		int

-- cleanups
, Date_of_Birth          date NULL
)

--- validations table
declare @val TABLE
(
	rowID int,
	errorValue nvarchar(100),
	errorMessage nvarchar(200)
)

--- warnings thable - these are mismatches on NationalID numbers
---- between current and exisitng data
--- they may be of two kinds :
--- the IDs match, but the data does not
--- the data matches (first name,last name,  DoB gender) but the IDs are different
declare @warnings TABLE
(
	rowID int,
	Id	nvarchar(50),
	givenName nvarchar(100),
	lastName  nvarchar(100),
	dob		  date,
	dobEstimate int,
	registerId	nvarchar(50),
	registerGivenName nvarchar(100),
	registerLastName  nvarchar(100),
	registerDob		  date,
	registerDobEstimate int
)


declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),8,4) as int)
From @xml.nodes('ListObject') as V(v)

Select @districtID = dID
from Districts WHERE dName = @districtName

print @SurveyYear
print @DistrictID


--INSERT INTO NdoeStudents_
INSERT INTO @ndoe
Select
v.value('@Index', 'int') [ RowIndex]
-- Start Gendereted Code -- a macro in the workbook can generate this from column names
, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                                       [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')											[State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                                      [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                                        [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                                      [School_Type]
, nullif(ltrim(v.value('@National_Student_ID', 'nvarchar(100)')),'')                              [National_Student_ID]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                                       [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                                      [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                                        [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                                        [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')											[Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')								[Date_of_Birth]
, nullif(ltrim(v.value('@DoB_Estimate', 'nvarchar(50)')),'')												[DoB_Estimate]
, nullif(ltrim(v.value('@Age', 'int')),'')														  [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                                      [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                                        [Ethnicity]
, nullif(ltrim(v.value('@Grade_Level', 'nvarchar(100)')),'')                                      [Grade_Level]
, nullif(ltrim(v.value('@From', 'nvarchar(100)')),'')                               [From]
, nullif(ltrim(v.value('@Transferred_From_which_school', 'nvarchar(100)')),'')                                  [Transferred_From_which_school]
, nullif(ltrim(v.value('@Transfer_In_Date', 'nvarchar(100)')),'')                                 [Transfer_In_Date]
, nullif(ltrim(v.value('@SpEd_Student', 'nvarchar(100)')),'')                                     [SpEd_Student]
, nullif(ltrim(v.value('@Full_Name_2', 'nvarchar(100)')),'')                                      [Full_Name_2]
, nullif(ltrim(v.value('@Days_Absent', 'nvarchar(100)')),'')                                      [Days_Absent]
, nullif(ltrim(v.value('@Completed', 'nvarchar(100)')),'')                                        [Completed]
, nullif(ltrim(v.value('@Outcome', 'nvarchar(100)')),'')                                          [Outcome]
, nullif(ltrim(v.value('@Dropout_Reason', 'nvarchar(100)')),'')                                   [Dropout_Reason]
, nullif(ltrim(v.value('@Expulsion_Reason', 'nvarchar(100)')),'')                                 [Expulsion_Reason]
, nullif(ltrim(v.value('@Post_secondary_study', 'nvarchar(100)')),'')                             [Post_secondary_study]
-- End Generated Code
, @fileReference
, NEWID()
-- placeholders for the nomarlsied fields
, null
, null
, null
-- cleanups
, null				-- Date_Of_Birth
from @xml.nodes('ListObject/row') as V(v)

-- poulate the normalised values
-- normalise school names for transfer schools
UPDATE @ndoe
SET tfrSchNo = S.schNo
FROM @ndoe
LEFT JOIN Schools S
	ON [@ndoe].Transferred_From_which_school in (S.schNo, S.schName)

UPDATE @ndoe
SET Date_Of_Birth = convert(date, inDate_Of_Birth)
WHERE ISNUMERIC(inDate_Of_Birth) = 0

UPDATE @ndoe
SET Date_Of_Birth = cast( convert(int, inDate_Of_Birth) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Birth) = 1

-- normalise grade levels
UPDATE @ndoe
SET clsLevel = codeCode
FROM @ndoe
LEFT JOIN lkpLEvels L
	ON [@ndoe].Grade_Level = L.codeDescription


-- normalise Gender
UPDATE @ndoe
SET Gender = case when codeCode is null then  Gender else codeCode end		-- avoid isnull becuase it will case as nvarchar(1)
FROM @ndoe
LEFT JOIN lkpGender G
	ON [@ndoe].Gender = G.codeDescription


-- Validations on the input data - the user can abort and correct it in the source document

declare @missingSchool int
declare @missingGrade int
declare @missingDoB int
declare @missingNatID int

declare @UnknownSchool int
declare @badStateSchool int

declare @unknownGrade int
declare @missingName int
declare @missingTransferSchool int

-- Validation 1) school must be supplied on every record

SELECT @missingSchool = count(*)
FROM @ndoe
WHERE School_No is null

INSERT INTO @val
SELECT RowIndex
, null
, 'No school'
FROM @ndoe
WHERE School_Name is null

-- it must be a valid school code
SELECT @unknownSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in (Select schNo from Schools)

INSERT INTO @val
SELECT RowIndex
, School_No
, 'unknown school no'
FROM @ndoe
WHERE School_No not in (Select schNo from Schools)
and School_No is not null

-- it must be in the nominated state
SELECT @badStateSchool = count(DISTINCT School_No)
from @ndoe
WHERE School_No not in
(Select schNo from Schools
 INNER JOIN Islands I
 ON Schools.iCode = I.iCode
 	WHERE I.iGroup <> @districtID
	)

INSERT INTO @val
Select RowIndex
, School_No
, 'School not in state'
FROM @ndoe
WHERE School_No not in
(Select schNo from Schools
 INNER JOIN Islands I
 ON Schools.iCode = I.iCode
 	WHERE I.iGroup = @districtID
	)
and School_No in (Select schNo from Schools)

-------

-- Validation 2) grade must be supplied on every record

SELECT @missingGrade = count(*)
FROM @ndoe
WHERE Grade_Level is null


INSERT INTO @val
Select RowIndex
, null
, 'no grade level'
FROM @ndoe
WHERE Grade_Level is null


-- it must be a defined  grade level

Select @unknownGrade = COUNT(DISTINCT Grade_Level)
FROM @ndoe
WHERE Grade_Level not in (Select codeDescription from lkpLevels)

INSERT INTO @val
Select RowIndex
, Grade_Level
, 'unknown grade level'
FROM @ndoe
WHERE  Grade_Level not in (Select codeDescription from lkpLevels)


-- Validation 3 date of birth must be on every record (even if there is a estimate)

SELECT @missingDoB = count(*)
FROM @ndoe
WHERE Date_of_Birth is null and
( isnull(Dob_Estimate,'')  <> 'unknown')

INSERT INTO @val
Select RowIndex
, null
, 'no date of birth'
FROM @ndoe
WHERE Date_of_Birth is null and
( isnull(Dob_Estimate,'')  <> 'unknown')


-- Validation 4 missing national student ID

SELECT @missingNatID = count(*)
FROM @ndoe
WHERE National_Student_ID is null

INSERT INTO @val
Select RowIndex
, null
, 'no Student ID number'
FROM @ndoe
WHERE National_Student_ID is null

-- Validation 5 some part of name

SELECT @missingName = count(*)
FROM @ndoe
WHERE First_Name is null and Last_Name is null

INSERT INTO @val
Select RowIndex
, null
, 'no name'
FROM @ndoe
WHERE First_Name is null and Last_Name is null

-- Validation 6 -- gender must be supplied and valid
INSERT INTO @val
Select RowIndex
, null
, 'missing gender'
FROM @ndoe
WHERE Gender is null

INSERT INTO @val
Select RowIndex
, Gender
, 'unknown gender value'
FROM @ndoe
WHERE Gender is not null and Gender not in (select codeCode from lkpGender)

-- Validation 6 Student Id must be unique in the input file

INSERT INTO @val
Select RowIndex
,  National_Student_ID
, 'duplicate Student ID'
FROM @ndoe
WHERE National_Student_ID in
(
	Select National_Student_ID
	FROM @Ndoe
	GROUP BY National_Student_ID
	HAVING count(*) > 1
)

-- Validation 7 transfer school must be valid if it is supplied, but if its not there, we can;t do much

SELECT @missingTransferSchool = count(DISTINCT Transferred_From_which_school)
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null

INSERT INTO @val
Select RowIndex
, Transferred_From_which_school
, 'Transfer origin school not known'
FROM @ndoe
WHERE [From] = 'Transfer In' and Transferred_From_which_school is not null
and tfrSchNo is null

--- Return the collection of errors
declare @NumValidationErrors int

select V.*
, N.Full_Name
from @val V
INNER JOIN @ndoe N
ON V.rowID = N.RowIndex
ORDER BY V.rowID, V.errorMessage

Select @NumValidationErrors = @@ROWCOUNT
-- Return any warnings about ID numbers
Select *
from @warnings

----- IF THERE ARE VALIDATION ERRORS LOGGED RETURN HERE
----- the data will not be processed
if (@NumValidationErrors > 0) return

------ Validation 6 - report any matches on family name, given name, DoB that have variant National ID
----SELECT stuGiven
----, stuFamilyName
----, stuDoB
----, stuGender
----FROM Student STU
----INNER JOIN @ndoe NDOE
----ON NDOE.First_Name = STU.stuGiven
----AND NDOE.Last_Name = STU.stuFamilyName
----AND NDOE.Gender = STU.stuGender
----AND NDOE.Date_of_Birth = STU.stuDoB

------ Validation 7 : Matcing National IDs but some difference on name or DoB

----SELECT stuGiven
----, stuFamilyName
----, stuDoB
----, stuGender
----, NDOE.First_Name
----, NDOE.Last_Name
----, NDOE.Date_of_Birth
----, NDOE.Gender

----FROM Student STU
----INNER JOIN @ndoe NDOE
----ON NDOE.National_Student_ID = STU.stuCardID
----WHERE (
----	NDOE.Last_Name <> STU.stuFamilyName
----	OR NDOE.Gender <> STU.stuGender
----	OR
----		( NDOE.Date_of_Birth <> STU.stuDoB
----			AND NDOE.DoB_Estimate = 0
----			AND STU.stuDoBEst = 0
----		)
----)

----- PROCESSING --------


-- Student Level records
-- Ho
-- How to identify an exisitng student record:
-- Must match on National ID, PLUS 2 of Last_Name DoB School of Enrolment ( this year same as last)
UPDATE @ndoe
SET studentID = stuID
FROM @ndoe
INNER JOIN Student_ STU
ON [@ndoe].National_Student_ID = STU.stuCardID
AND (
		[@ndoe].Last_Name = STU.stuFamilyName
		AND
		[@ndoe].Date_of_Birth = STU.stuDoB
	)

-- now match on National ID, 1 of name and DoB, and current school and previous school
UPDATE @ndoe
SET studentID = STU.stuID
FROM @ndoe
INNER JOIN Student_ STU
	ON [@ndoe].National_Student_ID = STU.stuCardID
	AND (
		[@ndoe].Last_Name = STU.stuFamilyName
		OR
		[@ndoe].Date_of_Birth = STU.stuDoB
	)
INNER JOIN StudentEnrolment_ STUE
	ON STU.stuID = STUE.stuID
WHERE
	STUE.stueYear = (@SurveyYear - 1)
	AND STUE.schNo = [@ndoe].School_No
	AND ( [@ndoe].[From] is null or [@ndoe].[From] in ('ECE', 'REP'))


 -- now any retireved identities are on @ndoe
 -- other records not matched
 print 'update student'
 UPDATE Student_
 SET stuEthnicity = ndoe.Ethnicity
 , stuEditFileREf = @filereference
 FROM Student_
 INNER JOIN  @ndoe ndoe
 ON ndoe.studentID = Student_.stuID

 -- Now insert any unidientified records
 print 'insert student'
 INSERT INTO Student_
 (
 stuID
 , stuGiven
 , stuFamilyName
 , stuDoB
 , stuDobEst
 , stuGender
 , stuCardID
 )
 Select
 StudentID
 , First_Name
 , Last_Name
 , Date_of_Birth
 , case DoB_Estimate when null then null when 'Yes' then null else 1 end
 , left(Gender,1)
 , National_Student_ID
 FROM @ndoe ndoe
 WHERE ndoe.studentID not in (Select stuID from Student_)

 -- Populate the StudentEnrolment_ records

 -- clear any existing entries
 DELETE FROM StudentEnrolment_
 WHERE schNo in (Select schNo from @ndoe)
 AND stueYear = @SurveyYear

 print 'insert student enrolment'
 INSERT INTO StudentEnrolment_
(
    stuID
    ,schNo
    ,stueYear
    ,stueClass
    ,stueFrom
    ,stueFromSchool
    ,stueFromDate
    ,stueSpEd
    ,stueDaysAbsent
    ,stueCompleted
    ,stueOutcome
    ,stueOutcomeReason
)
SELECT
StudentID
, School_No
, @SurveyYear
, clsLevel
, case [From] when 'Repeater' then 'REP'
				when 'Transferred In' then 'TRIN'
				when 'ECE' then 'ECE' end
, tfrSchNo
, Transfer_In_Date
, case SpEd_Student when 'No' then 0 when 'Yes' then 1 else null end
, Days_Absent
, case Completed when 'No' then 'N' when 'Yes' then 'Y' else null end
, Outcome
, Dropout_Reason
FROM @ndoe

-- create any records required in SchoolSurvey
-- but they should not be needed if we have already processed Schools

INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
)
Select DISTINCT @SurveyYear
, NDOE.School_No
, schType
FROM @ndoe NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo
LEFT JOIN SchoolSurvey SS
	ON NDOE.School_No = SS.schNo
	AND @SurveyYear = SS.svyYear
WHERE SS.ssID is null

-- for convenience, put the ssID back on the @ndoe table

UPDATE @ndoe
SET ssID = SS.ssID
FROM @ndoe
	INNER JOIN SchoolSurvey SS
		ON SS.schNo = [@ndoe].School_No
		AND SS.svyYear = @SurveyYear

DELETE FROM Enrollments
WHERE ssID in (SELECT ssID from @ndoe)

-- enter the enrolments
INSERT INTO Enrollments
(
ssID
, enAge
, enLevel
, enM
, enF
)
Select ssID
, Age
, stueClass
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
GROUP BY STUE.ssID
, STUE.Age
, stueClass


-- repeaters - these are those with REP in the source column

DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'REP'
AND ssID in (SELECT ssID from @ndoe)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptAge
, ptLevel
, ptM
, ptF
)
Select 'REP'
, ssID
, Age
, stueClass
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
WHERE STUE.stueFrom = 'REP'
GROUP BY STUE.ssID
, STUE.Age
, stueClass

-- pre-school attenders - these are those with ECE in the source column, and enrolment in Year level 1

DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'PSA'
AND ssID in (SELECT ssID from @ndoe)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptAge
, ptM
, ptF
)
Select 'PSA'
, ssID
, Age
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
	LEFT JOIN lkpLevels L
		ON STUE.stueClass = L.codeCode
WHERE STUE.stueFrom = 'ECE'
-- for safety, be sure only to count year 1 enrollees
	AND L.lvlYear = 1
GROUP BY STUE.ssID
, STUE.Age
, stueClass

-- Transfers In
-- pre-school attenders - these are those with ECE in the source column, and enrolment in Year level 1

DELETE FROM PupilTables
WHERE PupilTables.ptCode = 'TRIN'
AND ssID in (SELECT ssID from @ndoe)

INSERT INTO PupilTables
(
ptCode
, ssID
, ptLevel
, ptRow
, ptM
, ptF
)
Select 'TRIN'
, ssID
, stueClass
, TFRI.iGRoup		-- group by the State from which they transferred
, sum(case stuGender
	when 'M' then 1 when 'Male' then 1 else null end) M
, sum(case stuGender
	when 'F' then 1 when 'Female' then 1 else null end) F
FROM StudentEnrolment STUE		-- this view acalculates age and supplies ssID
	LEFT JOIN Schools TFRS
		ON STUE.stueFromSchool = TFRS.schNo
	LEFT JOIN Islands TFRI
		ON TFRS.iCode = TFRI.iCode
WHERE STUE.stueFrom = 'TRIN'
GROUP BY STUE.ssID
, TFRI.iGroup
, stueClass


-- finally return a pupil table of the data we have
-- build a pupil table structure

-- RETURN ENROLMENTS

-- recordset 1: identifying data
Select 'enrolments' tableName
, @SurveyYear surveyYear

-- 2 the row code set -- in this case the school nos

SElect DISTINCT isnull(schNo,'<>') codeCode
, isnull(schNo,'<>') codeDescription
FROM StudentEnrolment STUE
WHERE ssID in (Select ssId from @ndoe)
ORDER BY isnull(schNo,'<>')

---- 3 is the column definitions
--Select codeCode
--, codeDescription
--from lkpLevels
--ORDER BY lvlYear

Select codeCode
, codeDescription
FROM lkpLevels
ORDER BY lvlYear

-- 4 is the data

Select enLevel col
, isnull(schNo,'<>') row
, sum(enM) M
, sum(enF) F
FROM Enrollments E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
GROUP BY enLevel
, SchNo
ORDER BY SchNo
, enLevel


-- RETURN REPEATERS

-- recordset 1: identifying data
Select 'repeaters' tableName
, @SurveyYear surveyYear

-- recordset 2 , 3 the client will reuse the ones above

-- 4 repeater data

Select ptLevel col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
AND ptCode = 'REP'
GROUP BY ptLevel
, SchNo
ORDER BY SchNo
, ptLevel


-- RETURN PSA

-- recordset 1: identifying data
Select 'psa' tableName
, @SurveyYear surveyYear

-- recordset 2 -- reuse from above
--
-- 3 - age range
SELECT num codeCode
, num codeDescription
FRom metaNumbers
WHERE num between 4 and 12
ORDER BY Num

-- 4 psa data

Select ptAge col
, isnull(schNo,'<>') row
, sum(ptM) M
, sum(ptF) F
FROM PupilTables E
	INNER JOIN SchoolSurvey SS
		ON E.ssID = SS.ssID
WHERE E.ssID in (select ssID from @ndoe)
AND ptCode = 'PSA'
GROUP BY ptAge
, SchNo
ORDER BY SchNo
, ptAge

END
GO

