SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[ObjectVocab]
	-- Add the parameters for the stored procedure here
	@ObjectType nvarchar(20) ,
	@ObjectName nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		V.vxEng,
		isnull( CASE dbo.UserSystemLanguage()
					WHEN 0 then V.vxEng
					WHEN 1 then isnull(V.vxFr,G.vxFr)
					WHEN 2 then isnull(v.vxBis, G.vxBis)
				END, V.vxEng) vxTerm
	FROM TrVocab V
		LEFT OUTER JOIN
		(Select vxEng, vxFr, vxBis FROM TrVocab WHERE vxObjType = 'Global') G
		ON V.vxEng = G.vxEng
	WHERE V.vxObjName = @ObjectName
		AND v.vxObjType = @ObjectType
END
GO

