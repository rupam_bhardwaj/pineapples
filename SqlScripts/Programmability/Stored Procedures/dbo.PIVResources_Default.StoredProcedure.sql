SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2013
-- Description:	Default handling of a resource category for pivots
-- Uses the defaut query to establish the usual fields, Available, Number, Condition
-- Note the Somalia variant uses FunctionYN rather than Condition
-- =============================================
CREATE PROCEDURE [dbo].[PIVResources_Default]
	-- Add the parameters for the stored procedure here
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) ,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	print 'Resources DEfault'
   Select *
   INTO #tmpPIVCols
   FROM PIVColsResources
		WHERE Category = @group or @group is null

	exec dbo.PIVResources_DEfault_EXEC

   			@DimensionColumns,
			@DataColumns,
			@Group,
			@SchNo,
			@SurveyYear
END
GO

