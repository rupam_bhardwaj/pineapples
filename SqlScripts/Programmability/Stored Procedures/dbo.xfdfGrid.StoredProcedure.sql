SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xfdfGrid]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@grid xml
AS
BEGIN

/*

	 as of 7 10 2009, the table codes map to data Codes in metaPupilTableDefs, so
	that more than one type of table can write to the same ptCode in Pupil Tables
	For legacy applications, this will work the same (since there will be in metaPupilTableDEfs a generic record for each ptCode)


*/

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;

	declare @grdtbl table(
							Page nvarchar(50) ,
							YearLevel nvarchar(10) ,
							Row nvarchar(20),
							Col nvarchar(20),
							seq int,
							M int,
							F int)
	-- this is the set of column tags for the grid
	-- this is the set of column tags for the grid
	declare @colSet table(

	c nvarchar(2)
	, ck nvarchar(20)
	, cv nvarchar(100)
							)

	-- this is the set of row tags for the grid
	declare @rowSet table(

	r nvarchar(2)
	, rk nvarchar(100)
	, rv nvarchar(100)
	, rAge int
	)

	-- data n the grid
	declare @data TABLE
	(

		c			nvarchar(2)
		, r			nvarchar(2)
		, M		int
		, F		int

	)

	declare @codeType nvarchar(10)			-- the ptCode assocaited to this table
	declare @idoc int

	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'

	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid, @xmlns


-- if the table code is not supplied as an argument, get it from the xml
-- or if it uses the default, it will come from e.g. pineapples@school it eill be TableCode
-- ie there is a default layout definition for each data code
	declare @tableDef nvarchar(20)
	Select @tableDef = TableDef
			from OPENXML (@idoc, '/x:field',2)
			WITH (
				TableDef nvarchar(20) '@name'			-- we pass in the root field node e.g. <field name="REP">
				)


-- populate the table of grid values
	INSERT INTO @data
	Select c,r,M,F

	FROM OPENXML(@idoc,'/x:field/x:field[@name="D"]/x:field/x:field',2)
	WITH
	(

		c			nvarchar(10)		'@name'
		, r			nvarchar(10)		'../@name'
		, M		int					'x:field[@name="M"]/x:value'
		, F		int					'x:field[@name="F"]/x:value'
		)
		WHERE r <> 'T' and c <> 'T'
		AND (M is not null or F is not null)


--- get the complete set of columns
	INSERT INTO @colSet
	Select c, isnull(ck, cv), cv
	FROM OPENXML(@idoc ,'/x:field/x:field[@name="C"]/x:field',2)
	WITH
	(
		c			nvarchar(2)			'@name'
		, ck			nvarchar(20)			'x:field[@name="K"]/x:value'
		, cv			nvarchar(20)			'x:field[@name="V"]/x:value'
	)
	WHERE c <> 'T'			-- ignore any totals
-- get the complete set of rows
	INSERT INTO @rowSet
		(r, rk, rv)
		select r, isnull(rk, rv), rv
		FROM OPENXML(@idoc ,'/x:field/x:field[@name="R"]/x:field',2)
		WITH
		(
			r			nvarchar(2)			'@name'
			, rk			nvarchar(20)			'x:field[@name="K"]/x:value'
			, rv			nvarchar(20)			'x:field[@name="V"]/x:value'
		)
		WHERE r <> 'T'			-- ignore any totals


/*
	-- update the records that match
	UPDATE PupilTables
		set ptM = G.M,
			ptF = G.F
	from PupilTables P inner join @grdtbl G
		on P.ptRow = G.Row and P.ptCol = G.Col

	WHERE P.ssID = @surveyID
			and P.ptCode = @codeType

	INSERT INTO PupilTables(ssID, ptCode, ptRow, ptCol, ptM, ptF)
	SELECT @surveyID, @codeType, G.Row, G.Col, G.M,G.F
	FROM @grdtbl G left join (Select * from PupilTables
	WHERE ssID = @surveyID and ptCode = @codeType) P
	on G.Row = p.ptRow and G.col = p.ptCol
	WHERE ptID is null
    -- Insert statements for procedure here
	-- SELECT @grid
*/
declare @rowSplit int; --0 = ptAge, 1 = ptLevel, 2= ptRow
declare @colSplit int; --0 = ptAge, 1 = ptLevel, 2= ptCol
declare @pageSplit int; --0 = nothing goes in page 1=row number goes in page

Select * from @data
Select * from @colSet
Select * from @rowSet

select @rowSplit =
	case
		when tdefRows is null then -1			-- to support pupiltable formats
		when tdefRows like 'AGE%' then 0
		when tdefRows like 'LEVEL%' then 1
		else 2
	end,
	@colSplit =
	case
		when tdefCols is null then 1		-- level is the default for columns
		when tdefCols like 'AGE%' then 0
		when tdefCols like 'LEVEL%' then 1
		else 2
	end,
	@pagesplit =
	case
		when tdefRows = '<?>' then 1
		else 0
	end,
	@codetype = tdefDataCode
	from MetaPupilTableDefs
	WHERE tdefCode = @tableDef		-- this is the table definition

If (@@RowCount = 0) begin
	Select @ErrorMessage = 'The table code ' + @tableDef + ' is not defined in Pineapples. check the Table Defs in Survey Designer'
	RAISERROR(@ErrorMessage,16,2)
end
-- start the transaction
Select @RowSplit RowSplit
, @colSplit ColSplit

begin transaction

begin try

	-- delete only specified rows and columns
	DELETE from PupilTables
	FROM PupilTables, @rowSet, @colSet
	WHERE ssID = @SurveyID
		AND ptCode = @codeType
		--AND (ptPage = @page or (@page is null))
		AND (	(@rowsplit = -1) OR
				( @rowsplit = 0 and ptAge = rk) OR
			 (@rowsplit = 1 and ptLevel = rk)  OR
			 (@rowsplit = 2 and ptRow = rk)
			)
		AND (( @colsplit = 0 and ptAge = ck) OR
			 (@colsplit = 1 and ptLevel = ck)  OR
			 (@colsplit = 2 and ptCol = ck)
			)
	------Select count(*) from @RowSet
	------select @rowsplit
	------select * from @grdtbl

	INSERT INTO PupilTables(ssID, ptCode, ptPage, ptLevel, ptAge, ptRow, ptCol, ptM, ptF)
	SELECT @surveyID,
	@codeType,
	case	when @pagesplit = 1 then convert(int, R.r)	-- use the actual row number
														--, passed as a 2 character string
			else null
	end,
	-- for level, test the split values
	case
		when @rowsplit = 1 then R.rk			-- the row split is Level
		when @colsplit = 1 then C.ck			-- the column split is Level ( most common)
		--else YearLevel
	end,

	-- for age
	case
		when @rowsplit = 0 then R.rk			--
		when @colsplit = 0 then C.ck
		else null				-- ??? to support tertiary with yearLevel tag on the values node
	end,
	--row generic field ptRow
	case
		when @rowsplit = 2 then R.rk
		else null
	end,
	--col
	case
		when @colsplit = 2 then C.ck
		else null
	end,
	-- values
	D.M, D.F
	FROM @Data D
	 	INNER JOIN @rowSet R
			ON D.R = R.R
		INNER JOIN @colSet C
			ON D.C = C.C

	exec audit.xfdfInsert @SurveyID, 'records inserted',@TableDef,@@rowcount
end try

begin catch

	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,@TableDef
    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit
if @@trancount > 0
	commit transaction

return


END
GO
GRANT EXECUTE ON [dbo].[xfdfGrid] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[xfdfGrid] TO [pSurveyWrite] AS [dbo]
GO

