SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 12 2007
-- Description:	Pupil toiletratio
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDPupilToiletRatioData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- estimate enrol
SELECT *
INTO #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

create unique index eLSchno ON #ebse (LifeYear, schno)

SELECT *
INTO #ebst
from dbo.tfnESTIMATE_BestSurveyToilets()

create unique index tLSchno ON #ebst (LifeYear, schno)

-- select the results
Select
E.LifeYear [Survey Year],
E.schNo,
E.Estimate,
E.bestYear [Year of Data],
E.offset [Age of Data],
E.surveydimensionssID,
ET.Estimate as [Estimate Toilet],
ET.bestYear as [Year of Toilet Data],
ET.offset as [Age of Toilet Data],
null as ToiletType,
ssEnrolM EnrolM,
ssEnrolF EnrolF,
0 as NumToiletsM,
0 as NumToiletsF
from #ebse E
INNER JOIN SchoolSurvey S
ON E.bestssID = s.ssID
LEFT JOIN #ebst ET
	on E.LifeYear = ET.LifeYear and E.schNo = ET.schNo

UNION
Select
E.LifeYear [Survey Year],
E.schNo,
E.Estimate,
E.bestYear [Year of Data],
E.offset [Age of Data],
E.surveydimensionssID,
ET.Estimate as [Estimate Toilet],
ET.bestYear as [Year of Toilet Data],
ET.offset as [Age of Toilet Data],
toiType as ToiletType,
null as EnrolM,
null as EnrolF,
case toiUse WHEN 'Boys' then toiNum else null end NumToiletsM,
case toiUse WHEN 'Girls' then toiNum else null end NumToiletsF
from #ebst ET
INNER JOIN Toilets T
ON ET.bestssID = T.ssID
LEFT JOIN #ebse E
	on E.LifeYear = ET.LifeYear and E.schNo = ET.schNo


END
GO
GRANT EXECUTE ON [dbo].[sp_INDPupilToiletRatioData] TO [pSchoolRead] AS [dbo]
GO

