SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14/11/07
-- Description:	InputDataForEnrolmentPIVsubjectTable
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVEnrolmentSubjectData]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyenrolments()

    -- Insert statements for procedure here
	SELECT SE.* ,
	E.surveyDimensionssID,
	E.Estimate,
	E.Lifeyear as [Survey Year],
	E.Offset as [Age of Data],
	S.subjName
	from
	vtblSubjectEnrol SE
	INNER JOIN #EBSE E
	ON SE.ssID = E.bestssID
	INNER JOIN TRsubjects S
	ON SE.subjCode = S.subjcode


drop TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVEnrolmentSubjectData] TO [pEnrolmentRead] AS [dbo]
GO

