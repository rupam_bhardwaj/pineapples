SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [pEstablishmentRead].[EstablishmentYearRoleGradeInfo]
(
	-- Add the parameters for the function here
	@EstYear int
)
RETURNS
@RGInfo TABLE
(
	-- Add the column definitions for the TABLE variable here
	rgCode nvarchar(10)
	, rgDescription nvarchar(50)
	, rgSalaryLevelMin nvarchar(5)
	, rgSalaryLevelMax nvarchar(5)
	, rgSalaryPointMedian nvarchar(10)
	, Salary money
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
declare @AdjustMedianSalaryPoint int


Select @AdjustMedianSalaryPoint = estcNominalPointAdjust
FROM
	EstablishmentControl
WHERE
	estcYear = @EstYear

declare @rates table
(
	spCode nvarchar(10)
	, salLevel nvarchar(5)
	, spSalary money
	, spHAllow money
	, AnnualTotal money
	, spEffective datetime
	, pointOrder int
)

INSERT INTO @rates
SELECT
	spCode
	, salLevel
	, spSalary
	, spHAllow
	, isnull(spSalary,0) + isnull(spHAllow,0) AnnualTotal
	, spEffective
	-- this row number based on sort is so we can do an increment
	, row_number() OVER (ORDER BY spSort)
FROM
	(
	SELECT
		SalaryPointRates.*
		, spSort
		, salLevel
		-- row number to get the first pay effective date desc
		, row_number() over( PARTITION BY lkpSalaryPoints.spCode ORDER BY spEffective DESC)  RowNum
	FROM
		lkpSalaryPoints
		INNER JOIN SalaryPointRates
			ON LkpSalaryPoints.spCode = SalaryPointRates.spCode
		, EstablishmentControl
	WHERE
		EstablishmentControl.estcYear = @EstYear
		AND spEffective <= estcAuthorityDate
	) sub
	WHERE RowNum = 1


INSERT INTO @RGInfo
SELECT rgCode
	, rgDescription
	, rgSalaryLevelMin
	, rgSalaryLevelMax
	, R.spCode
	, R.AnnualTotal
FROM TRRoleGRades RG
	LEFT JOIN @rates RUnadj
		ON RG.rgSalaryPointMedian = RUnadj.spCode
	LEFT JOIN @rates R
		on (RUnadj.PointOrder + @AdjustMedianSalaryPoint) = R.PointOrder

RETURN
END
GO

