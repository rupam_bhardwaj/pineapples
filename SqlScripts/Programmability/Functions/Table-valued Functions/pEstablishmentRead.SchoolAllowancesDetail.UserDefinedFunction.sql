SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 31 5 2010
-- Description:	Total of allowances payable to teachers at a school, in a year
-- =============================================
CREATE FUNCTION [pEstablishmentRead].[SchoolAllowancesDetail]
(
	-- Add the parameters for the function here
	@EstYear int
)
RETURNS TABLE
AS
RETURN
(
	Select SUB.*
	, (NumApply * estaValue) TotalAllowancePayable
	FROM
	(
		Select S.schNo
		, schName
		, EA.lstName
		, estaCode
		, PA.codeDescription AllowanceName
		, estaValue
		, estaLimitPerSchool
		, Positions
		, case
				when estaLimitPerSchool = 99 then
					-- use the value in the list
					case when cast(lstValue as int) <= 0 then
						-- not < 0
						 -- note the plus here is becuase the value is negative
						 case when Positions + cast(lstValue as int) < 0 then 0
								else Positions + cast(lstValue as int)
						 end
					when Positions > cast(lstValue as int) then cast(lstValue as int)
					else Positions
					end
				when estaLimitPerSchool = -99 then
					-- minus becuase the value is positive
					case when Positions - cast(lstValue as int) < 0 then 0
							else Positions - cast(lstValue as int)
					 end
				when estaLimitPerSchool <= 0 then
						case when Positions + estaLimitPerSchool < 0 then 0
								else Positions + estaLimitPerSchool
						 end
				when Positions > estaLimitPerSchool then estaLimitPerSchool
				else Positions
		  end NumApply

		FROM ListSchools LST
			INNER JOIN Schools S
				ON LST.schNo = S.schNo
			INNER JOIN EstablishmentAllowance EA
				ON LST.lstName = EA.lstName
				AND (EA.estaListValue = LST.lstValue or EA.estaListValue is null)
				AND EA.estaYear = @EstYear
			INNER JOIN TRPayAllowance PA
				ON PA.codeCode = EA.estaCode
			INNER JOIN SchoolEstablishment SE
				ON SE.schNo = S.schNo
					AND SE.estYear = @estYear
			INNER JOIN (
						SELECT SER.estID
							, sum(EstrQuota) as Quota
							, sum(estrCount) as Positions
						FROM SchoolEstablishmentRoles SER

						GROUP BY
							 SER.estID
						) SERTot
				ON SE.estID = SERTot.estID

	) SUB
)
GO

