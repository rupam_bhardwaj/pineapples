SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2007 10 20
-- Description:	return the estimate best survey data
-- can exclude based on a quality block, on any enrolment subcategory
-- for toilets relevant quality item is Rooms/Toilets
-- =============================================
CREATE FUNCTION [dbo].[tfnESTIMATE_BestSurveyToilets]
()
RETURNS
@tbl TABLE
(
	schNo nvarchar(50),
	LifeYear int,
	ActualssID int,
	bestssID int,
	bestYear int,
	Offset int,
	bestssqLevel int,
	ActualssqLevel int,
	SurveyDimensionssID int,
	Estimate smallint
)
AS
BEGIN
-- extract these parameters to variables first - saves much execution time
	declare @FillForward int
	declare @fillBack int
	Select @FillForward = dbo.sysParamInt(N'EST_ROOM_FILL_FORWARD')
	select @FillBack = dbo.sysParamInt(N'EST_ROOM_FILL_BACKWARD')


INSERT INTO @tbl

SELECT subq2.schNo,
	LifeYear,
	ActualssID,
	SS.ssID,
	bestYear,
	LifeYear - BestYear Offset,
	bestssqLevel,
	QI.ssqLevel,
	isnull(ss.ssID, ActualSSID),
	CASE
		WHEN (BestYear IS NULL) THEN NULL
		WHEN LifeYear = BestYear THEN 0
		WHEN QI.ssqLevel = 2 then 2
		ELSE 1
	END AS Estimate
FROM

	(SELECT	schNo, 	LifeYear, ActualssID,
		-- this line extracts the best year from the numeric min(absoffset)
		case
			when min(AbsOffset) = 9999 then null
			when min(AbsOffset)>1000 then LifeYear + (min(absOffset) - 1000)/100
			else LifeYear - min(absOffset)/100
		end  bestYear,
		-- this line extracts the data year ssqLevel from min(absoffset)
		case  (min(absOffset)%10)
			when 9 then null
			else (min(absOffset) % 10)
		end bestssqLevel
	from
		(SELECT     L.schNo, L.svyYear AS LifeYear, L.ActualssID,
		-- we pack the data year as follows 1000+offset*100 + datayear ssqLevel for later years,
		-- offset*100 + datayear ssqLevel for earlier years
				  case when HD.ssId is null then 9999
					else
					(case when HD.SvyYear>l.svyyear then 1000 else 0 end) + abs(l.svyyear -HD.svyyear)*100 + isnull(HD.ssqLevel,9)
					end AbsOffset
						FROM dbo.SchoolLifeYears AS L LEFT OUTER JOIN
							-- hd (ie hasdata) is all the school surveys that have a resource of the selected type, without a quality block
							(Select S.ssID, S.svyYear, s.schNo,QR.ssqLevel from SchoolSurvey S
								LEFT OUTER JOIN dbo.tfnQualityIssues('Rooms','Toilets')QR
									ON S.ssID = QR.ssID
								WHERE S.ssID = any (Select ssID from Toilets)
									AND isnull(QR.ssqLevel,0) < 2
							) HD
							-- for each life year, consider as candidates those surveys that have data, in the selected band of years
							-- based on fillforward and fillback
							ON L.schNo = HD.schNo AND HD.svyYear BETWEEN
							L.svyYear - @FillForward AND L.svyYear + @FillBack) subQ
	GROUP BY schNo, LifeYear, ActualssID
	) subQ2

	LEFT OUTER JOIN SchoolSurvey SS
	-- join back to SchoolSurvey on best year to get the best ssID
		ON subq2.schNo = SS.schNo and subQ2.BestYear = SS.svyYear
	LEFT OUTER JOIN dbo.tfnQualityIssues('Rooms','Toilets') QI
		ON subQ2.ActualssID = QI.ssID

	RETURN
END
GO
GRANT SELECT ON [dbo].[tfnESTIMATE_BestSurveyToilets] TO [public] AS [dbo]
GO

