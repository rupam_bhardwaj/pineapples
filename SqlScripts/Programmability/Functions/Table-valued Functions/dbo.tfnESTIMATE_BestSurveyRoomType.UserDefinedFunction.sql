SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2007 10 20
-- Description:	return the estimate best surveyenrolment data
-- can exclude based on a quality block, on any enrolment subcategory
-- =============================================
CREATE FUNCTION [dbo].[tfnESTIMATE_BestSurveyRoomType]
(
	@RoomType nvarchar(50) = null
)
RETURNS
@tbl TABLE
(
	schNo nvarchar(50),
	LifeYear int,
	rmType nvarchar(50),
	ActualssID int,
	bestssID int,
	bestYear int,
	Offset int,
	bestssqLevel int,
	ActualssqLevel int,
	SurveyDimensionssID int,
	Estimate smallint
)
AS
BEGIN
-- extract these parameters to variables first - saves much execution time
	declare @FillForward int
	declare @fillBack int
	Select @FillForward = dbo.sysParamInt(N'EST_ROOM_FILL_FORWARD')
	select @FillBack = dbo.sysParamInt(N'EST_ROOM_FILL_BACKWARD')


INSERT INTO @tbl

SELECT subq2.schNo,
	LifeYear,
	rmType,
	ActualssID,
	SS.ssID,
	bestYear,
	LifeYear - BestYear Offset,
	bestssqLevel,
	QI.ssqLevel,
	isnull(ss.ssID, ActualSSID),
	CASE
		WHEN (BestYear IS NULL) THEN NULL
		WHEN LifeYear = BestYear THEN 0
		WHEN QI.ssqLevel = 2 then 2
		ELSE 1
	END AS Estimate
FROM

	(SELECT	schNo, 	LifeYear, ActualssID,
		rmType,
		-- this line extracts the best year from the numeric min(absoffset)
		case
			when min(AbsOffset)>1000 then LifeYear + (min(absOffset) - 1000)/100
			else LifeYear - min(absOffset)/100
		end  bestYear,
		-- this line extracts the data year ssqLevel from min(absoffset)
		case  (min(absOffset)%10)
			when 9 then null
			else (min(absOffset) % 10)
		end bestssqLevel
	from
		(SELECT     L.schNo, L.svyYear AS LifeYear, L.ActualssID,
			rmType,
		-- we pack the data year as follows 1000+offset*100 + datayear ssqLevel for later years,
		-- offset*100 + datayear ssqLevel for earlier years
			  (case when S.SvyYear>l.svyyear then 1000 else 0 end) + abs(l.svyyear -S.svyyear)*100 + isnull(S.ssqLevel,9) AbsOffset
		FROM dbo.SchoolLifeYears AS L LEFT OUTER JOIN
			(Select SS.ssID, schNo, svyYear, rmType , rmQualityCode, ssqLevel
				FROM SchoolSurvey SS INNER JOIN Rooms R
						ON SS.ssID = R.ssID
					LEFT JOIN SchoolSurveyQuality Q
						ON R.ssID = Q.ssID AND R.rmQualityCode = Q.ssqSubItem AND Q.ssqDataItem = 'Rooms'
				WHERE isnull(Q.ssqLevel,0) < 2
			) S
			ON L.schNo = S.schNo AND S.svyYear BETWEEN
			L.svyYear - @FillForward AND L.svyYear + @FillBack


		) subQ
-- 2009 02 09 fix the selection
	WHERE (@RoomType is null or @RoomType = rmType)
	GROUP BY schNo, LifeYear, ActualssID, rmType
	) subQ2

	LEFT OUTER JOIN SchoolSurvey SS
	-- join back to SchoolSurvey on best year to get the best ssID
		ON subq2.schNo = SS.schNo and subQ2.BestYear = SS.svyYear
	LEFT OUTER JOIN dbo.tfnQualityIssues('Rooms',@RoomType) QI
		ON subQ2.ActualssID = QI.ssID

	RETURN
END
GO
GRANT SELECT ON [dbo].[tfnESTIMATE_BestSurveyRoomType] TO [public] AS [dbo]
GO

