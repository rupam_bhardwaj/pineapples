SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 11 2007
-- Description:	Get the applicable qualification records for each teacher
-- =============================================
CREATE FUNCTION [dbo].[tfnApplicableQualifications]
(
)
RETURNS
@quals TABLE
(
	-- Add the column definitions for the TABLE variable here
	tchsID int,
	ytqQual nvarchar(10),
	bestYear smallint,
	bestytqID int
)
AS
BEGIN

INSERT into @quals

SELECT
tchsID,
ytqQual,
convert(smallint,SUBSTRING(MaxData,3,4)) bestYear,
convert(int,SUBSTRING(MaxData,8,20)) bestytqID

FROM
	(
		SELECT TeacherSurvey.tchsID,
		ytq.ytqQual,
		Max(
		case when ytqSector is null then '0' else '1' end +
		'-' +
		convert(nchar(4), ytq.svyYear)+
		'-'+
		convert(nchar(20),ytqID)
		) MaxData
		FROM SurveyYearTeacherQual AS ytq, SchoolSurvey AS ss
			INNER JOIN TeacherSurvey ON ss.ssID = TeacherSurvey.ssID
		WHERE (((ytq.ytqQual)=[tchEdQual] Or (ytq.ytqQual)=[tchQual]) AND
		((ytq.ytqSector)=[tchSector] Or (ytq.ytqSector) Is Null) AND ((ytq.svyYear)<=[ss].[svyYear]))
		GROUP BY TeacherSurvey.tchsID, ytq.ytqQual
	) subQ
	-- Fill the table variable with the rows for your result set

	RETURN
END
GO
GRANT SELECT ON [dbo].[tfnApplicableQualifications] TO [public] AS [dbo]
GO

