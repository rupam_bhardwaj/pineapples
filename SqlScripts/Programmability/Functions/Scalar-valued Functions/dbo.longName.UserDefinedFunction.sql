SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-14
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[longName]
(
	-- Add the parameters for the function here
	@Prefix nvarchar(10)
	,@Given nvarchar(50)
	, @Middle nvarchar(50)
	, @FamilyName nvarchar(50)
	, @Suffix nvarchar(10)
)
RETURNS nvarchar(200)
AS
BEGIN
	declare @Sep nvarchar(1)
	DECLARE @Result nvarchar(101)

	if not (isnull(@PRefix,'') = '')
		select @PRefix = rtrim(ltrim(@Prefix)) + ' '
	else
		select @Prefix = ''


	if not (isnull(@Given,'') = '')
		select @Given = ltrim(rtrim(@Given)) + ' '
	else
		select @Given = ''

	if not (isnull(@Middle,'')='')
		select @Middle = ltrim(rtrim(@Middle)) + ' '
	ELSE
		select @Middle = ''

	if not (isnull(@FamilyName,'')='')
		select @FamilyName = ltrim(rtrim(@FamilyName)) + ' '
	else
		select @FamilyName = ''


	select @result = ltrim(rtrim(@Prefix + @Given + @Middle + @FamilyName + isnull(@Suffix,'')))

	-- Declare the return variable here


	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[longName] TO [public] AS [dbo]
GO

