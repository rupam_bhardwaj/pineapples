SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 04 2010
-- Description:	refactored into a function for ease of use in other SPs
-- =============================================
CREATE FUNCTION [common].[fnIs_MemberEx]
(
	-- Add the parameters for the function here
	@RoleOrGroup sysname
)
RETURNS int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	if IS_SRVROLEMEMBER('sysadmin') = 1
		return 1
	else begin
		if is_member('db_owner') = 1
			return 1
		else
			return IS_MEMBER(@RoleOrGroup)
    end
    return 0
END
GO

