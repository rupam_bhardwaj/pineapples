SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-14
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[shortName]
(
	-- Add the parameters for the function here
	@Given nvarchar(50)
	, @FamilyName nvarchar(50)
)
RETURNS nvarchar(200)
AS
BEGIN
	declare @Sep nvarchar(1)
	DECLARE @Result nvarchar(101)

	if (@Given is null or @FamilyName is null)
		select @Sep = ''
	else
		select @Sep = ' '

	select @result = ltrim(rtrim(@Given)) + @sep + ltrim(rtrim(@FamilyName))

	-- Declare the return variable here


	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[shortName] TO [public] AS [dbo]
GO

