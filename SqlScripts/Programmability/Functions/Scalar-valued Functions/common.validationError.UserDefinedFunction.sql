SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 03 2011
-- Description:	Create a structured validation error
-- =============================================
CREATE FUNCTION [common].[validationError]
(
	-- Add the parameters for the function here
	@text nvarchar(1000)
	, @field nvarchar(50) = null
	, @param nvarchar(50) = null
)
RETURNS nvarchar(1000)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(1000)

	DECLARE @openTag nvarchar(100)

	if @field is null
		SELECT @OpenTag = '<ValidationError>'
	else
		SELECT @OpenTag = '<ValidationError field="' + @field + '">'

	if @param is not null
		SELECT @text = REPLACE(@text,'%s',@param)


	SELECT @REsult = @OpenTag + @text + '</ValidationError>'
	-- Add the T-SQL statements to compute the return value here


	-- Return the result of the function
	RETURN @Result

END
GO

