SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 29 5 2010
-- Description:	Active Survey year - this is the year for which changes to school status are synchronised between the school year and schoolyearhistory
-- =============================================
CREATE FUNCTION [common].[ActiveSurveyYear]
(
	-- Add the parameters for the function here

)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = common.sysParamInt('ACTIVE_SURVEY_YEAR', year(getdate()))

	RETURN @Result

END
GO

