CREATE ROLE [pTeacherWrite]
GO
EXEC sys.sp_addextendedproperty @name=N'Comment', @value=N'Permission to write basic teacher data' , @level0type=N'USER',@level0name=N'pTeacherWrite'
GO
EXEC sys.sp_addextendedproperty @name=N'PineapplesUser', @value=N'CoreRole' , @level0type=N'USER',@level0name=N'pTeacherWrite'
GO

