describe("Hello world", function () {
    it("says hello", function () {
        expect("hello Saturn").toEqual("Hello world!");
    });
    it("waves goodbye", function () {
        expect("goodbye Pluto").toEqual("goodbye Pluto");
    });
    it("says ta ta ", function () {
        expect("goodbye Venus").toEqual("Hello world!");
    });
});
describe("Indicators controller", function () {
    beforeEach(function () {
        angular.mock.module("pineapples");
    });
    it("is defined in pineapples", function () {
        inject(function (_IndicatorsMgr_) {
            expect(_IndicatorsMgr_).toBeDefined();
        });
    });
});
describe("ui-router", function () {
    var $state;
    var states = [
        'site',
        'site.indicators',
        'site.indicators.kemis',
        'site.indicators.siemis',
        'site.indicators.kemis.pri',
        'site.indicators.siemis.pri',
        'site.indicators.siemis.js',
        'site.indicators.siemis.ss'
    ];
    beforeEach(function () {
        angular.mock.module("pineapples");
        inject(function (_$state_) {
            $state = _$state_;
        });
    });
    function checkState(statename) {
        it("defines route " + statename, function () {
            expect($state.get(statename)).not.toBeNull();
        });
    }
    for (var i = 0; i < states.length; i++) {
        checkState(states[i]);
    }
    ;
});
