﻿describe("ui-router", () => {
  let $state;
  let states = [
    'site'
    , 'site.ataglance'
    , 'site.ataglance.kemis'
    , 'site.ataglance.siemis'
    , 'site.ataglance.kemis.pri'
    , 'site.ataglance.siemis.pri'
    , 'site.ataglance.siemis.js'
    , 'site.ataglance.siemis.ss'
  ];
  beforeEach(() => {
    angular.mock.module("pineapples");
    inject((_$state_) => {
      $state = _$state_;
    });
  });
  function checkState(statename) {
    it( "defines route " + statename , () => {
      expect($state.get(statename)).not.toBeNull();
    });
  }
  for (let i = 0; i < states.length; i++) {
    checkState(states[i]);
  };
});