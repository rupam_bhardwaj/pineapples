﻿module Sw {

  interface IIsolateScope extends ng.IScope {
    width: number;
    height: number;
    url: string;
    mode: string;
    flashstring: string;        // the typed contents of the input
    onChanged: any;
    filter: any;
    placeholder: string;

  }

  let modaltemplate = '<div class="modal-header"> ' +
    ' <h3 class="modal-title" > I\'m a modal!</h3> ' +
    ' </div> ' +
    ' <div class="modal-body"> ' +
    ' <ul> ' +
    ' <li ng- repeat="item in items" > ' +
    ' <a href="#" ng-click="$event.preventDefault(); selected.item = item" > {{ item } ' +
    ' } </a> ' +
    ' </li> ' +
    ' </ul> ' +
    ' Selected: <b>{{ selected.item }}</b> ' +
    ' </div> ' +
    ' <div class="modal-footer" > ' +
    ' <button class="btn btn-primary" type= "button" ng- click="ok()" >OK</button> ' +
    ' <button class="btn btn-warning" type= "button" ng- click="cancel()">Cancel</button> ' +
    ' </div> ';
  // this controller is for the modal popup
  class ModalController {
  }
  // introducing a local controller that is confined to this closure
  // this overcomes issues of assigning methods to the instance

  class LocalController {
    public noResults: boolean;
    public loading: boolean;

    private _lastFetchString = "";
    private _fetchData: any[] = [];

    // injected services
    public http: any;
    public uibModal: any;
    public ngModel: any;
    // scope
    public scope: IIsolateScope;
    public doSearch(s: string) {
      if (s.length < 3) {
        // if we don't have 3 chars don;t do anything
        return [];
      }
      s = s.toLowerCase();
      let l = this._fetchData.length;
      // we don;t need to fetch:
      // the current search is the last fetch
      if (s === this._lastFetchString) {
        return this._filterLocal(s);
      }
      // the current search is backspaced and there is already a full list
      if (_.startsWith(this._lastFetchString, s) && l === 30) {
        return this._filterLocal(s);
      }
      // the current search string is longer than  the last search and that search returned < 30 records
      // ie we have everything
      if (_.startsWith(s, this._lastFetchString) && l > 0 && l < 30) {
        return this._filterLocal(s);
      }
      // return the promise with its resolution
      return this.http.post(this.scope.url, "\"" + s + "\"")
        .then( (response) => {
          this._fetchData = response.data.ResultSet[0];
          this._lastFetchString = s;
          return this._fetchData;
        });
    };
    private _filterLocal = (s) => {
      return _.filter(this._fetchData, d => { return (d.N.toLowerCase().indexOf(s) >= 0); });
    };

    public onSelect($item, $model, $label) {
      // now we have a unique element that should be returned to the paramChanged event handler
      if (this.ngModel) {
        this.ngModel.$setViewValue($item);
      }
      if (this.scope.onChanged) {
        this.scope.onChanged($item);
      }
      if (this.scope.filter) {
        if (this.scope.mode === "finder") {
          // TO DO?
        } else {
          this.scope.filter.params = { SchoolNo: $item.C };
          this.scope.filter.FindNow();
        };
      };
    };

    public onKey($event) {
      if ($event.which === 13) {
        // if there is no results, return a params collection for searching
        // we parse the selection string
        if (this.noResults && this.scope.filter) {
          // a filter has been provided - in finder mode use it pop up a modal
          // in flash mode (default) just invoke the method
          if (this.scope.mode === "finder") {
            // make a modal
            var modalInstance = this.uibModal.open({
              animation: true,
              template: modaltemplate,
              //  controller: "ModalInstanceCtrl",
              size: "lg"
              // resolve: {
              //  items: function () {
              //    return $scope.items;
              //  }
            });
          } else {
            this.scope.filter.FlashFind(this.scope.flashstring);
          }
        }
      }
    }
  }

  class FilterEditor implements ng.IDirective {
    public template = '  <input type="text"  ng-model="flashstring" placeholder="{{placeholder}}"' +
    ' uib-typeahead="s.N for s in vm.doSearch($viewValue)"' +
    ' typeahead-loading="vm.loading"' +
    ' typeahead-append-to-body="true"' +
    ' typeahead-no-results="vm.noResults" class="form-control"' +
    ' typeahead-on-select="vm.onSelect($item, $model, $label)"' +
    ' ng-keypress="vm.onKey($event)">';

    // note the name filterEditor - its the directive name, not the function name
    public require = ["?ngModel", "filterEditor"];    // get the array sent in to the link
    public controller = LocalController;
    public controllerAs = "vm";
    public link: ng.IDirectiveLinkFn;
    public scope = {
      width: "@",
      height: "@",
      url: "@",
      mode: "@",
      onChanged: "=",
      filter: "=",
      placeholder: "@"

    };
    public restrict = "EA";

    constructor($http, $uibModal) { // constructor gets list of dependencies too

      FilterEditor.prototype.link = (scope: IIsolateScope, element, attrs, ctrlrs) => {
        // trap the svg element
        let localCtlr: LocalController = <LocalController>ctrlrs[1];
        localCtlr.http = $http;
        localCtlr.uibModal = $uibModal;
        localCtlr.scope = scope;

        let ngModel = ctrlrs[0];
        if (ngModel) {
          ngModel.$render = () => {
            if (ngModel.$viewValue && ngModel.$viewValue.N !== undefined) {
              scope.flashstring = ngModel.$viewValue.N;
            };
          };
          localCtlr.ngModel = ngModel;
        }

      };
    }

    public static factory() {
      var directive = ($http, $uibModal) => {
        return new FilterEditor($http, $uibModal);
      };
      directive['$inject'] = ['$http', '$uibModal'];
      return directive;
    }
  }

  angular
    .module("sw.common")
    .directive("filterEditor",  FilterEditor.factory());
}
