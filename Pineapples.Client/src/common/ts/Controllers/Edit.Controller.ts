﻿module Sw {



  export class EditController {


    public modelForm: ng.IFormController;   // angular matches this to the name of the form in the view

    private modelInit: any;
    private modelNew: any;      // if we entered in new mode, keep that initial form to reuse
    public isWaiting = false;
    public isEditing = false;
    public isNew = false;
    public atEditStart = false;   // flag to force the main editable tab to be selected when switching to edit mode

    static $inject = ["$timeout", "ApiUi", "$stateParams", "api", "data"];
    constructor( private $timeout: ng.ITimeoutService, private apiUi: Sw.Api.IApiUi, stateparams: ng.ui.IStateParamsService, private api : any, public model: Sw.Api.IEditable) {
      this.pristine();
      if (stateparams["id"] === null) {
        this.isNew = true;
        this.modelNew = angular.copy(this.model.plain()); // we are copying over all the restangular methods as well...
        // if its a new record, default straight to editing mode
        this.setEditing(true);
      }
    }
    public setEditing(editing: boolean) {
      if (editing) {
        this.atEditStart = true;
      }
      this.isEditing = editing;

    }

    public save() {
      // use the retangular method
      this.isWaiting = true;
      let promise: ng.IPromise<any> = null;
      if (this.isNew) {
        promise = this.model.post();
      } else {
        promise = this.model.put();
      }
      promise.then(
        // success reponse - the matching data fields are returned from the server
        (newData) => {
          this.isWaiting = false;
          if (this.isNew) {
            // by convention, new will return the same data as get (read),
            // whereas put just returns the affected fields
            // so we replace the model entirely in this case:
            this.model = newData;
            this.pristine();

            this.apiUi.addNewReport(this.model).then((response) => {
              // Create a new one
              this.isNew = true;
              // remove any values from model that are not functions
              this.api.new().then((newData) => {
                this.model = newData;
                this.pristine();
                this.setEditing(true);
              });
            }, (errorData) => {
              this.isNew = false;
            });

          } else {
            // update fields on the  record from the returned data
            // use .plain() method to ignore restangular adornments added to newData
            // call a handler on the object in case it wants to do more stuff
            this.model._onUpdated(newData.plain());
            this.pristine();
          }
        },
        (errorData) => {
          // TO DO some analysis of the error like in RowEditManager,
          // with opportunity to recover from Concurreny error, or continue if validation error,
          // or even back out by restoring this.SchoolInit
          this.isWaiting = false;
          this.apiUi.showErrorResponse(this.model, errorData).then((response) => {
            switch (response) {
              case "retry":
                // retry the save  - note this is not re-entrant becuase it is a callback from the promise
                this.save();
                break;
              case "overwrite":
                // force a save by overwriting the Rowversion and try again
                this.model._rowversion = errorData.data[this.model._rowversionProperty()];
                this.save();
                break;
              case "discard":
                // in a conflict, use the new data
                angular.extend(this.model, errorData.data);
                this.pristine();
                break;
            }
          });
        });
    }

    public undo() {
      // TO DO not quite going to work with New, becuase modelInit does not have all the properties that model has
      angular.extend(this.model, this.modelInit);
      this.pristine();
    }

    public refresh() {
      this.model.get().then((newData) => {
        this.model = newData;
        this.pristine();

      }, (error) => {
      });
    }

    private pristine() {
      if (this.modelForm) {
        this.modelForm.$setPristine();
        this.modelInit = angular.copy(this.model.plain());      // if we want to implement Undo
        this.setEditing(false);
      }
    }

  }

  angular
    .module('sw.common')
    .controller('EditController', EditController);
}
