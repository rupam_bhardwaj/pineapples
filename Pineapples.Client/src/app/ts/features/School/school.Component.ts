﻿namespace Pineapples.Schools {
  interface IBindings {
    model: School;
  }
  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: School;

    static $inject = ["ApiUi", "schoolsAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

    /**
     * Callback for reports - by passing this to the report-list component,
     * it is attached to every report in the list
     * So, this stub will get called whenever any report is generated from the report-list
     * @param report - the report definition
     * @param context - the report context is the school; i.e. this.model
     * @param instanceInfo - the returned instanceInfo
     */
    public setReportParams(report, context, instanceInfo) {
      console.log(report);
    }
  }

  angular
    .module("pineapples")
    .component("componentSchool", new Sw.Component.ItemComponentOptions("school", Controller));
}