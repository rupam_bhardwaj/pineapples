﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "quarterlyreports");
    svc.read = (id) => svc.get(id).catch(errorService.catch);

    svc.new = () => {

      let e = new Pineapples.QuarterlyReports.QuarterlyReport({
        qrID: null
      });
      restAngular.restangularizeElement(null, e, "quarterlyreports");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("quarterlyReportsAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
