﻿module Pineapples.Api {

  let factory = (restAngular, errorService) => {
    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "perfAssess");
    svc.read = (id) => svc.get(id).catch(errorService.catch);
    svc.addRestangularMethod("template", "post", "item/template");
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("perfAssessAPI", ['Restangular', 'ErrorService', factory]);
}
