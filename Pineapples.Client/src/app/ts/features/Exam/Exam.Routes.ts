﻿// Exams Routes
namespace Pineappples.Exams {

  let routes = function ($stateProvider) {
    var featurename = 'Exams';
    var filtername = 'ExamFilter';
    var templatepath = "exam";
    var tableOptions = "examFieldOptions";
    var url = "exams";
    var usersettings = null;
    //var mapview = 'ExamMapView';

    // root state for 'exams' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath, url, usersettings, tableOptions);

    // default 'api' in this feature is examsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "examsAPI";

    let statename = "site.exams";
    $stateProvider.state(statename, state);

    // takes a list state
    state = Sw.Utils.RouteHelper.listState("Exam");
    statename = "site.exams.list";
    state.url = "^/exams/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/exams/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("exam/upload");
        $state.go("site.exams.upload");
      }]
    };
    statename = "site.exams.reload";
    $stateProvider.state(statename, state);
    
    // chart, table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

    // new - state with a custom url route
    state = {
      url: "^/exams/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.exams.list": {
          component: "componentExam"
        }
      },
      resolve: {
        model: ['examsAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.exams.list.new", state);

    state = {
      url: "^/exams/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Exams",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.exams.reports", state);
    
    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/exams/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.exams.list": {
          component: "componentExam"
        }
      },
      resolve: {
        model: ['examsAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.exams.list.item", state);

    state = {
      url: "^/exams/upload",
      views: {
        "@": {
          component: "examUploadComponent"
        }
      },
    };
    $stateProvider.state("site.exams.upload", state);

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}