﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      {
        field: 'stuCardID',
        name: 'Student Card ID',
        displayName: 'Student Card ID',
        editable: false,
        pinnedLeft: true,
        width: 140,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"],
        cellTemplate: 
          `<div class="ui-grid-cell-contents">
            <a ng-click="grid.appScope.listvm.action('item','stuID', row.entity);">
            {{row.entity.stuCardID}}
           </a></div>`,
      },
      {
        field: 'stuGiven',
        name: 'Given Name',
        displayName: 'Given Name',
        pinnedLeft: true,
        width: 160,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
      {
        field: 'stuFamilyName',
        name: 'Family Name',
        displayName: 'Family Name',
        pinnedLeft: true,
        width: 160,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },    
    ]
  };

  class StudentParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };
    
  }
  class StudentFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "studentsAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "student";
      this.ParamManager = new StudentParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }
    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "stuGiven";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "School Type";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      config.tableOptions = "studentFieldOptions";
      config.dataOptions = "studentDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        // resolve it when it the lookup is available
        d.resolve(config);
      }, (reason: any) => {
        // swallow the error
        console.log("Unable to get tableOptions for StudentFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("StudentFilter", StudentFilter);
}
