\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}New Deployment}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Operating System}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Install .NET 4.6 Framework}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Create Account to Run the Application}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Active Directory Server}{6}{section.2.4}
\contentsline {section}{\numberline {2.5}Database Installation}{6}{section.2.5}
\contentsline {section}{\numberline {2.6}Setting up the New Empty Database}{6}{section.2.6}
\contentsline {section}{\numberline {2.7}Main Configuration}{7}{section.2.7}
\contentsline {section}{\numberline {2.8}Setting Up the IdentitiesP Database}{8}{section.2.8}
\contentsline {section}{\numberline {2.9}Module Configuration}{8}{section.2.9}
\contentsline {section}{\numberline {2.10}Make User Owner of both Databases}{9}{section.2.10}
\contentsline {section}{\numberline {2.11}Give User Login Permission}{11}{section.2.11}
\contentsline {section}{\numberline {2.12}Deploy the Application}{12}{section.2.12}
\contentsline {section}{\numberline {2.13}Web Server (IIS)}{13}{section.2.13}
\contentsline {section}{\numberline {2.14}PDF eSurvey Technology}{16}{section.2.14}
\contentsline {subsection}{\numberline {2.14.1}Getting the eSurveys on the Server}{17}{subsection.2.14.1}
\contentsline {subsection}{\numberline {2.14.2}PDF eSurvey Technology Manager}{18}{subsection.2.14.2}
\contentsline {chapter}{\numberline {3}Upgrade}{21}{chapter.3}
\contentsline {chapter}{\numberline {4}Backup and Disaster Recovery}{23}{chapter.4}
\contentsline {chapter}{\numberline {5}Reporting Issues}{25}{chapter.5}
\contentsline {chapter}{\numberline {6}Release History}{27}{chapter.6}
\contentsline {chapter}{\numberline {7}Indices and tables}{29}{chapter.7}
