﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.TeacherLink>))]
    public class TeacherLinkBinder : Softwords.Web.Models.Entity.ModelBinder<Models.TeacherLink>
    {
        public int? ID
        {
            get
            {
                return (int?)getProp("lnkID");
            }
            set
            {
                definedProps.Add("lnkID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.TeacherLink e = null;

            IQueryable<Models.TeacherLink> qry = from teacherlink in cxt.TeacherLinks
                                                 where teacherlink.lnkID == ID
                                        select teacherlink;

            if (qry.Count() == 0)
            {
                throw new RecordNotFoundException(ID);
            }

            e = qry.First();
            if (Convert.ToBase64String(e.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(e);
                throw new ConcurrencyException(this.definedProps);
            }
            ToDb(cxt, identity, e);


        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.TeacherLink e = null;

            IQueryable<Models.TeacherLink> qry = from teacherlink in cxt.TeacherLinks
                                      where teacherlink.lnkID == ID
                                      select teacherlink;

            if (qry.Count() != 0)
            {
                throw new DuplicateKeyException(ID);
            }

            e = new Models.TeacherLink();
            cxt.TeacherLinks.Add(e);
            ToDb(cxt, identity, e);
        }
    }
}
