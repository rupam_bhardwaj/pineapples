﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pineapples.Data.DB;
using Softwords.DataTools;
using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data
{
    public class BaseRepository
    {
        protected DB.PineapplesEfContext cxt;
        protected BaseRepository(DB.PineapplesEfContext cxt)
        {
            this.cxt = cxt;
        }

        #region Protected data access methods

        protected DataSet cmdToDataset(SqlCommand cmd)
        {
            return Context.execCommand(cmd);
        }
        protected async Task<DataSet> cmdToDatasetAsync(SqlCommand cmd)
        {
            return await Context.execCommandAsync(cmd);
        }
        protected IDataResult sqlExec(SqlCommand cmd)
        {
            return sqlExec(cmd, true);
        }
        protected IDataResult sqlExec(SqlCommand cmd, bool hasPageInfo)
        {
            DataSet ds = cmdToDataset(cmd);
            return new DataResult(ds, hasPageInfo);
        }

        protected async Task<IDataResult> sqlExecAsync(SqlCommand cmd, bool hasPageInfo)
        {
            DataSet ds = await cmdToDatasetAsync(cmd);
            return new DataResult(ds, hasPageInfo);
        }

        protected IDataResult sqlExec(SqlCommand cmd, bool hasPageInfo, string tag)
        {
            DataSet ds = cmdToDataset(cmd);
            return new DataResult(ds, hasPageInfo, tag);
        }

        protected async Task<IDataResult> sqlExecAsync(SqlCommand cmd, bool hasPageInfo, string tag)
        {
            DataSet ds = await cmdToDatasetAsync(cmd);
            return new DataResult(ds, hasPageInfo, tag);
        }
        protected IDataResult sqlExec(SqlCommand cmd, bool hasPageInfo, string tag, string[] tablenames)
        {
            DataSet ds = cmdToDataset(cmd);
            for (int i = 0; i < tablenames.Length; i++)
            {
                ds.Tables[i].TableName = tablenames[i];
            }
            // pass true for sendTableNames
            return new DataResult(ds, hasPageInfo, tag, true);
        }
        protected async Task<IDataResult> sqlExecAsync(SqlCommand cmd, bool hasPageInfo, string tag, string[] tablenames)
        {
            DataSet ds = await cmdToDatasetAsync(cmd);
            for (int i = 0; i < tablenames.Length; i++)
            {
                ds.Tables[i].TableName = tablenames[i];
            }
            // pass true for sendTableNames
            return new DataResult(ds, hasPageInfo, tag, true);
        }
        protected DataResult structuredResult(SqlCommand cmd, string tag, string[] relatedTableNames)
        {
            DataSet ds = cmdToDataset(cmd);
            var result = ds.ToParentChild(relatedTableNames);
            return new DataResult(result, tag);
        }

        protected async Task<DataResult> structuredResultAsync(SqlCommand cmd, string tag, string[] relatedTableNames)
        {
            DataSet ds = await cmdToDatasetAsync(cmd);
            var result = ds.ToParentChild(relatedTableNames);
            return new DataResult(result, tag);
        }
        protected DataResult structuredResult(SqlCommand cmd, string tag, string[] relatedTableNames, StructureOptions[] options)
        {
            DataSet ds = cmdToDataset(cmd);
            var result = ds.ToParentChild(relatedTableNames, options);
            return new DataResult(result, tag);
        }
        protected async Task<DataResult> structuredResultAsync(SqlCommand cmd, string tag, string[] relatedTableNames, StructureOptions[] options)
        {
            DataSet ds = await cmdToDatasetAsync(cmd);
            var result = ds.ToParentChild(relatedTableNames, options);
            return new DataResult(result, tag);
        }
        protected DB.PineapplesEfContext Context
        {
            get { return cxt; }
        }
        #endregion

        #region type conversion helpers
        protected string db2str(object fld)
        {
            if (fld == null || Convert.IsDBNull(fld))
                return null;
            return fld.ToString();
        }
        protected int? db2int(object fld)
        {
            if (fld == null || Convert.IsDBNull(fld))
                return null;
            return Convert.ToInt32(fld);
        }
        #endregion
    }
}
