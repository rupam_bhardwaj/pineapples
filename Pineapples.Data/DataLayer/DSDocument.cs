﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using Softwords.DataTools;
using System.Security.Claims;

namespace Pineapples.Data.DataLayer
{
    internal class DSDocument : BaseRepository, IDSDocument
    {
        public DSDocument(DB.PineapplesEfContext cxt) : base(cxt) { }

        #region CRUD methods
        public IDataResult Create(DocumentBinder binder, ClaimsIdentity identity)
        {
            binder.Create(cxt, identity);
            return Read((Guid?)binder.ID);

        }
        public IDataResult Read(Guid? ID)
        {
            IEnumerable<Models.Document> qry = from document in cxt.Documents
                                                  where document.docID == ID
                                                  select document;
            Models.Document e = null;
            if (qry.Any())
            {
                e = qry.First();
            }

            DataResult ds = DataResult.FromResult(e, "document");
            return ds;
        }

        public DocumentBinder Update(DocumentBinder binder, ClaimsIdentity identity)
        {
            binder.Update(cxt, identity);
            return binder;
        }
        public DocumentBinder Delete(Guid? docID, ClaimsIdentity identity)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
