﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class SchoolAccreditationFilter : Filter
    {
        public string SAID { get; set; }
        public string School { get; set; }
        public string SchoolNo { get; set; }
        public string InspYear { get; set; }
    }
    
    public class SchoolAccreditationTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public SchoolAccreditationFilter filter { get; set; }
    }
}
