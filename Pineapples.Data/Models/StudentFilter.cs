﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class StudentFilter : Filter
    {
        public Guid? StudentID { get; set; }
        public string StudentCardID { get; set; }
        public string StudentGiven { get; set; }
        public string StudentFamilyName { get; set; }
        public DateTime? StudentDoB { get; set; }
        public string StudentGender { get; set; }
        public string StudentEthnicity { get; set; }
    }

    public class StudentTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public StudentFilter filter { get; set; }
    }
}
