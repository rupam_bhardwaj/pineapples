﻿using System;
using System.Collections.Generic;
using System.Linq;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public class ExamFilter : Filter
    {
        public string SchoolNo { get; set; }
        public int? ExamID { get; set; }
        public string ExamCode { get; set; }
        public string ExamYear { get; set; }
        public DateTime? ExamDate { get; set; }
    }
}
