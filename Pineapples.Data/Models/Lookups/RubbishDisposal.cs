using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRubbishDisposal")]
    [Description(@"Method of rubbish Disposal. Appears on SchoolSurvey")]
    public partial class RubbishDisposal : SequencedCodeTable
    {
        [Column("codeDesc", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Desc")]
        public override string Description { get; set; }

        [Column("codeSort", TypeName = "int")]
        public override int? Seq { get; set; }
    }
}
