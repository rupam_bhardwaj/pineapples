using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpPNAReasons")]
    [Description(@"Principal reason for Pupils Not Attending Vanuatu 2010 ff.")]
    public partial class PNAReason : SequencedCodeTable { }
}
