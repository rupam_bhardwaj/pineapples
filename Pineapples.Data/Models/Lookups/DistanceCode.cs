using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpDistanceCodes")]
    [Description(@"Represents distance from School In Distance/Transport component")]
    public partial class DistanceCode: ChangeTracked
    {
        [Key]

        [Column("codeNum", TypeName = "int")]
        [Required(ErrorMessage = "code Num is required")]
        [Display(Name = "code Num")]
        public int codeNum { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public string Description { get; set; }
    }
}
