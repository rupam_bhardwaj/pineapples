using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpToiletTypes")]
    [Description(@"Type of toilets. Foreign key on Toilets")]
    public partial class ToiletType : ChangeTracked
    {
        [Key]

        [Column("ttypName", TypeName = "nvarchar")]
        [MaxLength(25)]
        [StringLength(25)]
        [Required(ErrorMessage = "ttyp Name is required")]
        [Display(Name = "ttyp Name")]
        public string ttypName { get; set; }

        [Column("ttypSystem", TypeName = "bit")]
        [Display(Name = "ttyp System")]
        public bool? ttypSystem { get; set; }

        [Column("ttypSort", TypeName = "int")]
        [Display(Name = "ttyp Sort")]
        public int? ttypSort { get; set; }

        [Column("ttypNameL1", TypeName = "nvarchar")]
        [MaxLength(25)]
        [StringLength(25)]
        [Display(Name = "ttyp Name L1")]
        public string ttypNameL1 { get; set; }

        [Column("ttypNameL2", TypeName = "nvarchar")]
        [MaxLength(25)]
        [StringLength(25)]
        [Display(Name = "ttyp Name L2")]
        public string ttypNameL2 { get; set; }

        [Column("ttypGroup", TypeName = "nvarchar")]
        [MaxLength(25)]
        [StringLength(25)]
        [Display(Name = "ttyp Group")]
        public string ttypGroup { get; set; }
    }
}
