using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSalaryPoints")]
    [Description(@"Salary point futher divide Salary Levels. Each point is tied to a level. Salries are tied to Salary Points, and rates may vary over time.")]
    public partial class SalaryPoint : ChangeTracked
    {
        [Key]

        [Column("spCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "sp Code is required")]
        [Display(Name = "sp Code")]
        [Description(@"Salary Point Code")]
        public string spCode { get; set; }

        [Column("spSalary", TypeName = "money")]
        [Display(Name = "sp Salary")]
        [Description(@"Unused - salaries related to points are maintained by date in SalaryPointRates")]
        public decimal? spSalary { get; set; }

        [Column("spSort", TypeName = "int")]
        [Display(Name = "sp Sort")]
        [Description(@"Sort order for salary points. Should correspond to increasing pay ie lowest to highest. Need not be contiguous numbers.")]
        public int? spSort { get; set; }

        [Column("salLevel", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "sal Level")]
        [Description(@"Salary Level to which this point belongs")]
        public string salLevel { get; set; }

        [Column("spExternalU", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "sp External U")]
        [Description(@"override the code for searching ro loading from payroll system")]
        public string spExternalU { get; set; }

        [Column("spExternal", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required(ErrorMessage = "sp External is required")]
        [Display(Name = "sp External")]
        [Description(@"code for external payroll system = spExternalU, defaults to spCode")]
        public string spExternal { get; set; }
    }
}
