﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Pineapples.Data
{
    public class TeacherFilter : Filter
    {
        public int? TeacherID { get; set; }
        public string Surname { get; set; }
        public string GivenName { get; set; }
        public string PayrollNo { get; set; }
        public string RegistrationNo { get; set; }
        public string ProvidentFundNo { get; set; }
        public string Gender { get; set; }
        public DateTime? DoB { get; set; }
        public DateTime? DobEnd { get; set; }
        public string Language { get; set; }
        public string PaidBy { get; set; }
        public string Qualification { get; set; }
        public string EdQualification { get; set; }
        public string Subject { get; set; }
        public int? SearchSubjectTaught { get; set; }
        public int? SearchTrained { get; set; }
        public string AtSchool { get; set; }
        public string AtSchoolType { get; set; }
        public int? InYear { get; set; }
        public string Role { get; set; }
        public string LevelTaught { get; set; }
        public int? YearOfEdMin { get; set; }
        public int? YearOfEdMax { get; set; }
        public string ISCEDSubClass { get; set; }
        public int? UseOr { get; set; }
        public int? CrossSearch { get; set; }
        public int? SoundSearch { get; set; }
        public XDocument XmlFilter { get; set; }
    }

    public class TeacherTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public TeacherFilter filter { get; set; }
    }
}