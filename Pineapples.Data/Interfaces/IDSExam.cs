﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;
using System.Xml.Linq;


namespace Pineapples.Data
{
    public interface IDSExam : IDSCrud<ExamBinder, int>
    {
        IDataResult SchoolExamData(string schNo, int examID);
        IDataResult SchoolExamList(string schNo);

        IDataResult Filter(ExamFilter fltr);

        /// <summary>
        /// Upload exam from standard Xml format
        /// </summary>
        /// <param name="examData">the Xml file</param>
        /// <param name="fileReference">File Reference for the Xml file in the FileDB</param>
        /// <param name="user">current user name</param>
        /// <returns></returns>
        IDataResult Upload(XDocument examData, Guid fileReference, string user);    
    }
}
