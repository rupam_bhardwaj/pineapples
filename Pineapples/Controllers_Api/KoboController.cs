﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Softwords.Web;
using OfficeOpenXml;
using Microsoft.AspNet.SignalR;
using Pineapples.Hubs;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/kobo")]
    public class KoboController : PineapplesApiController
    {
        public KoboController(DataLayer.IDSFactory factory) : base(factory) { }


        /// <summary>
        /// uploads and stores the file
        /// </summary>
        /// <returns>a summary of the data in the file, together with the file ID</returns>
        [HttpPost]
        [Route(@"upload")]
        public async Task<object> Upload()
        {

            ListObject lo = null;
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);
            Dictionary<string, object> results = new Dictionary<string, object>();

            foreach (MultipartFileData fd in files.FileData)
            {
                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = System.IO.Path.GetExtension(filename);

                Guid g;
                OfficeOpenXml.ExcelPackage p;
                using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {
                    g = Providers.FileDB.Store(fstrm, ext);
                    // add a document record for this file

                    // create an Excel workbook object from the stream
                    p = new ExcelPackage(fstrm);
                };
                // we can get rid of the local file now
                System.IO.File.Delete(fd.LocalFileName);

                // we have to determine what this Kobo package is: most definitivate is the Version column
                // There is only one sheet, one table, one ListObject
                ExcelWorksheet wk = p.Workbook.Worksheets[1];
                // get the first table off that sheet...
                lo = new ListObject(wk);
                XDocument xd = lo.ToXml();
                IDataResult validateResults = await Ds.KoboValidateAsync(xd, g, User.Identity.Name);

                string inspType = String.Empty;
                if (lo.ColIndex("inspectionType") == -1)
                    inspType = "<>";
                else
                {
                    inspType = (string)lo.First()["inspectionType"];
                    var typrec = Factory.Context.InspectionTypes.Find(inspType);
                    if (typrec != null)
                        inspType = typrec.Description;
                }
                    

                results.Add("id", g);
                results.Add("sheetName", wk.Name);
                results.Add("listRows", lo.Count());
                results.Add("surveyTool", inspType);
                results.Add("firstDate", lo.First()[0]);
                results.Add("lastDate", lo.Last()[0]);
                results.Add("version", lo.First()["_version_"]);

                results.Add("validations", validateResults.ResultSet);
            }
            return results;      // only the last one? but we expect only one at a time.....?
        }

        /// <summary>
        /// Processes the nominated file
        /// </summary>
        /// <param name="fileId">guid to the Excel document in the FileDB</param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"process/{fileId}")]
        public async Task<object> Process(string fileId)
        {
            Guid g = new Guid(fileId);
            ExcelPackage p;

            // first get the file based on its Id
            string path = Providers.FileDB.GetFilePath(fileId);
            using (System.IO.FileStream fstrm = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                // create an Excel workbook object from the stream
                p = new ExcelPackage(fstrm);
            };

            
            // There is only one sheet, one table, one ListObject
            ExcelWorksheet wk = p.Workbook.Worksheets[1];
            // get the first table off that sheet...
            ListObject lo = new ListObject(wk);
            XDocument xd = lo.ToXml();
            IDataResult results = await Ds.KoboAsync(xd, g, User.Identity.Name);
            return results;         // we are interested in the student results
        }


        readonly Lazy<IHubContext<INdoeProcessCallbacks>> hub = new Lazy<IHubContext<INdoeProcessCallbacks>>(
            () => GlobalHost.ConnectionManager.GetHubContext<INdoeProcessCallbacks>("ndoeprocess")
        );

        protected IHubContext<INdoeProcessCallbacks> Hub
        {
            get { return hub.Value; }
        }

        private IDSKobo Ds
        {
            get { return Factory.Kobo(); }
        }

        #region helpers

        private XElement collectSchools(ListObject lo, string sheetName)
        {
            XElement xrow;
            XElement xsheet;
            xsheet = new XElement("Sheet");
            xsheet.SetAttributeValue("name", sheetName);
            foreach (ListRow r in lo)
            {
                xrow = new XElement("School_No", r["School_No"]);
                xrow.SetAttributeValue("Index", r.idx);
                // it turns out that to get the sheet name by backtracking up the xml hierarchy
                // is monumentally slow
                // so we add it on each and every node :(
                xrow.SetAttributeValue("Sheet", sheetName);
                xsheet.Add(xrow);
            }
            return xsheet;
        }
        /// <summary>
        /// add the district and survey year as attributes to the root node
        /// a convenient way to get these into the stored proc
        /// </summary>
        /// <param name="xd"></param>
        private XDocument prepXml(XDocument xd, string districtName, string schoolYear)
        {
            xd.Root.SetAttributeValue("state", districtName);
            xd.Root.SetAttributeValue("schoolYear", schoolYear);
            return xd;
        }

        #endregion
    }
}
