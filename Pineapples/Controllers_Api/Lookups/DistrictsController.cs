﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;


namespace Pineapples.Controllers
{
    [RoutePrefix("api/districts")]
    public class DistrictsController : TableMaintenanceController<District, string>
    {
        public DistrictsController(DataLayer.IDSFactory factory) : base(factory) { }

    }
}
