﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/exams")]
    public class ExamsController : PineapplesApiController
    {
        public ExamsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(ExamFilter fltr)
        {
            return Ds.Filter(fltr);
        }
        #endregion

        [HttpPost]
        [ActionName("schoolcompare")]
        public object SchoolCompare(ExamFilter fltr)
        {
            if ( fltr.ExamID == null )
            {
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError);
                throw new HttpResponseException(resp);
            }
            return Factory.Exam().SchoolExamData(fltr.SchoolNo, (int)fltr.ExamID);
        }

        [HttpPost]
        [ActionName("schoollist")]
        public object SchoolList(string schoolNo)
        {
            return Factory.Exam().SchoolExamList(schoolNo);
        }

        #region upload
        [HttpPost]
        [Route(@"upload")]
        public async Task<object> Upload()
        {
            dynamic result = new System.Dynamic.ExpandoObject();
            string ExamCodePath = "NMCT/NMCT_test";
            string ExamCodeAttr = "test_group_ID";
            string ExamNamePath = "NMCT/NMCT_test/Test_name";
            string ExamYear = "NMCT/NMCT_test/Year";

            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);

            foreach (MultipartFileData fd in files.FileData)
            {
                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = Path.GetExtension(filename);

                using (FileStream fstrm = new FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {
                    Guid g = Providers.FileDB.Store(fstrm, ext);
                    // get the exam data - rewind the stream
                    fstrm.Position = 0;
                    XDocument exam = XDocument.Load(fstrm);
                    result.examCode = exam.XPathSelectElement(ExamCodePath).Attribute(ExamCodeAttr).Value;
                    result.examName = exam.XPathSelectElement(ExamNamePath).Value;
                    result.examYear = exam.XPathSelectElement(ExamYear).Value;
                    result.students = exam.XPathSelectElements("NMCT/Students/Student").Count();
                    result.results = exam.XPathSelectElements("NMCT/Students/Student/Results/Result").Count();
                    result.id = g;
                }
                // delete the local file, or else app_data is strewn with BodyParts! 'using' ensures the stream is disposed
                System.IO.File.Delete(fd.LocalFileName);

                int examYear = Convert.ToInt32(((string)(result.examYear)).Substring(0, 4)) + 1;
                string examCode = result.examCode;
                // as well we'll check if there is already a record
                Data.Models.Exam ex = Factory.Context.Exams.FirstOrDefault(exam => exam.exCode == examCode && exam.exYear == examYear);
                result.exam = ex;
            }
            return result;
        }

        /// <summary>
        /// process an uploaded file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(@"process/{fileId}")]
        public Softwords.DataTools.IDataResult Process(string fileId)
        {
            Guid g = new Guid(fileId);
            // get the document back from the Id
            Stream strm = FileDB.Get(fileId);
            XDocument exam = XDocument.Load(strm);
            exam = doTransform(exam);
            return Factory.Exam().Upload(exam, g, User.Identity.Name);
        }

        /// <summary>
        /// If the process fails validation tests, the file is deleted.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(@"cancelprocess/{fileId}")]
        public void CancelProcess(string fileId)
        {
            Guid g = new Guid(fileId);
             // cancel should only be allowed to delete a file that is not referenced in the Documents table
            // so, id we find the key FileId in the table Documents - do not remove
            // otherwise, we effectively have a url that lets you trash the FileDb
            if (!Factory.Context.Documents.Any(doc => doc.docID == g))
            {
                FileDB.Remove(fileId);
            }
        }



        /// <summary>
        /// adds a sequence number attribute to each Student node
        /// </summary>
        /// <param name="examData">the XDocument of exam data</param>
        /// <returns></returns>
        private XDocument doTransform(XDocument examData)
        {

            string xslMarkup = @"<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
    xmlns:msxsl='urn:schemas-microsoft-com:xslt' exclude-result-prefixes='msxsl'>
    <xsl:output method='xml' indent='yes'/>
    <xsl:template match='@* | node()'>
        <xsl:copy>
            <xsl:apply-templates select='@* | node()'/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match='Student'>
      <Student>
        <xsl:attribute name='seq'>
          <xsl:value-of select='position()'/>
        </xsl:attribute>
        <xsl:apply-templates select='@* | node()'/>
      
      </Student>
    </xsl:template>
</xsl:stylesheet>
";

            XDocument transformed = new XDocument();
            using (System.Xml.XmlWriter writer = transformed.CreateWriter())
            {
                // Load the style sheet.  
                System.Xml.Xsl.XslCompiledTransform xslt = new System.Xml.Xsl.XslCompiledTransform();
                xslt.Load(System.Xml.XmlReader.Create(new StringReader(xslMarkup)));

                // Execute the transform and output the results to a writer.  
                xslt.Transform(examData.CreateReader(), writer);
            }
            return transformed;
        }
        #endregion

        #region Exam CRUD methods
        [HttpGet]
        [Route(@"{examID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Exam, PermissionAccess.Read)]
        public object Read(int examID)
        {
            return Ds.Read(examID); //, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.School, PermissionAccess.ReadX));
        }

        [HttpPost]
        [Route(@"")]
        // [Route(@"{examID}")] this route can never make sense because examID is an identity and so
        // cannot be specified by the client
        [PineapplesPermission(PermissionTopicEnum.Exam, Softwords.Web.PermissionAccess.WriteX)]
        public object Create(ExamBinder examIdentity)
        {
            try
            {
                return Ds.Create(examIdentity, (ClaimsIdentity)User.Identity);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpPut]
        [Route(@"{examID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Exam, Softwords.Web.PermissionAccess.Write)]
        public object Update(ExamBinder examIdentity)
        {
            try
            {
                return Ds.Update(examIdentity, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }
        #endregion

        private IDSExam Ds
        {
            get { return Factory.Exam(); }
        }
    }
}
