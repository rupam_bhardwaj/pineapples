﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;
using Softwords.Web;
using OfficeOpenXml;
using Microsoft.AspNet.SignalR;
using Pineapples.Hubs;
using System.Collections.Specialized;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/jasper")]
    public class JasperApiController : PineapplesApiController
    {
        public JasperApiController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpGet]
        [Route("reportlist")]
        public async Task<HttpResponseMessage> ReportList()
        {
            string requestPath = "rest_v2/resources?type=reportUnit";
            return await JasperResponse(requestPath);
        }

        [HttpPost]
        [Route("reportlist")]
        public async Task<HttpResponseMessage> ReportList(JasperListRequest input)
        {
            //  
            string requestPath = String.Format("rest_v2/resources?type=reportUnit&folderUri=/{0}", jasperReportPath(input.folder));
            // set up the url to point to jasper
            return await JasperResponse(requestPath);
        }

        [HttpGet]
        [Route("reportparams/{p0}")]
        [Route("reportparams/{p0}/{p1}")]
        public async Task<HttpResponseMessage> ReportParams(string p0, string p1)
        {
            // not sure we want to keep this Get version? 
            string requestPath = p0;
            if (p1 != String.Empty)
            {
                requestPath += "/" + p1;
            }

            requestPath = String.Format("rest_v2/reports/{0}/inputControls", jasperReportPath(requestPath));

            // set up the url to point to jasper
            return await JasperResponse(requestPath);
        }
        [HttpPost]
        [Route("reportparams")]
        public async Task<HttpResponseMessage> ReportParamsPost(InputRequest request)
        {
             string requestPath = String.Format("rest_v2/reports/{0}/inputControls", jasperReportPath(request.reportName));

            // set up the url to point to jasper
            return await JasperResponse(requestPath);
        }

        [HttpPost]
        [Route("report")]
        public async Task<HttpResponseMessage> getReport(ReportInfo info)
        {
           // List<KeyValuePair<string, object>> args = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(info.args);
            string path = info.path;
           // assemble the jasperurl - its all in the get
            string requestUrl;
            NameValueCollection args = System.Web.HttpUtility.ParseQueryString(string.Empty);
            foreach (string n in info.args.Keys)
            {
                args[n] = System.Web.HttpUtility.UrlEncode(info.args[n].ToString());
            }
            switch (info.format)
            {
                case "EXCEL":
                    info.format = "xls";
                    break;
                case null:
                    info.format = "pdf";
                    break;
                default:
                    info.format = info.format.ToLower();
                    break;
            }
            requestUrl = String.Format("rest_v2/reports/{0}.{1}?{2}", jasperReportPath(info.path) , info.format, args.ToString());
            HttpResponseMessage cliResponse = await JasperResponse(requestUrl);
            if (info.filename != null && cliResponse.Content.Headers.ContentDisposition != null)
            {
                string ext = System.IO.Path.GetExtension(info.filename);
                if (ext == string.Empty)
                {
                    info.filename += "." + info.format;
                }
                cliResponse.Content.Headers.ContentDisposition.FileName = info.filename;
            };
            return cliResponse;
        }

        protected async Task<HttpResponseMessage> JasperResponse(string requestPath)
        {
            var httpClient = new HttpClient();
            HttpRequestMessage request = JasperRequest(requestPath);

            try
            {
                HttpResponseMessage cliResponse = await httpClient.SendAsync(request);
                return cliResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // a standard get request
        protected HttpRequestMessage JasperRequest(string requestPath)
        {

            string requestUrl = jasperPath(requestPath);

            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(requestUrl),
                Method = HttpMethod.Get,
            };
            // to do, not hardcoded user name / password??
            var authToken = Convert.ToBase64String(
                System.Text.ASCIIEncoding.ASCII.GetBytes(
                    string.Format("{0}:{1}",
                    System.Web.Configuration.WebConfigurationManager.AppSettings["jasperUser"] ?? "emis",
                    System.Web.Configuration.WebConfigurationManager.AppSettings["jasperPass"] ?? "EmisUser")));

            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", authToken);

            request.Headers.Add("Accept", "application/json");
            return request;
        }

        protected string jasperPath(string request)
        {
            string jasperRoot = System.Web.Configuration.WebConfigurationManager.AppSettings["jasperUrl"] ?? "http://localhost:8082/jasperserver";
            return String.Format("{0}/{1}",jasperRoot, request);
        }

        /// <summary>
        /// Converts a relative path in the Jasper report hierarchy to an absolute path
        /// It begins with the jasperReportRoot (usually 'Reports')
        /// and append the Context too that.
        /// So, when the client requests
        /// schools/schoolreport
        /// the report server up will be e.g.
        /// reports/siemis/schools/schoolreport
        /// </summary>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        protected string jasperReportPath(string relativePath)
        {
            // first check if this is an absolute report path; ie it has been created from a jasper resource
            if (relativePath.StartsWith("/" + jasperReportRoot + "/" + Context))
                return relativePath.Substring(1);

            // info.path is a path relative to the report root (assumed to be 'reports') and the context
            if (Context == String.Empty)
            {
                if (relativePath == string.Empty)
                {
                    // jasper doesn;t like a trailing /
                    return jasperReportRoot;
                }
                return String.Format("{0}/{1}", jasperReportRoot, relativePath);
            }
            else
            {
                if (relativePath == string.Empty)
                {
                    // jasper doesn;t like a trailing /
                    return String.Format("{0}/{1}", jasperReportRoot, Context);
                }
                return String.Format("{0}/{1}/{2}", jasperReportRoot, Context, relativePath);
            }

        }
        protected string jasperReportRoot
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["jasperReportRoot"] ?? "reports";
            }
        }
    }
}
