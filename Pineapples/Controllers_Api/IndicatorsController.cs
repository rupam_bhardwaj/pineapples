﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using Softwords.DataTools;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;


namespace Pineapples.Controllers
{
    public class IndicatorsController : PineapplesApiController
    {
        public IndicatorsController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpGet]
        [Route(@"api/indicators/vermdata")]
        public HttpResponseMessage vermData()
        {
            // look first for the name qualified by the context e.g. VermData_kemis.xml
            string filename = string.Format(@"~/assets_local/data/VermData_{0}.xml", Context);

            filename = System.Web.Hosting.HostingEnvironment.MapPath(filename);
            if (!System.IO.File.Exists(filename))
            {
                filename = @"~/assets_local/data/VermData.xml";
            }
            Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(filename);

            return new HttpResponseMessage() { Content = content };
        }

        [HttpGet]
        [Route(@"api/indicators/vermdatadistrict/{district}")]
        public HttpResponseMessage vermDataDistrict(string district)
        {
            // look first for the name qualified by the context e.g. VermData_fedemis_chk.xml
            string filename = string.Format(@"~/assets_local/data/vermdata_{0}_{1}.xml", Context, district);

            filename = System.Web.Hosting.HostingEnvironment.MapPath(filename);
            if (!System.IO.File.Exists(filename))
            {
                filename = @"~/assets_local/data/VermData.xml";
            }
            Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(filename);

            return new HttpResponseMessage() { Content = content };
        }

        [HttpGet]
        [Route(@"api/indicators/makewarehouse/{year:int?}")]
        [PineapplesPermission(PermissionTopicEnum.Survey, Softwords.Web.PermissionAccess.Admin)]
        public async Task<HttpResponseMessage> makeWarehouse(int? year = null)
        {


            await Factory.Indicators().makeWarehouse(year);



            return await makeVermData(year);
        }
        [HttpGet]
        [Route(@"api/indicators/makevermdata/{year:int?}")]
        // [PineapplesPermission(PermissionTopicEnum.Survey,Softwords.Web.PermissionAccess.Admin)]
        public async Task<HttpResponseMessage> makeVermData(int? year = null)
        {
            string filename = string.Format(@"~/assets_local/data/VermData_{0}.xml", Context);

            System.Xml.Linq.XDocument xverm = await Factory.Indicators().getVermData(year);

            filename = System.Web.Hosting.HostingEnvironment.MapPath(filename);
            xverm.Save(filename);
            Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(xverm);

            return new HttpResponseMessage() { Content = content };
        }

        [HttpGet]
        [Route(@"api/indicators/livevermdata/{year:int?}")]
        public async Task<HttpResponseMessage> liveVermData(int? year = null)
        {
            System.Xml.Linq.XDocument xverm = await Factory.Indicators().getVermData(year);
            Softwords.Web.Content.XmlContent content = new Softwords.Web.Content.XmlContent(xverm);

            return new HttpResponseMessage() { Content = content };
        }



    }
}
