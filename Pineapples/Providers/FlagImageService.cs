﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using ImageProcessor.Web.Services;

namespace Pineapples.Providers
{
    public class FlagImageService : CloudImageService, IImageService
    {
        public override bool IsValidRequest(string path)
        {
            // return base.IsValidRequest(trimPath(path));
            return (trimPath(path).Length == 2);
        }

        public override async Task<byte[]> GetImage(object id)
        {
            string countryCode = id.ToString();
            string url = String.Format("{0}/{1}.gif", countryCode.Substring(0, 1), countryCode.Substring(0, 2));
            byte[] buffer = await base.GetImage(url);
            return buffer;
        }
        private string trimPath(string path)
        {
            if (path.EndsWith("/"))
            {
                path = path.Substring(0, path.Length - 1);
            }
            return path;
        }
    }
}